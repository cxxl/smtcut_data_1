
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T19:44:29+02:00
Command line: BIOMD0000000200


Model: BIOMD0000000200
Logfile: db\BIOMD0000000200\ep11-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^20.4 in 27 bags
Bag sizes: 30 2 32 24 2 8 2 16 16 4 12 2 8 2 6 2 14 14 6 6 1 2 1 2 9 11 12

Minimizing (2,0)... (2,0)


Running SMT solver (1)... 0.141 sec
Checking... Minimizing (21,87)... (20,6), 0.078 sec
Inserting... 0.000 sec
[1 ph, mem 53/45, smt 0.141, isect 0.078, ins 0.000, tot 0.297]

Running SMT solver (2)... 0.047 sec
Checking... Minimizing (19,81)... (18,16), 0.078 sec
Inserting... 0.000 sec
[2 ph, mem 56/49, smt 0.188, isect 0.156, ins 0.000, tot 0.422]

Running SMT solver (3)... 0.031 sec
Checking... Minimizing (20,82)... (20,8), 0.062 sec
Inserting... 0.000 sec
[3 ph, mem 59/52, smt 0.219, isect 0.219, ins 0.000, tot 0.516]

Running SMT solver (4)... 0.016 sec
Checking... Minimizing (20,82)... (19,10), 0.078 sec
Inserting... 0.000 sec
[2 ph, mem 59/52, smt 0.234, isect 0.297, ins 0.000, tot 0.625]

Running SMT solver (5)... 0.000 sec
Checking... Minimizing (21,82)... (19,11), 0.078 sec
Inserting... 0.000 sec
[3 ph, mem 62/54, smt 0.234, isect 0.375, ins 0.000, tot 0.703]

Running SMT solver (6)... 0.000 sec
Checking... Minimizing (19,81)... (18,17), 0.094 sec
Inserting... 0.016 sec
[2 ph, mem 69/62, smt 0.234, isect 0.469, ins 0.016, tot 0.812]

Running SMT solver (7)... 0.016 sec
Checking... Minimizing (21,84)... (21,4), 0.047 sec
Inserting... 0.000 sec
[3 ph, mem 61/54, smt 0.250, isect 0.516, ins 0.016, tot 0.891]

Running SMT solver (8)... 0.016 sec
Checking... Minimizing (21,84)... (20,6), 0.078 sec
Inserting... 0.000 sec
[3 ph, mem 65/58, smt 0.266, isect 0.594, ins 0.016, tot 0.984]

Running SMT solver (9)... 0.031 sec
Checking... Minimizing (20,85)... (19,15), 0.094 sec
Inserting... 0.000 sec
[4 ph, mem 67/60, smt 0.297, isect 0.688, ins 0.016, tot 1.109]

Running SMT solver (10)... 0.016 sec
Checking... Minimizing (19,84)... (18,18), 0.094 sec
Inserting... 0.000 sec
[3 ph, mem 74/66, smt 0.312, isect 0.781, ins 0.016, tot 1.234]

Running SMT solver (11)... 0.000 sec
Checking... Minimizing (20,83)... (19,17), 0.094 sec
Inserting... 0.000 sec
[4 ph, mem 66/59, smt 0.312, isect 0.875, ins 0.016, tot 1.344]

Running SMT solver (12)... 0.000 sec
Checking... Minimizing (19,82)... (18,22), 0.094 sec
Inserting... 0.016 sec
[4 ph, mem 72/64, smt 0.312, isect 0.969, ins 0.031, tot 1.469]

Running SMT solver (13)... 0.016 sec
Checking... Minimizing (21,87)... (20,4), 0.078 sec
Inserting... 0.000 sec
[5 ph, mem 74/66, smt 0.328, isect 1.047, ins 0.031, tot 1.562]

Running SMT solver (14)... 0.031 sec
Checking... Minimizing (19,82)... (18,20), 0.094 sec
Inserting... 0.000 sec
[6 ph, mem 66/58, smt 0.359, isect 1.141, ins 0.031, tot 1.688]

Running SMT solver (15)... 0.016 sec
Checking... Minimizing (20,87)... (19,11), 0.094 sec
Inserting... 0.000 sec
[6 ph, mem 70/62, smt 0.375, isect 1.234, ins 0.031, tot 1.812]

Running SMT solver (16)... 0.000 sec
Checking... Minimizing (19,87)... (18,15), 0.094 sec
Inserting... 0.016 sec
[7 ph, mem 73/65, smt 0.375, isect 1.328, ins 0.047, tot 1.922]

Running SMT solver (17)... 0.016 sec
Checking... Minimizing (20,82)... (19,16), 0.094 sec
Inserting... 0.000 sec
[8 ph, mem 76/68, smt 0.391, isect 1.422, ins 0.047, tot 2.031]

Running SMT solver (18)... 0.016 sec
Checking... Minimizing (20,82)... (19,10), 0.078 sec
Inserting... 0.000 sec
[9 ph, mem 70/62, smt 0.406, isect 1.500, ins 0.047, tot 2.125]

Running SMT solver (19)... 0.016 sec
Checking... Minimizing (19,82)... (18,19), 0.078 sec
Inserting... 0.016 sec
[10 ph, mem 73/65, smt 0.422, isect 1.578, ins 0.062, tot 2.234]

Running SMT solver (20)... 0.016 sec
Checking... Minimizing (21,87)... (21,4), 0.078 sec
Inserting... 0.000 sec
[11 ph, mem 75/67, smt 0.438, isect 1.656, ins 0.062, tot 2.328]

Running SMT solver (21)... 0.016 sec
Checking... Minimizing (21,87)... (21,4), 0.062 sec
Inserting... 0.016 sec
[11 ph, mem 79/71, smt 0.453, isect 1.719, ins 0.078, tot 2.422]

Running SMT solver (22)... 0.047 sec
Checking... Minimizing (20,85)... (19,15), 0.094 sec
Inserting... 0.000 sec
[12 ph, mem 73/65, smt 0.500, isect 1.812, ins 0.078, tot 2.578]

Running SMT solver (23)... 0.000 sec
Checking... Minimizing (20,85)... (20,12), 0.094 sec
Inserting... 0.000 sec
[13 ph, mem 76/68, smt 0.500, isect 1.906, ins 0.078, tot 2.672]

Running SMT solver (24)... 0.016 sec
Checking... Minimizing (20,85)... (20,9), 0.078 sec
Inserting... 0.000 sec
[13 ph, mem 79/71, smt 0.516, isect 1.984, ins 0.078, tot 2.781]

Running SMT solver (25)... 0.031 sec
Checking... Minimizing (19,82)... (18,16), 0.078 sec
Inserting... 0.000 sec
[14 ph, mem 80/72, smt 0.547, isect 2.062, ins 0.078, tot 2.906]

Running SMT solver (26)... 0.047 sec
Checking... Minimizing (22,85)... (20,8), 0.078 sec
Inserting... 0.000 sec
[15 ph, mem 74/66, smt 0.594, isect 2.141, ins 0.078, tot 3.031]

Running SMT solver (27)... 0.016 sec
Checking... Minimizing (20,85)... (20,8), 0.078 sec
Inserting... 0.000 sec
[15 ph, mem 79/71, smt 0.609, isect 2.219, ins 0.078, tot 3.141]

Running SMT solver (28)... 0.047 sec
Checking... Minimizing (19,85)... (18,20), 0.094 sec
Inserting... 0.000 sec
[15 ph, mem 80/72, smt 0.656, isect 2.312, ins 0.078, tot 3.281]

Running SMT solver (29)... 0.031 sec
Checking... Minimizing (20,80)... (20,9), 0.047 sec
Inserting... 0.000 sec
[16 ph, mem 76/68, smt 0.688, isect 2.359, ins 0.078, tot 3.359]

Running SMT solver (30)... 0.016 sec
Checking... Minimizing (19,82)... (18,13), 0.094 sec
Inserting... 0.000 sec
[17 ph, mem 78/70, smt 0.703, isect 2.453, ins 0.078, tot 3.469]

Running SMT solver (31)... 0.031 sec
Checking... Minimizing (19,82)... (18,18), 0.094 sec
Inserting... 0.000 sec
[18 ph, mem 82/74, smt 0.734, isect 2.547, ins 0.078, tot 3.594]

Running SMT solver (32)... 0.031 sec
Checking... Minimizing (20,84)... (20,7), 0.078 sec
Inserting... 0.000 sec
[19 ph, mem 84/76, smt 0.766, isect 2.625, ins 0.078, tot 3.703]

Running SMT solver (33)... 0.031 sec
Checking... Minimizing (20,84)... (20,10), 0.094 sec
Inserting... 0.000 sec
[20 ph, mem 79/71, smt 0.797, isect 2.719, ins 0.078, tot 3.828]

Running SMT solver (34)... 0.016 sec
No more solutions found

20 polyhedra, 34 rounds, smt 0.812 sec, isect 2.719 sec, insert 0.078 sec, total 3.844 sec
Preprocessing 0.047 sec, super total 3.891 sec
Comparing... 0.531 sec.  Solution matches input.
Total contains() calls: 274, full: 16, ratio: 5.839%
