
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:37:10+02:00
Command line: ..\trop\ptcut.py BIOMD0000000714 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000714 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 2NJ4UJQP4KW0Z
Effective epsilon: 1/11.0

Reading db\BIOMD0000000714\vector_field_q.txt
Reading db\BIOMD0000000714\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k17**2*k2*x1 + k17**2*k4 + k2*x1*x4**2 + k4*x4**2

Multiplying equation 2 with: k11*k17**2 + k11*x4**2 + k13*k17**2*x3 + k17**2*k8*x1 + k17**2*k9*x2

Multiplying equation 3 with: k15**6*k17**12 + 6*k15**6*k17**10*x4**2 + 15*k15**6*k17**8*x4**4 + 20*k15**6*k17**6*x4**6 + 15*k15**6*k17**4*x4**8 + 6*k15**6*k17**2*x4**10 + k15**6*x4**12 + k17**12*x2**6

Multiplying equation 4 with: k17**2*k20*x3 + k17**2*x2 + k17**2 + x4**2
System is rational in variable x1,x2,x3,x4.
System is rational in parameter k2,k4,k6,k8,k9,k11,k13,k15,k17,k20.
Tropicalization time: 6.964 sec
Variables: x1-x4
Saving tropical system...
Creating polyhedra... 
Creation time: 0.073 sec
Saving polyhedra...

Sorting ascending...
Dimension: 4
Formulas: 4
Order: 0 2 3 1
Possible combinations: 2 * 3 * 5 * 6 = 180 (10^2)
 Applying common planes (codim=1, hs=1)...
 [1/4 (#0)]: 2 => 2 (-), dim=3.0, hs=3.0
 [2/4 (#2)]: 3 => 3 (-), dim=2.0, hs=4.0
 [3/4 (#3)]: 5 => 5 (-), dim=2.0, hs=4.4
 [4/4 (#1)]: 6 => 4 (2 empty), dim=2.0, hs=4.0
  Removed: 1.2,1.3
 Savings: factor 1.5, 0 formulas
[1/3 (#2, #0)]: 3 * 2 = 6 => 2 (4 incl), dim=2.0, hs=4.0
 Applying common planes (codim=0, hs=2)...
 [1/3 (w1)]: 2 => 2 (-), dim=2.0, hs=4.5
 [2/3 (#3)]: 5 => 4 (1 empty), dim=2.0, hs=5.5
  Removed: 3.11
 [3/3 (#1)]: 4 => 4 (-), dim=2.0, hs=4.8
 Savings: factor 1.25, 0 formulas
[2/3 (#3, w1)]: 4 * 2 = 8 => 2 (4 empty, 2 incl), dim=1.0, hs=4.5
[3/3 (#1, w2)]: 4 * 2 = 8 => 1 (7 empty), dim=0.0, hs=4.0
Total time: 0.013 sec, total intersections: 49, total inclusion tests: 88
Common plane time: 0.008 sec
Solutions: 1, dim=0.0, hs=4.0, max=2, maxin=8
f-vector: [1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.069 GiB, pagefile 0.062 GiB
Memory: pmem(rss=73707520, vms=66494464, num_page_faults=20863, peak_wset=73711616, wset=73707520, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40368, nonpaged_pool=40368, pagefile=66494464, peak_pagefile=66494464, private=66494464)
