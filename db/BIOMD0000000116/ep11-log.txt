
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:12:30+02:00
Command line: ..\trop\ptcut.py BIOMD0000000116 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000116 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 2TRPYA49M5WF5
Effective epsilon: 1/11.0

Reading db\BIOMD0000000116\vector_field_q.txt
Reading db\BIOMD0000000116\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 3 with: k14 + x3

Multiplying equation 6 with: k20 + x6
System is rational in variable x3,x6.
System is rational in parameter k8,k9,k14,k15,k17,k20.
System is radical in parameter k8,k9,k15,k17.
Term in equation 1 is zero since k13=0
Term in equation 1 is zero since k13=0
Term in equation 1 is zero since k13=0
Term in equation 1 is zero since k13=0
Term in equation 2 is zero since k13=0
Term in equation 2 is zero since k13=0
Term in equation 4 is zero since k13=0
Term in equation 4 is zero since k13=0
Term in equation 4 is zero since k13=0
Term in equation 4 is zero since k13=0
Term in equation 5 is zero since k13=0
Term in equation 5 is zero since k13=0
Tropicalization time: 1.961 sec
Variables: x1-x6
Saving tropical system...
Creating polyhedra... 
Creation time: 0.017 sec
Saving polyhedra...

Sorting ascending...
Dimension: 6
Formulas: 6
Order: 0 3 2 5 1 4
Possible combinations: 2 * 2 * 3 * 3 * 4 * 4 = 576 (10^3)
 Applying common planes (codim=0, hs=5)...
 [1/6 (#0)]: 2 => 2 (-), dim=5.0, hs=5.0
 [2/6 (#3)]: 2 => 2 (-), dim=5.0, hs=5.0
 [3/6 (#2)]: 3 => 3 (-), dim=5.0, hs=6.3
 [4/6 (#5)]: 3 => 3 (-), dim=5.0, hs=6.7
 [5/6 (#1)]: 4 => 3 (1 incl), dim=5.0, hs=5.7
  Removed: 1.3
 [6/6 (#4)]: 4 => 3 (1 incl), dim=5.0, hs=6.7
  Removed: 4.3
 Savings: factor 1.7777777777777777, 0 formulas
[1/5 (#0, #3)]: 2 * 2 = 4 => 4 (-), dim=4.0, hs=5.0
[2/5 (w1, #2)]: 4 * 3 = 12 => 12 (-), dim=3.0, hs=6.3
[3/5 (w2, #5)]: 12 * 3 = 36 => 16 (20 empty), dim=2.0, hs=7.0
[4/5 (w3, #1)]: 16 * 3 = 48 => 14 (19 empty, 15 incl), dim=2.0, hs=7.2
 Applying common planes (codim=0, hs=2)...
 [1/2 (w4)]: 14 => 14 (-), dim=2.0, hs=7.2
 [2/2 (#4)]: 3 => 3 (-), dim=5.0, hs=8.7
 Savings: factor 1.0, 0 formulas
[5/5 (w4, #4)]: 14 * 3 = 42 => 15 (13 empty, 14 incl), dim=2.0, hs=7.5
Total time: 0.050 sec, total intersections: 177, total inclusion tests: 869
Common plane time: 0.012 sec
Solutions: 15, dim=2.0, hs=7.5, max=16, maxin=48
f-vector: [0, 0, 15]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.062 GiB
Memory: pmem(rss=73220096, vms=66224128, num_page_faults=22280, peak_wset=73224192, wset=73220096, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40368, nonpaged_pool=40368, pagefile=66224128, peak_pagefile=66224128, private=66224128)
