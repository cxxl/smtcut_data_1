
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 6-4-6-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 >= 0    \ x1**-1 <= 1.0
  ie1: +1 x1 >= -3   \ x1 <= 1331.0
  eq0: +1 x4 = 0     \ x4 = 1.0
  eq1: +1 x3 = -3    \ x3 = 1331.0
  eq2: +1 x2 = 0     \ x2 = 1.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

\ combo: 6-0-6-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -2 x1 >= -1   \ x1**-1 <= 3.31662
  ie1: +1 x1 >= 0    \ x1 <= 1.0
  eq0: +1 x4 = 0     \ x4 = 1.0
  eq1: +1 x3 = -3    \ x3 = 1331.0
  eq2: +1 x2 = 0     \ x2 = 1.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

\ combo: 6-4-6-0-1
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x3 >= 1    \ x3**-1 <= 0.0909091
  ie1: +1 x3 >= -3   \ x3 <= 1331.0
  eq0: +1 x4 = 0     \ x4 = 1.0
  eq1: +1 x2 = 0     \ x2 = 1.0
  eq2: +1 x1 = -3    \ x1 = 1331.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

\ combo: 6-1-6-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -2 x2 >= -1   \ x2**-1 <= 3.31662
  ie1: +1 x2 >= 0    \ x2 <= 1.0
  eq0: +1 x4 = 0     \ x4 = 1.0
  eq1: +1 x3 = -3    \ x3 = 1331.0
  eq2: +1 x1 = 0     \ x1 = 1.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

