
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T21:50:06+02:00
Command line: BIOMD0000000491 --ppone --ppsmt


Model: BIOMD0000000491
Logfile: db\BIOMD0000000491\ep11-os-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^24.0 in 58 bags
Bag sizes: 1 3 2 1 1 3 7 2 3 2 2 2 5 4 4 4 2 10 2 11 2 5 1 1 1 1 1 3 3 7 5 5 3 2 3 2 2 2 2 2 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 2 14 57

Filtering bags...
Collecting...
Bag 1/50, 3 polyhedra
Bag 2/50, 2 polyhedra
Bag 3/50, 3 polyhedra
Bag 4/50, 7 polyhedra
Bag 5/50, 2 polyhedra
Bag 6/50, 3 polyhedra
Bag 7/50, 2 polyhedra
Bag 8/50, 2 polyhedra
Bag 9/50, 2 polyhedra
Bag 10/50, 5 polyhedra
Bag 11/50, 4 polyhedra
Bag 12/50, 4 polyhedra
Bag 13/50, 4 polyhedra
Bag 14/50, 2 polyhedra
Bag 15/50, 10 polyhedra
Bag 16/50, 2 polyhedra
Bag 17/50, 11 polyhedra
Bag 18/50, 2 polyhedra
Bag 19/50, 5 polyhedra
Bag 20/50, 3 polyhedra
Bag 21/50, 3 polyhedra
Bag 22/50, 7 polyhedra
Bag 23/50, 5 polyhedra
Bag 24/50, 5 polyhedra
Bag 25/50, 3 polyhedra
Bag 26/50, 2 polyhedra
Bag 27/50, 3 polyhedra
Bag 28/50, 2 polyhedra
Bag 29/50, 2 polyhedra
Bag 30/50, 2 polyhedra
Bag 31/50, 2 polyhedra
Bag 32/50, 2 polyhedra
Bag 33/50, 2 polyhedra
Bag 34/50, 2 polyhedra
Bag 35/50, 2 polyhedra
Bag 36/50, 2 polyhedra
Bag 37/50, 3 polyhedra
Bag 38/50, 2 polyhedra
Bag 39/50, 3 polyhedra
Bag 40/50, 2 polyhedra
Bag 41/50, 2 polyhedra
Bag 42/50, 2 polyhedra
Bag 43/50, 2 polyhedra
Bag 44/50, 2 polyhedra
Bag 45/50, 2 polyhedra
Bag 46/50, 2 polyhedra
Bag 47/50, 2 polyhedra
Bag 48/50, 2 polyhedra
Bag 49/50, 14 polyhedra
Bag 50/50, 57 polyhedra
Possible combinations after preprocessing: 10^24.0 in 51 bags
Bag sizes: 1 3 2 3 7 2 3 2 2 2 5 4 4 4 2 10 2 11 2 5 3 3 7 5 5 3 2 3 2 2 2 2 2 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 2 14 57
Time: 0.922 sec

Checking for superfluous polyhedra...
Bag 1/51, 57 polyhedra:
  Polyhedron 1... superfluous, 0.062 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.016 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.016 sec
  Polyhedron 11... superfluous, 0.016 sec
  Polyhedron 12... superfluous, 0.016 sec
  Polyhedron 13... superfluous, 0.016 sec
  Polyhedron 14... superfluous, 0.016 sec
  Polyhedron 15... superfluous, 0.016 sec
  Polyhedron 16... superfluous, 0.016 sec
  Polyhedron 17... superfluous, 0.016 sec
  Polyhedron 18... superfluous, 0.016 sec
  Polyhedron 19... superfluous, 0.016 sec
  Polyhedron 20... superfluous, 0.016 sec
  Polyhedron 21... superfluous, 0.016 sec
  Polyhedron 22... superfluous, 0.016 sec
  Polyhedron 23... superfluous, 0.016 sec
  Polyhedron 24... superfluous, 0.016 sec
  Polyhedron 25... superfluous, 0.016 sec
  Polyhedron 26... superfluous, 0.016 sec
  Polyhedron 27... superfluous, 0.016 sec
  Polyhedron 28... superfluous, 0.016 sec
  Polyhedron 29... superfluous, 0.016 sec
  Polyhedron 30... superfluous, 0.016 sec
  Polyhedron 31... superfluous, 0.016 sec
  Polyhedron 32... superfluous, 0.016 sec
  Polyhedron 33... superfluous, 0.016 sec
  Polyhedron 34... superfluous, 0.016 sec
  Polyhedron 35... superfluous, 0.016 sec
  Polyhedron 36... superfluous, 0.016 sec
  Polyhedron 37... superfluous, 0.016 sec
  Polyhedron 38... superfluous, 0.016 sec
  Polyhedron 39... superfluous, 0.016 sec
  Polyhedron 40... superfluous, 0.016 sec
  Polyhedron 41... superfluous, 0.016 sec
  Polyhedron 42... superfluous, 0.016 sec
  Polyhedron 43... superfluous, 0.000 sec
  Polyhedron 44... superfluous, 0.000 sec
  Polyhedron 45... superfluous, 0.016 sec
  Polyhedron 46... superfluous, 0.016 sec
  Polyhedron 47... superfluous, 0.016 sec
  Polyhedron 48... superfluous, 0.016 sec
  Polyhedron 49... superfluous, 0.016 sec
  Polyhedron 50... superfluous, 0.016 sec
  Polyhedron 51... superfluous, 0.016 sec
  Polyhedron 52... superfluous, 0.016 sec
  Polyhedron 53... superfluous, 0.016 sec
  Polyhedron 54... superfluous, 0.016 sec
  Polyhedron 55... superfluous, 0.016 sec
  Polyhedron 56... superfluous, 0.016 sec
  Polyhedron 57... required, 0.047 sec
  => 1 polyhedra left
Bag 2/51, 14 polyhedra:
  Polyhedron 1... superfluous, 0.031 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.031 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.031 sec
  Polyhedron 8... superfluous, 0.016 sec
  Polyhedron 9... superfluous, 0.031 sec
  Polyhedron 10... superfluous, 0.031 sec
  Polyhedron 11... superfluous, 0.016 sec
  Polyhedron 12... superfluous, 0.031 sec
  Polyhedron 13... superfluous, 0.031 sec
  Polyhedron 14... required, 0.047 sec
  => 1 polyhedra left
Bag 3/51, 11 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... required, 0.016 sec
  => 1 polyhedra left
Bag 4/51, 10 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.000 sec
  Polyhedron 10... required, 0.016 sec
  => 1 polyhedra left
Bag 5/51, 7 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... required, 0.016 sec
  => 1 polyhedra left
Bag 6/51, 7 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... required, 0.016 sec
  => 1 polyhedra left
Bag 7/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... required, 0.016 sec
  => 1 polyhedra left
Bag 8/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... required, 0.000 sec
  => 1 polyhedra left
Bag 9/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... required, 0.016 sec
  => 1 polyhedra left
Bag 10/51, 5 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... required, 0.016 sec
  => 1 polyhedra left
Bag 11/51, 4 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... required, 0.000 sec
  => 1 polyhedra left
Bag 12/51, 4 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... required, 0.000 sec
  => 1 polyhedra left
Bag 13/51, 4 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... required, 0.000 sec
  => 1 polyhedra left
Bag 14/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 15/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 16/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 17/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 18/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 19/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 20/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 21/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 22/51, 3 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... required, 0.000 sec
  => 1 polyhedra left
Bag 23/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 24/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 25/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 26/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 27/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 28/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 29/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 30/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 31/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 32/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 33/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 34/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 35/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 36/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 37/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 38/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 39/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 40/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 41/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 42/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 43/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 44/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 45/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 46/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 47/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 48/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 49/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Bag 50/51, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Possible combinations after preprocessing: 1 in 1 bags
Bag sizes: 1
Time: 2.109 sec
Trivial: one solution
Preprocessing 3.047 sec, super total 3.047 sec
Comparing... 0.125 sec.  Solution matches input.
Total contains() calls: 0, full: 0, ratio: 0.000%
