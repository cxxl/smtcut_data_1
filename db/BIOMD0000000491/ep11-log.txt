
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-03T22:38:13+02:00
Command line: ..\trop\ptcut.py BIOMD0000000491 -e11 --no-cache=n --maxruntime 3600 --maxmem 41393M --comment "concurrent=1, timeout=3600"

----------------------------------------------------------------------
Solving BIOMD0000000491 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=False, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=1, timeout=3600
Random seed: 3FLR9RJJSS9B0
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 57
Formulas: 58
Order: 0 3 4 22 23 24 25 26 2 7 9 10 11 16 18 20 33 35 36 37 38 39 40 41 42 43 45 47 48 49 50 51 52 53 54 55 1 5 8 27 28 32 34 44 46 13 14 15 12 21 30 31 6 29 17 19 56 57
Possible combinations: 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 4 * 4 * 4 * 5 * 5 * 5 * 5 * 7 * 7 * 10 * 11 * 14 * 57 = 909,039,634,252,981,862,400,000 (10^24)
 Applying common planes (codim=12, hs=66)...
 [1/50 (#2)]: 2 => 2 (-), dim=44.0, hs=64.0
 [2/50 (#7)]: 2 => 1 (1 incl), dim=45.0, hs=67.0
  Removed: 7.0
 [3/50 (#9)]: 2 => 1 (1 incl), dim=45.0, hs=67.0
  Removed: 9.0
 [4/50 (#10)]: 2 => 1 (1 incl), dim=45.0, hs=66.0
  Removed: 10.0
 [5/50 (#11)]: 2 => 1 (1 incl), dim=45.0, hs=66.0
  Removed: 11.0
 [6/50 (#16)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 16.1
 [7/50 (#18)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 18.1
 [8/50 (#20)]: 2 => 2 (-), dim=44.0, hs=66.5
 [9/50 (#33)]: 2 => 2 (-), dim=44.0, hs=66.0
 [10/50 (#35)]: 2 => 2 (-), dim=44.0, hs=66.0
 [11/50 (#36)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 36.1
 [12/50 (#37)]: 2 => 2 (-), dim=44.0, hs=66.0
 [13/50 (#38)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 38.1
 [14/50 (#39)]: 2 => 2 (-), dim=44.0, hs=66.0
 [15/50 (#40)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 40.1
 [16/50 (#41)]: 2 => 2 (-), dim=44.0, hs=66.0
 [17/50 (#42)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 42.1
 [18/50 (#43)]: 2 => 2 (-), dim=44.0, hs=66.0
 [19/50 (#45)]: 2 => 2 (-), dim=44.0, hs=66.0
 [20/50 (#47)]: 2 => 2 (-), dim=44.0, hs=66.0
 [21/50 (#48)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 48.1
 [22/50 (#49)]: 2 => 2 (-), dim=44.0, hs=66.0
 [23/50 (#50)]: 2 => 1 (1 incl), dim=44.0, hs=66.0
  Removed: 50.1
 [24/50 (#51)]: 2 => 2 (-), dim=44.0, hs=66.0
 [25/50 (#52)]: 2 => 2 (-), dim=44.0, hs=65.5
 [26/50 (#53)]: 2 => 2 (-), dim=44.0, hs=65.5
 [27/50 (#54)]: 2 => 2 (-), dim=44.0, hs=65.5
 [28/50 (#55)]: 2 => 2 (-), dim=44.0, hs=65.5
 [29/50 (#1)]: 3 => 1 (2 incl), dim=45.0, hs=66.0
  Removed: 1.0,1.2
 [30/50 (#5)]: 3 => 1 (2 incl), dim=45.0, hs=66.0
  Removed: 5.1,5.2
 [31/50 (#8)]: 3 => 1 (2 incl), dim=43.0, hs=65.0
  Removed: 8.0,8.2
 [32/50 (#27)]: 3 => 3 (-), dim=44.0, hs=67.3
 [33/50 (#28)]: 3 => 1 (2 incl), dim=44.0, hs=67.0
  Removed: 28.1,28.2
 [34/50 (#32)]: 3 => 1 (2 incl), dim=44.0, hs=68.0
  Removed: 32.1,32.2
 [35/50 (#34)]: 3 => 1 (2 incl), dim=44.0, hs=67.0
  Removed: 34.1,34.2
 [36/50 (#44)]: 3 => 1 (2 incl), dim=44.0, hs=67.0
  Removed: 44.1,44.2
 [37/50 (#46)]: 3 => 1 (2 incl), dim=44.0, hs=67.0
  Removed: 46.1,46.2
 [38/50 (#13)]: 4 => 1 (3 incl), dim=44.0, hs=66.0
  Removed: 13.1,13.2,13.3
 [39/50 (#14)]: 4 => 4 (-), dim=44.0, hs=67.5
 [40/50 (#15)]: 4 => 3 (1 incl), dim=44.0, hs=66.7
  Removed: 15.3
 [41/50 (#12)]: 5 => 1 (4 incl), dim=44.0, hs=66.0
  Removed: 12.1,12.2,12.3,12.4
 [42/50 (#21)]: 5 => 5 (-), dim=44.0, hs=68.4
 [43/50 (#30)]: 5 => 1 (4 incl), dim=44.0, hs=69.0
  Removed: 30.0,30.2,30.3,30.4
 [44/50 (#31)]: 5 => 5 (-), dim=44.0, hs=69.2
 [45/50 (#6)]: 7 => 1 (6 incl), dim=43.0, hs=65.0
  Removed: 6.0,6.1,6.3,6.4,6.5,6.6
 [46/50 (#29)]: 7 => 1 (6 incl), dim=43.0, hs=69.0
  Removed: 29.0,29.2,29.3,29.4,29.5,29.6
 [47/50 (#17)]: 10 => 1 (9 incl), dim=44.0, hs=66.0
  Removed: 17.0,17.2,17.3,17.4,17.5,17.6,17.7,17.8,17.9
 [48/50 (#19)]: 11 => 1 (10 incl), dim=38.0, hs=64.0
  Removed: 19.1,19.2,19.3,19.4,19.5,19.6,19.7,19.8,19.9,19.10
 [49/50 (#56)]: 14 => 14 (-), dim=44.0, hs=65.7
 [50/50 (#57)]: 57 => 24 (33 incl), dim=44.0, hs=65.7
  Removed: 57.6,57.8,57.10,57.12,57.14,57.16,57.18,57.20,57.22,57.24,57.26,57.27,57.28,57.30,57.31,57.32,57.33,57.34,57.37,57.38,57.39,57.40,57.43,57.44,57.46,57.47,57.48,57.50,57.51,57.52,57.53,57.55,57.56
 Applying common planes (codim=49, hs=57)...
 [1/23 (#2)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 2.1
 [2/23 (#20)]: 2 => 1 (1 incl), dim=7.0, hs=57.0
  Removed: 20.1
 [3/23 (#33)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 33.0
 [4/23 (#35)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 35.0
 [5/23 (#37)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 37.0
 [6/23 (#39)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 39.0
 [7/23 (#41)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 41.0
 [8/23 (#43)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 43.0
 [9/23 (#45)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 45.0
 [10/23 (#47)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 47.0
 [11/23 (#49)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 49.0
 [12/23 (#51)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 51.0
 [13/23 (#52)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 52.0
 [14/23 (#53)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 53.0
 [15/23 (#54)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 54.0
 [16/23 (#55)]: 2 => 1 (1 incl), dim=8.0, hs=57.0
  Removed: 55.0
 [17/23 (#27)]: 3 => 1 (2 incl), dim=7.0, hs=57.0
  Removed: 27.1,27.2
 [18/23 (#14)]: 4 => 1 (3 incl), dim=7.0, hs=57.0
  Removed: 14.0,14.1,14.3
 [19/23 (#15)]: 3 => 1 (2 incl), dim=7.0, hs=57.0
  Removed: 15.0,15.1
 [20/23 (#21)]: 5 => 1 (4 incl), dim=7.0, hs=57.0
  Removed: 21.0,21.2,21.3,21.4
 [21/23 (#31)]: 5 => 1 (4 incl), dim=7.0, hs=57.0
  Removed: 31.1,31.2,31.3,31.4
 [22/23 (#56)]: 14 => 1 (13 incl), dim=7.0, hs=57.0
  Removed: 56.1,56.2,56.3,56.4,56.5,56.6,56.7,56.8,56.9,56.10,56.11,56.12,56.13
 [23/23 (#57)]: 24 => 2 (22 incl), dim=7.0, hs=57.0
  Removed: 57.1,57.2,57.3,57.4,57.5,57.7,57.9,57.11,57.13,57.15,57.17,57.19,57.21,57.23,57.25,57.29,57.35,57.36,57.41,57.42,57.45,57.54
 Applying common planes (codim=56, hs=57)...
 [1/1 (#57)]: 2 => 1 (1 incl), dim=0.0, hs=57.0
  Removed: 57.0
 Applying common planes (codim=57, hs=57)...
 Savings: factor 909,039,634,252,981,862,400,000 (10^24), 57 formulas
Total time: 17.244 sec, total intersections: 313, total inclusion tests: 1,233
Common plane time: 17.243 sec
Solutions: 1, dim=0.0, hs=57.0, max=1, maxin=0
f-vector: [1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.616 GiB, pagefile 0.645 GiB
Memory: pmem(rss=74522624, vms=69718016, num_page_faults=167441, peak_wset=661741568, wset=74522624, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=63760, nonpaged_pool=44720, pagefile=69718016, peak_pagefile=692264960, private=69718016)
