
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T19:44:05+02:00
Command line: BIOMD0000000491


Model: BIOMD0000000491
Logfile: db\BIOMD0000000491\ep11-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^24.0 in 58 bags
Bag sizes: 1 3 2 1 1 3 7 2 3 2 2 2 5 4 4 4 2 10 2 11 2 5 1 1 1 1 1 3 3 7 5 5 3 2 3 2 2 2 2 2 2 2 2 2 3 2 3 2 2 2 2 2 2 2 2 2 14 57

Minimizing (8,0)... (8,0)


Running SMT solver (1)... 0.078 sec
Checking... Minimizing (57,171)... (57,0), 0.109 sec
Inserting... 0.000 sec
[1 ph, mem 55/48, smt 0.078, isect 0.109, ins 0.000, tot 0.297]

Running SMT solver (2)... 0.062 sec
No more solutions found

1 polyhedra, 2 rounds, smt 0.141 sec, isect 0.109 sec, insert 0.000 sec, total 0.359 sec
Preprocessing 0.047 sec, super total 0.406 sec
Comparing... 0.109 sec.  Solution matches input.
Total contains() calls: 0, full: 0, ratio: 0.000%
