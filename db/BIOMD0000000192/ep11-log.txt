
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:14:37+02:00
Command line: ..\trop\ptcut.py BIOMD0000000192 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000192 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 1S5EHGA1TWPHF
Effective epsilon: 1/11.0

Reading db\BIOMD0000000192\vector_field_q.txt
Reading db\BIOMD0000000192\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 10 with: k16 + x12

Multiplying equation 11 with: k18 + x11

Multiplying equation 12 with: k16 + x12

Multiplying equation 13 with: k16*k18 + k16*x11 + k18*x12 + x11*x12

System is rational in variable x11,x12.
System is rational in parameter k2,k16,k18.
Tropicalization time: 2.973 sec
Variables: x1, x2, x4-x6, x8-x13
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 0.109 sec
Saving polyhedra...

Sorting ascending...
Dimension: 11
Formulas: 14
Order: 11 1 3 4 10 0 2 5 6 8 12 13 9 7
Possible combinations: 1 * 2 * 2 * 2 * 2 * 4 * 4 * 4 * 4 * 4 * 4 * 8 * 9 * 12 = 56,623,104 (10^8)
 Applying common planes (codim=1, hs=14)...
 [1/13 (#1)]: 2 => 2 (-), dim=9.0, hs=13.0
 [2/13 (#3)]: 2 => 2 (-), dim=9.0, hs=13.5
 [3/13 (#4)]: 2 => 2 (-), dim=9.0, hs=14.0
 [4/13 (#10)]: 2 => 2 (-), dim=9.0, hs=14.0
 [5/13 (#0)]: 4 => 3 (1 incl), dim=9.0, hs=13.7
  Removed: 0.2
 [6/13 (#2)]: 4 => 2 (2 incl), dim=9.0, hs=11.5
  Removed: 2.1,2.2
 [7/13 (#5)]: 4 => 3 (1 incl), dim=9.0, hs=13.7
  Removed: 5.2
 [8/13 (#6)]: 4 => 1 (3 empty), dim=9.0, hs=14.0
  Removed: 6.0,6.2,6.5
 [9/13 (#8)]: 4 => 1 (3 empty), dim=9.0, hs=14.0
  Removed: 8.0,8.2,8.5
 [10/13 (#12)]: 4 => 3 (1 empty), dim=9.0, hs=12.3
  Removed: 12.0
 [11/13 (#13)]: 8 => 5 (3 empty), dim=9.0, hs=13.6
  Removed: 13.4,13.5,13.6
 [12/13 (#9)]: 9 => 3 (6 empty), dim=9.0, hs=14.0
  Removed: 9.0,9.5,9.11,9.12,9.17,9.27
 [13/13 (#7)]: 12 => 2 (10 empty), dim=9.0, hs=14.5
  Removed: 7.1,7.3,7.4,7.6,7.9,7.11,7.12,7.14,7.17,7.19
 Applying common planes (codim=2, hs=16)...
 [1/11 (#1)]: 2 => 2 (-), dim=8.0, hs=15.5
 [2/11 (#3)]: 2 => 2 (-), dim=8.0, hs=15.0
 [3/11 (#4)]: 2 => 2 (-), dim=8.0, hs=14.5
 [4/11 (#10)]: 2 => 1 (1 empty), dim=8.0, hs=16.0
  Removed: 10.0
 [5/11 (#0)]: 3 => 3 (-), dim=8.0, hs=16.7
 [6/11 (#2)]: 2 => 2 (-), dim=8.0, hs=13.5
 [7/11 (#5)]: 3 => 3 (-), dim=8.0, hs=16.0
 [8/11 (#12)]: 3 => 3 (-), dim=7.3, hs=14.3
 [9/11 (#13)]: 5 => 4 (1 empty), dim=8.0, hs=15.8
  Removed: 13.1
 [10/11 (#9)]: 3 => 2 (1 empty), dim=8.0, hs=15.0
  Removed: 9.2
 [11/11 (#7)]: 2 => 1 (1 empty), dim=8.0, hs=15.0
  Removed: 7.2
 Applying common planes (codim=4, hs=15)...
 [1/9 (#1)]: 2 => 2 (-), dim=6.0, hs=14.5
 [2/9 (#3)]: 2 => 2 (-), dim=6.0, hs=14.0
 [3/9 (#4)]: 2 => 2 (-), dim=6.0, hs=13.5
 [4/9 (#0)]: 3 => 3 (-), dim=6.0, hs=15.7
 [5/9 (#2)]: 2 => 2 (-), dim=6.0, hs=12.5
 [6/9 (#5)]: 3 => 2 (1 empty), dim=6.0, hs=15.0
  Removed: 5.3
 [7/9 (#12)]: 3 => 3 (-), dim=5.3, hs=13.3
 [8/9 (#13)]: 4 => 3 (1 empty), dim=6.0, hs=15.0
  Removed: 13.2
 [9/9 (#9)]: 2 => 2 (-), dim=6.0, hs=15.0
 Savings: factor 32,768 (10^5), 5 formulas
[1/8 (#1, #3)]: 2 * 2 = 4 => 4 (-), dim=5.0, hs=13.2
[2/8 (w1, #4)]: 4 * 2 = 8 => 4 (4 empty), dim=4.0, hs=11.5
[3/8 (w2, #2)]: 4 * 2 = 8 => 4 (2 empty, 2 incl), dim=4.0, hs=11.5
[4/8 (w3, #5)]: 4 * 2 = 8 => 4 (4 incl), dim=3.8, hs=12.2
[5/8 (w4, #9)]: 4 * 2 = 8 => 4 (2 empty, 2 incl), dim=2.8, hs=12.2
[6/8 (w5, #0)]: 4 * 3 = 12 => 3 (3 empty, 6 incl), dim=2.7, hs=12.0
 Applying common planes (codim=1, hs=1)...
 [1/3 (w6)]: 3 => 3 (-), dim=2.7, hs=12.0
 [2/3 (#12)]: 3 => 2 (1 incl), dim=5.0, hs=13.5
  Removed: 12.3
 [3/3 (#13)]: 3 => 3 (-), dim=5.0, hs=14.3
 Savings: factor 1.5, 0 formulas
[7/8 (w6, #12)]: 3 * 2 = 6 => 2 (2 empty, 2 incl), dim=2.0, hs=12.0
 Applying common planes (codim=5, hs=7)...
 [1/2 (w7)]: 2 => 2 (-), dim=2.0, hs=12.0
 [2/2 (#13)]: 3 => 3 (-), dim=3.0, hs=12.3
 Savings: factor 1.0, 0 formulas
[8/8 (#13, w7)]: 3 * 2 = 6 => 2 (4 incl), dim=1.0, hs=12.0
Total time: 0.116 sec, total intersections: 187, total inclusion tests: 289
Common plane time: 0.081 sec
Solutions: 2, dim=1.0, hs=12.0, max=4, maxin=12
f-vector: [0, 2]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.070 GiB, pagefile 0.064 GiB
Memory: pmem(rss=74186752, vms=67043328, num_page_faults=21457, peak_wset=75382784, wset=74186752, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40776, nonpaged_pool=40776, pagefile=67043328, peak_pagefile=68546560, private=67043328)
