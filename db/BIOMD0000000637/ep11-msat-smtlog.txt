
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T19:44:13+02:00
Command line: BIOMD0000000637


Model: BIOMD0000000637
Logfile: db\BIOMD0000000637\ep11-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^12.3 in 15 bags
Bag sizes: 12 12 9 12 16 3 9 2 3 8 4 6 4 9 8

Minimizing (0,0)... (0,0)


Running SMT solver (1)... 0.031 sec
Checking... Minimizing (11,50)... (10,10), 0.031 sec
Inserting... 0.000 sec
[1 ph, mem 48/41, smt 0.031, isect 0.031, ins 0.000, tot 0.141]

Running SMT solver (2)... 0.000 sec
Checking... Minimizing (13,52)... (12,0), 0.031 sec
Inserting... 0.000 sec
[2 ph, mem 52/45, smt 0.031, isect 0.062, ins 0.000, tot 0.172]

Running SMT solver (3)... 0.000 sec
Checking... Minimizing (13,49)... (11,5), 0.031 sec
Inserting... 0.000 sec
[2 ph, mem 56/49, smt 0.031, isect 0.094, ins 0.000, tot 0.203]

Running SMT solver (4)... 0.016 sec
Checking... Minimizing (11,50)... (10,11), 0.016 sec
Inserting... 0.016 sec
[2 ph, mem 60/52, smt 0.047, isect 0.109, ins 0.016, tot 0.250]

Running SMT solver (5)... 0.000 sec
Checking... Minimizing (9,49)... (9,14), 0.031 sec
Inserting... 0.000 sec
[2 ph, mem 64/57, smt 0.047, isect 0.141, ins 0.016, tot 0.281]

Running SMT solver (6)... 0.016 sec
Checking... Minimizing (8,50)... (8,22), 0.031 sec
Inserting... 0.016 sec
[2 ph, mem 56/49, smt 0.062, isect 0.172, ins 0.031, tot 0.344]

Running SMT solver (7)... 0.016 sec
Checking... Minimizing (8,50)... (8,20), 0.031 sec
Inserting... 0.000 sec
[2 ph, mem 60/53, smt 0.078, isect 0.203, ins 0.031, tot 0.391]

Running SMT solver (8)... 0.016 sec
Checking... Minimizing (8,52)... (8,18), 0.031 sec
Inserting... 0.000 sec
[3 ph, mem 61/54, smt 0.094, isect 0.234, ins 0.031, tot 0.438]

Running SMT solver (9)... 0.016 sec
Checking... Minimizing (9,50)... (9,12), 0.016 sec
Inserting... 0.000 sec
[4 ph, mem 63/56, smt 0.109, isect 0.250, ins 0.031, tot 0.484]

Running SMT solver (10)... 0.000 sec
Checking... Minimizing (9,50)... (9,11), 0.031 sec
Inserting... 0.000 sec
[5 ph, mem 66/59, smt 0.109, isect 0.281, ins 0.031, tot 0.516]

Running SMT solver (11)... 0.000 sec
Checking... Minimizing (8,50)... (8,18), 0.047 sec
Inserting... 0.000 sec
[5 ph, mem 59/52, smt 0.109, isect 0.328, ins 0.031, tot 0.562]

Running SMT solver (12)... 0.016 sec
Checking... Minimizing (10,52)... (9,13), 0.031 sec
Inserting... 0.000 sec
[6 ph, mem 61/54, smt 0.125, isect 0.359, ins 0.031, tot 0.609]

Running SMT solver (13)... 0.000 sec
Checking... Minimizing (9,52)... (8,21), 0.031 sec
Inserting... 0.016 sec
[6 ph, mem 64/58, smt 0.125, isect 0.391, ins 0.047, tot 0.656]

Running SMT solver (14)... 0.016 sec
Checking... Minimizing (12,52)... (10,12), 0.031 sec
Inserting... 0.000 sec
[7 ph, mem 67/60, smt 0.141, isect 0.422, ins 0.047, tot 0.703]

Running SMT solver (15)... 0.000 sec
Checking... Minimizing (12,52)... (10,10), 0.031 sec
Inserting... 0.000 sec
[8 ph, mem 69/62, smt 0.141, isect 0.453, ins 0.047, tot 0.734]

Running SMT solver (16)... 0.016 sec
Checking... Minimizing (9,52)... (8,18), 0.031 sec
Inserting... 0.000 sec
[8 ph, mem 63/56, smt 0.156, isect 0.484, ins 0.047, tot 0.797]

Running SMT solver (17)... 0.000 sec
Checking... Minimizing (9,52)... (8,18), 0.031 sec
Inserting... 0.016 sec
[8 ph, mem 68/61, smt 0.156, isect 0.516, ins 0.062, tot 0.844]

Running SMT solver (18)... 0.000 sec
Checking... Minimizing (9,52)... (9,16), 0.031 sec
Inserting... 0.000 sec
[9 ph, mem 68/61, smt 0.156, isect 0.547, ins 0.062, tot 0.891]

Running SMT solver (19)... 0.000 sec
Checking... Minimizing (9,52)... (9,12), 0.031 sec
Inserting... 0.000 sec
[10 ph, mem 71/63, smt 0.156, isect 0.578, ins 0.062, tot 0.922]

Running SMT solver (20)... 0.016 sec
Checking... Minimizing (11,51)... (10,7), 0.031 sec
Inserting... 0.000 sec
[11 ph, mem 73/65, smt 0.172, isect 0.609, ins 0.062, tot 0.969]

Running SMT solver (21)... 0.016 sec
Checking... Minimizing (11,51)... (10,12), 0.031 sec
Inserting... 0.000 sec
[11 ph, mem 66/59, smt 0.188, isect 0.641, ins 0.062, tot 1.016]

Running SMT solver (22)... 0.000 sec
Checking... Minimizing (11,51)... (10,12), 0.016 sec
Inserting... 0.016 sec
[12 ph, mem 67/60, smt 0.188, isect 0.656, ins 0.078, tot 1.062]

Running SMT solver (23)... 0.000 sec
No more solutions found

12 polyhedra, 23 rounds, smt 0.188 sec, isect 0.656 sec, insert 0.078 sec, total 1.062 sec
Preprocessing 0.000 sec, super total 1.062 sec
Comparing... 0.109 sec.  Solution matches input.
Total contains() calls: 114, full: 11, ratio: 9.649%
