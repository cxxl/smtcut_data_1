
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:32:07+02:00
Command line: ..\trop\ptcut.py BIOMD0000000517 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000517 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 1SM18VN2GGZK9
Effective epsilon: 1/11.0

Reading db\BIOMD0000000517\vector_field_q.txt
Reading db\BIOMD0000000517\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k16*k18 + k16*x4 + k18*x4 + x4**2

Multiplying equation 2 with: k19 + x4

Multiplying equation 3 with: k20 + x4

Multiplying equation 4 with: k16 + x4
System is rational in variable x4.
System is rational in parameter k5,k16,k18,k19,k20,k21.
Tropicalization time: 8.604 sec
Variables: x1-x4
Saving tropical system...
Creating polyhedra... 
Creation time: 1.023 sec
Saving polyhedra...

Sorting ascending...
Dimension: 4
Formulas: 4
Order: 3 2 0 1
Possible combinations: 5 * 9 * 30 * 32 = 43,200 (10^5)
 Applying common planes (codim=0, hs=3)...
 [1/4 (#3)]: 5 => 5 (-), dim=3.0, hs=4.6
 [2/4 (#2)]: 9 => 9 (-), dim=3.0, hs=5.0
 [3/4 (#0)]: 30 => 12 (18 incl), dim=3.0, hs=4.5
  Removed: 0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.20,0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29
 [4/4 (#1)]: 32 => 16 (16 incl), dim=3.0, hs=4.8
  Removed: 1.33,1.35,1.37,1.39,1.41,1.14,1.15,1.16,1.18,1.20,1.22,1.26,1.27,1.28,1.29,1.30
 Savings: factor 5.0, 0 formulas
[1/3 (#2, #3)]: 9 * 5 = 45 => 8 (10 empty, 27 incl), dim=2.8, hs=5.2
 Applying common planes (codim=0, hs=1)...
 [1/3 (w1)]: 8 => 8 (-), dim=2.8, hs=5.2
 [2/3 (#0)]: 12 => 12 (-), dim=3.0, hs=4.5
 [3/3 (#1)]: 16 => 15 (1 incl), dim=3.0, hs=4.8
  Removed: 1.31
 Savings: factor 1.0666666666666667, 0 formulas
[2/3 (#0, w1)]: 12 * 8 = 96 => 11 (33 empty, 52 incl), dim=2.6, hs=5.5
[3/3 (#1, w2)]: 15 * 11 = 165 => 20 (98 empty, 47 incl), dim=2.7, hs=5.6
Total time: 0.077 sec, total intersections: 418, total inclusion tests: 2,085
Common plane time: 0.031 sec
Solutions: 20, dim=2.7, hs=5.6, max=20, maxin=165
f-vector: [2, 0, 0, 18]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.070 GiB, pagefile 0.064 GiB
Memory: pmem(rss=74944512, vms=68001792, num_page_faults=21530, peak_wset=75239424, wset=74944512, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=41048, nonpaged_pool=41048, pagefile=68001792, peak_pagefile=68419584, private=68001792)
