
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 1-1-0-0
\ dimension 0; is compact
MAXIMIZE
Subject To
  eq0: +1 x4 = 1   \ x4 = 0.0909091
  eq1: +1 x3 = 2   \ x3 = 0.00826446
  eq2: +1 x2 = 3   \ x2 = 0.000751315
  eq3: +1 x1 = 2   \ x1 = 0.00826446
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

\ combo: 0-1-0-0
\ dimension 0; is compact
MAXIMIZE
Subject To
  eq0: +1 x4 = 0   \ x4 = 1.0
  eq1: +1 x3 = 1   \ x3 = 0.0909091
  eq2: +1 x2 = 2   \ x2 = 0.00826446
  eq3: +1 x1 = 1   \ x1 = 0.0909091
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

\ combo: 6-24-7-2
\ dimension 3; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 +2 x4 >= -1   \ x1**-1 * x4**2 <= 11.0
  ie1: -1 x1 +1 x2 >= 1    \ x1**-1 * x2 <= 0.0909091
  ie2: -1 x2 -1 x4 >= 1    \ x2**-1 * x4**-1 <= 0.0909091
  ie3: -1 x4 >= 1          \ x4**-1 <= 0.0909091
  ie4: +1 x1 >= -2         \ x1 <= 121.0
  eq0: +1 x3 = -2          \ x3 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

\ combo: 6-12-7-2
\ dimension 3; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 +1 x2 >= 1   \ x1**-1 * x2 <= 0.0909091
  ie1: -1 x2 -1 x4 >= 1   \ x2**-1 * x4**-1 <= 0.0909091
  ie2: +1 x1 >= -2        \ x1 <= 121.0
  ie3: +1 x4 >= -1        \ x4 <= 11.0
  eq0: +1 x3 = -2         \ x3 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

\ combo: 6-2-7-2
\ dimension 3; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 +2 x4 >= -1   \ x1**-1 * x4**2 <= 11.0
  ie1: -1 x2 -1 x4 >= 1    \ x2**-1 * x4**-1 <= 0.0909091
  ie2: -1 x4 >= 0          \ x4**-1 <= 1.0
  ie3: +1 x1 >= -2         \ x1 <= 121.0
  ie4: +1 x4 >= -1         \ x4 <= 11.0
  ie5: +1 x2 >= -2         \ x2 <= 121.0
  ie6: +1 x1 -1 x2 >= -1   \ x1 * x2**-1 <= 11.0
  eq0: +1 x3 = -2          \ x3 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

\ combo: 9-2-1-2
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x1 +1 x4 >= -1   \ x1**-1 * x4 <= 11.0
  ie1: -1 x2 >= 1          \ x2**-1 <= 0.0909091
  ie2: +1 x1 >= -2         \ x1 <= 121.0
  ie3: +1 x4 >= 1          \ x4 <= 0.0909091
  ie4: +1 x2 >= -2         \ x2 <= 121.0
  eq0: +1 x3 = -2          \ x3 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 6

\ combo: 2-2-1-2
\ dimension 3; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 +1 x4 >= -1   \ x1**-1 * x4 <= 11.0
  ie1: -1 x2 >= 1          \ x2**-1 <= 0.0909091
  ie2: -1 x4 >= -1         \ x4**-1 <= 11.0
  ie3: +1 x1 >= -2         \ x1 <= 121.0
  ie4: +1 x4 >= 0          \ x4 <= 1.0
  ie5: +1 x2 >= -2         \ x2 <= 121.0
  eq0: +1 x3 = -2          \ x3 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 7

\ combo: 6-9-7-2
\ dimension 3; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 +2 x4 >= -1   \ x1**-1 * x4**2 <= 11.0
  ie1: -1 x4 >= 1          \ x4**-1 <= 0.0909091
  ie2: +1 x1 >= -2         \ x1 <= 121.0
  ie3: +1 x2 >= -2         \ x2 <= 121.0
  ie4: +1 x1 -1 x2 >= -1   \ x1 * x2**-1 <= 11.0
  eq0: +1 x3 = -2          \ x3 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 8

\ combo: 7-10-8-3
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x1 +2 x4 >= -1   \ x1**-1 * x4**2 <= 11.0
  ie1: -1 x4 >= 1          \ x4**-1 <= 0.0909091
  ie2: +1 x1 >= -2         \ x1 <= 121.0
  ie3: +1 x3 >= -2         \ x3 <= 121.0
  eq0: +1 x2 = -2          \ x2 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 9

\ combo: 10-3-2-3
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x1 +1 x4 >= -1   \ x1**-1 * x4 <= 11.0
  ie1: +1 x4 >= 1          \ x4 <= 0.0909091
  ie2: +1 x3 >= -2         \ x3 <= 121.0
  ie3: +1 x1 >= -2         \ x1 <= 121.0
  eq0: +1 x2 = -2          \ x2 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 10

\ combo: 7-3-8-3
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x1 +2 x4 >= -1   \ x1**-1 * x4**2 <= 11.0
  ie1: -1 x4 >= 0          \ x4**-1 <= 1.0
  ie2: +1 x1 >= -2         \ x1 <= 121.0
  ie3: +1 x4 >= -1         \ x4 <= 11.0
  ie4: +1 x3 >= -2         \ x3 <= 121.0
  eq0: +1 x2 = -2          \ x2 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 11

\ combo: 3-3-2-3
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x1 +1 x4 >= -1   \ x1**-1 * x4 <= 11.0
  ie1: -1 x4 >= -1         \ x4**-1 <= 11.0
  ie2: +1 x1 >= -2         \ x1 <= 121.0
  ie3: +1 x4 >= 0          \ x4 <= 1.0
  ie4: +1 x3 >= -2         \ x3 <= 121.0
  eq0: +1 x2 = -2          \ x2 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 12

\ combo: 8-11-10-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 >= 1    \ x2**-1 <= 0.0909091
  ie1: -1 x4 >= 1    \ x4**-1 <= 0.0909091
  ie2: +1 x2 >= -2   \ x2 <= 121.0
  ie3: +2 x4 >= -3   \ x4 <= 36.4829
  ie4: +1 x3 >= -2   \ x3 <= 121.0
  eq0: +1 x1 = -2    \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 13

\ combo: 11-4-3-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 >= 1    \ x2**-1 <= 0.0909091
  ie1: +1 x2 >= -2   \ x2 <= 121.0
  ie2: +1 x4 >= 1    \ x4 <= 0.0909091
  ie3: +1 x3 >= -2   \ x3 <= 121.0
  eq0: +1 x1 = -2    \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 14

\ combo: 8-4-10-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 >= 1    \ x2**-1 <= 0.0909091
  ie1: -1 x4 >= 0    \ x4**-1 <= 1.0
  ie2: +1 x2 >= -2   \ x2 <= 121.0
  ie3: +1 x4 >= -1   \ x4 <= 11.0
  ie4: +1 x3 >= -2   \ x3 <= 121.0
  eq0: +1 x1 = -2    \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 15

\ combo: 4-4-3-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 >= 1    \ x2**-1 <= 0.0909091
  ie1: -1 x4 >= -1   \ x4**-1 <= 11.0
  ie2: +1 x2 >= -2   \ x2 <= 121.0
  ie3: +1 x4 >= 0    \ x4 <= 1.0
  ie4: +1 x3 >= -2   \ x3 <= 121.0
  eq0: +1 x1 = -2    \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 16

\ combo: 8-25-10-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 +1 x3 -1 x4 >= -1   \ x2**-1 * x3 * x4**-1 <= 11.0
  ie1: -1 x4 >= 1                \ x4**-1 <= 0.0909091
  ie2: +1 x2 >= -1               \ x2 <= 11.0
  ie3: +2 x4 >= -3               \ x4 <= 36.4829
  ie4: +1 x3 >= -2               \ x3 <= 121.0
  eq0: +1 x1 = -2                \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 17

\ combo: 11-13-3-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 +1 x3 >= -1   \ x2**-1 * x3 <= 11.0
  ie1: +1 x4 >= 1          \ x4 <= 0.0909091
  ie2: +1 x2 >= -1         \ x2 <= 11.0
  eq0: +1 x1 = -2          \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 18

\ combo: 8-13-10-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 +1 x3 -1 x4 >= -1   \ x2**-1 * x3 * x4**-1 <= 11.0
  ie1: -1 x4 >= 0                \ x4**-1 <= 1.0
  ie2: +1 x2 >= -1               \ x2 <= 11.0
  ie3: +1 x4 >= -1               \ x4 <= 11.0
  ie4: +1 x3 >= -2               \ x3 <= 121.0
  eq0: +1 x1 = -2                \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 19

\ combo: 4-13-3-4
\ dimension 3; is not compact
MAXIMIZE
Subject To
  ie0: -1 x2 +1 x3 >= -1   \ x2**-1 * x3 <= 11.0
  ie1: -1 x4 >= -1         \ x4**-1 <= 11.0
  ie2: +1 x2 >= -1         \ x2 <= 11.0
  ie3: +1 x4 >= 0          \ x4 <= 1.0
  eq0: +1 x1 = -2          \ x1 = 121.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

