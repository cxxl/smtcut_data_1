
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-03T21:23:53+02:00
Command line: ..\..\trop\ptcut.py BIOMD0000000105 -e11 --no-cache=n --maxruntime 3600 --maxmem 3838M --comment "concurrent=11, timeout=3600"

----------------------------------------------------------------------
Solving BIOMD0000000105 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=False, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=11, timeout=3600
Random seed: 36J1WCDF49RPY
Effective epsilon: 1/11.0

Reading db\BIOMD0000000105\vector_field_q.txt
Reading db\BIOMD0000000105\conservation_laws_q.txt
Computing tropical system... 
System is rational in parameter k18,k19.
System is radical in parameter k18,k19.
Term in equation 3 is zero since k13=0
Term in equation 3 is zero since k13=0
Term in equation 3 is zero since k13=0
Term in equation 3 is zero since k13=0
Term in equation 3 is zero since k13=0
Term in equation 8 is zero since k13=0
Term in equation 8 is zero since k13=0
Term in equation 8 is zero since k13=0
Term in equation 8 is zero since k13=0
Term in equation 8 is zero since k13=0
Term in equation 21 is zero since k13=0
Term in equation 22 is zero since k13=0
Term in equation 23 is zero since k13=0
Term in equation 24 is zero since k13=0
Term in equation 25 is zero since k13=0
Term in equation 30 is zero since k13=0
Equation 30 is zero, ignored.
Term in equation 31 is zero since k13=0
Equation 31 is zero, ignored.
Term in equation 32 is zero since k13=0
Equation 32 is zero, ignored.
Term in equation 33 is zero since k13=0
Equation 33 is zero, ignored.
Term in equation 34 is zero since k13=0
Equation 34 is zero, ignored.
Tropicalization time: 37.058 sec
Variables: x1-x25, x37, x39
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 16.038 sec
Saving polyhedra...

Sorting ascending...
Dimension: 27
Formulas: 30
Order: 3 7 22 24 26 0 5 9 18 19 20 21 27 28 29 6 25 4 8 2 17 11 10 1 13 14 15 16 23 12
Possible combinations: 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 6 * 7 * 8 * 8 * 13 * 24 * 34 * 35 * 36 * 37 * 37 * 37 * 37 * 45 * 46 = 142,728,439,960,110,838,579,200 (10^23)
 Applying common planes (codim=4, hs=41)...
 [1/25 (#0)]: 2 => 2 (-), dim=22.0, hs=43.0
 [2/25 (#5)]: 2 => 2 (-), dim=22.0, hs=35.5
 [3/25 (#9)]: 2 => 2 (-), dim=22.0, hs=35.5
 [4/25 (#18)]: 2 => 2 (-), dim=22.0, hs=38.5
 [5/25 (#19)]: 2 => 2 (-), dim=22.0, hs=38.5
 [6/25 (#20)]: 2 => 2 (-), dim=22.0, hs=38.5
 [7/25 (#21)]: 2 => 2 (-), dim=22.0, hs=39.0
 [8/25 (#27)]: 2 => 2 (-), dim=22.0, hs=40.5
 [9/25 (#28)]: 2 => 2 (-), dim=22.0, hs=37.0
 [10/25 (#29)]: 2 => 2 (-), dim=22.0, hs=41.0
 [11/25 (#6)]: 6 => 6 (-), dim=22.0, hs=38.3
 [12/25 (#25)]: 7 => 7 (-), dim=22.0, hs=37.6
 [13/25 (#4)]: 8 => 8 (-), dim=22.0, hs=35.8
 [14/25 (#8)]: 8 => 8 (-), dim=22.0, hs=35.8
 [15/25 (#2)]: 13 => 12 (1 incl), dim=22.0, hs=34.6
  Removed: 2.0
 [16/25 (#17)]: 24 => 3 (20 empty, 1 incl), dim=22.0, hs=43.0
  Removed: 17.0,17.3,17.4,17.5,17.6,17.7,17.8,17.9,17.10,17.12,17.13,17.14,17.15,17.16,17.17,17.18,17.19,17.20,17.21,17.22,17.23
 [17/25 (#11)]: 34 => 8 (26 empty), dim=22.0, hs=40.0
  Removed: 11.2,11.5,11.6,11.7,11.8,11.9,11.10,11.12,11.16,11.17,11.18,11.19,11.20,11.21,11.23,11.25,11.26,11.27,11.28,11.29,11.30,11.31,11.32,11.33,11.34,11.35
 [18/25 (#10)]: 35 => 8 (27 empty), dim=22.0, hs=39.9
  Removed: 10.2,10.5,10.6,10.7,10.8,10.9,10.10,10.11,10.12,10.16,10.17,10.18,10.19,10.20,10.21,10.22,10.25,10.26,10.27,10.28,10.29,10.30,10.31,10.32,10.33,10.34,10.35
 [19/25 (#1)]: 36 => 32 (3 empty, 1 incl), dim=22.0, hs=39.9
  Removed: 1.10,1.11,1.34,1.22
 [20/25 (#13)]: 37 => 8 (27 empty, 2 incl), dim=22.0, hs=40.0
  Removed: 13.2,13.3,13.6,13.7,13.8,13.9,13.11,13.12,13.13,13.15,13.16,13.19,13.20,13.21,13.22,13.24,13.25,13.26,13.28,13.29,13.30,13.31,13.32,13.33,13.34,13.35,13.36,13.37,13.38
 [21/25 (#14)]: 37 => 8 (27 empty, 2 incl), dim=22.0, hs=40.0
  Removed: 14.2,14.3,14.6,14.7,14.8,14.10,14.11,14.12,14.13,14.15,14.16,14.19,14.20,14.21,14.23,14.24,14.25,14.26,14.28,14.29,14.30,14.31,14.32,14.33,14.34,14.35,14.36,14.37,14.38
 [22/25 (#15)]: 37 => 8 (27 empty, 2 incl), dim=22.0, hs=40.0
  Removed: 15.2,15.3,15.6,15.7,15.9,15.10,15.11,15.12,15.13,15.15,15.16,15.19,15.20,15.22,15.23,15.24,15.25,15.26,15.28,15.29,15.30,15.31,15.32,15.33,15.34,15.35,15.36,15.37,15.38
 [23/25 (#16)]: 37 => 8 (27 empty, 2 incl), dim=22.0, hs=39.6
  Removed: 16.2,16.3,16.6,16.8,16.9,16.10,16.11,16.12,16.13,16.15,16.16,16.19,16.21,16.22,16.23,16.24,16.25,16.26,16.28,16.29,16.30,16.31,16.32,16.33,16.34,16.35,16.36,16.37,16.38
 [24/25 (#23)]: 45 => 19 (16 empty, 10 incl), dim=22.0, hs=39.6
  Removed: 23.0,23.2,23.5,23.9,23.14,23.20,23.27,23.35,23.54,23.55,23.56,23.58,23.59,23.62,23.63,23.67,23.68,23.73,23.74,23.80,23.81,23.88,23.89,23.97,23.98,23.107
 [25/25 (#12)]: 46 => 12 (34 empty), dim=22.0, hs=39.4
  Removed: 12.3,12.6,12.7,12.8,12.9,12.10,12.12,12.13,12.17,12.18,12.19,12.20,12.21,12.22,12.23,12.24,12.28,12.29,12.30,12.31,12.32,12.34,12.35,12.37,12.38,12.39,12.40,12.41,12.42,12.43,12.44,12.45,12.46,12.47
 Savings: factor 753,102 (10^6), 5 formulas
[1/24 (#0, #5)]: 2 * 2 = 4 => 4 (-), dim=21.0, hs=37.2
 Applying common planes (codim=4, hs=31)...
 [1/24 (w1)]: 4 => 4 (-), dim=21.0, hs=37.2
 [2/24 (#9)]: 2 => 2 (-), dim=22.0, hs=36.5
 [3/24 (#18)]: 2 => 2 (-), dim=22.0, hs=39.5
 [4/24 (#19)]: 2 => 2 (-), dim=22.0, hs=39.5
 [5/24 (#20)]: 2 => 2 (-), dim=22.0, hs=39.5
 [6/24 (#21)]: 2 => 2 (-), dim=22.0, hs=40.0
 [7/24 (#27)]: 2 => 2 (-), dim=22.0, hs=41.5
 [8/24 (#28)]: 2 => 2 (-), dim=22.0, hs=38.0
 [9/24 (#29)]: 2 => 2 (-), dim=22.0, hs=42.0
 [10/24 (#6)]: 6 => 6 (-), dim=22.0, hs=39.3
 [11/24 (#25)]: 7 => 7 (-), dim=22.0, hs=38.6
 [12/24 (#4)]: 8 => 8 (-), dim=22.0, hs=36.8
 [13/24 (#8)]: 8 => 8 (-), dim=22.0, hs=36.8
 [14/24 (#2)]: 12 => 12 (-), dim=22.0, hs=35.6
 [15/24 (#17)]: 3 => 3 (-), dim=22.0, hs=44.0
 [16/24 (#11)]: 8 => 8 (-), dim=22.0, hs=41.0
 [17/24 (#10)]: 8 => 8 (-), dim=22.0, hs=40.9
 [18/24 (#1)]: 32 => 20 (12 incl), dim=22.0, hs=38.5
  Removed: 1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.12,1.24
 [19/24 (#13)]: 8 => 8 (-), dim=22.0, hs=41.0
 [20/24 (#14)]: 8 => 8 (-), dim=22.0, hs=41.0
 [21/24 (#15)]: 8 => 8 (-), dim=22.0, hs=41.0
 [22/24 (#16)]: 8 => 8 (-), dim=22.0, hs=40.6
 [23/24 (#23)]: 19 => 19 (-), dim=22.0, hs=40.6
 [24/24 (#12)]: 12 => 12 (-), dim=22.0, hs=40.4
 Savings: factor 1.6, 0 formulas
[2/24 (w1, #9)]: 4 * 2 = 8 => 4 (4 incl), dim=21.0, hs=37.2
[3/24 (w2, #18)]: 4 * 2 = 8 => 8 (-), dim=20.0, hs=35.2
[4/24 (w3, #19)]: 8 * 2 = 16 => 16 (-), dim=19.0, hs=33.0
[5/24 (w4, #20)]: 16 * 2 = 32 => 32 (-), dim=18.0, hs=30.8
[6/24 (w5, #21)]: 32 * 2 = 64 => 64 (-), dim=17.0, hs=29.4
[7/24 (w6, #27)]: 64 * 2 = 128 => 112 (16 empty), dim=16.0, hs=28.9
[8/24 (w7, #28)]: 112 * 2 = 224 => 144 (64 empty, 16 incl), dim=15.0, hs=28.5
[9/24 (w8, #29)]: 144 * 2 = 288 => 288 (-), dim=14.0, hs=28.2
[10/24 (w9, #17)]: 288 * 3 = 864 => 432 (272 empty, 160 incl), dim=13.0, hs=29.5
[11/24 (w10, #6)]: 432 * 6 = 2592 => 192 (1488 empty, 912 incl), dim=13.0, hs=29.4
[12/24 (w11, #25)]: 192 * 7 = 1344 => 534 (140 empty, 670 incl), dim=12.0, hs=29.1
[13/24 (w12, #4)]: 534 * 8 = 4272 => 1760 (474 empty, 2038 incl), dim=11.0, hs=29.1
[14/24 (w13, #8)]: 1760 * 8 = 14080 => 1760 (970 empty, 11350 incl), dim=11.0, hs=29.1
[15/24 (w14, #11)]: 1760 * 8 = 14080 => 2494 (7090 empty, 4496 incl), dim=10.0, hs=29.3
[16/24 (w15, #10)]: 2494 * 8 = 19952 => 2636 (11969 empty, 5347 incl), dim=9.0, hs=29.3
[17/24 (w16, #13)]: 2636 * 8 = 21088 => 2599 (11668 empty, 6821 incl), dim=8.0, hs=29.4
[18/24 (w17, #14)]: 2599 * 8 = 20792 => 1830 (11613 empty, 7349 incl), dim=7.1, hs=29.6
[19/24 (w18, #15)]: 1830 * 8 = 14640 => 1016 (8666 empty, 4958 incl), dim=6.0, hs=29.4
[20/24 (w19, #16)]: 1016 * 8 = 8128 => 489 (4849 empty, 2790 incl), dim=5.9, hs=29.5
[21/24 (w20, #2)]: 489 * 12 = 5868 => 341 (2730 empty, 2797 incl), dim=5.9, hs=29.6
[22/24 (w21, #12)]: 341 * 12 = 4092 => 245 (2877 empty, 970 incl), dim=5.0, hs=29.2
[23/24 (w22, #23)]: 245 * 19 = 4655 => 382 (3556 empty, 717 incl), dim=4.0, hs=29.3
[24/24 (w23, #1)]: 382 * 20 = 7640 => 130 (7264 empty, 246 incl), dim=3.0, hs=28.6
Total time: 644.176 sec, total intersections: 145,464, total inclusion tests: 14,973,212
Common plane time: 9.652 sec
Solutions: 130, dim=3.0, hs=28.6, max=2636, maxin=21088
f-vector: [0, 0, 0, 130]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 1.093 GiB, pagefile 1.111 GiB
Memory: pmem(rss=111673344, vms=106975232, num_page_faults=674692, peak_wset=1173532672, wset=111673344, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=308840, nonpaged_pool=61312, pagefile=106975232, peak_pagefile=1193136128, private=106975232)
