
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:13:36+02:00
Command line: ..\trop\ptcut.py BIOMD0000000169 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000169 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 38C3OZBJ3J4H8
Effective epsilon: 1/11.0

Reading db\BIOMD0000000169\vector_field_q.txt
Reading db\BIOMD0000000169\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 6 with: k29*x10 + 1

Multiplying equation 10 with: k27*x6 + 1

System is rational in variable x6,x10.
System is rational in parameter k27,k29,k33.
Term in equation 4 is zero since k7=0
Tropicalization time: 2.539 sec
Variables: x1-x11
Saving tropical system...
Creating polyhedra... 
Creation time: 0.067 sec
Saving polyhedra...

Sorting ascending...
Dimension: 11
Formulas: 11
Order: 8 10 0 2 3 9 4 6 1 5 7
Possible combinations: 1 * 1 * 3 * 3 * 4 * 4 * 6 * 6 * 8 * 8 * 9 = 2,985,984 (10^6)
 Applying common planes (codim=2, hs=6)...
 [1/9 (#0)]: 3 => 3 (-), dim=8.0, hs=6.0
 [2/9 (#2)]: 3 => 3 (-), dim=8.0, hs=6.0
 [3/9 (#3)]: 4 => 4 (-), dim=8.0, hs=9.0
 [4/9 (#9)]: 4 => 4 (-), dim=8.0, hs=9.0
 [5/9 (#4)]: 6 => 3 (3 incl), dim=8.3, hs=8.7
  Removed: 4.0,4.3,4.4
 [6/9 (#6)]: 6 => 3 (3 incl), dim=8.3, hs=8.7
  Removed: 6.1,6.3,6.4
 [7/9 (#1)]: 8 => 7 (1 incl), dim=8.0, hs=8.6
  Removed: 1.4
 [8/9 (#5)]: 8 => 8 (-), dim=8.0, hs=9.2
 [9/9 (#7)]: 9 => 3 (6 incl), dim=8.7, hs=8.3
  Removed: 7.1,7.2,7.3,7.4,7.6,7.7
 Savings: factor 13.714285714285714 (10^1), 2 formulas
[1/8 (#0, #2)]: 3 * 3 = 9 => 3 (6 incl), dim=7.0, hs=6.0
 Applying common planes (codim=3, hs=4)...
 [1/8 (w1)]: 3 => 3 (-), dim=7.0, hs=6.0
 [2/8 (#3)]: 4 => 4 (-), dim=7.0, hs=9.0
 [3/8 (#9)]: 4 => 4 (-), dim=7.0, hs=9.0
 [4/8 (#4)]: 3 => 3 (-), dim=7.3, hs=8.7
 [5/8 (#6)]: 3 => 3 (-), dim=7.3, hs=8.7
 [6/8 (#1)]: 7 => 5 (2 incl), dim=7.0, hs=8.0
  Removed: 1.0,1.2
 [7/8 (#5)]: 8 => 4 (4 incl), dim=7.5, hs=9.0
  Removed: 5.9,5.2,5.11,5.5
 [8/8 (#7)]: 3 => 3 (-), dim=7.7, hs=8.3
 Savings: factor 2.8, 0 formulas
[2/8 (w1, #4)]: 3 * 3 = 9 => 8 (1 empty), dim=6.2, hs=8.5
[3/8 (w2, #6)]: 8 * 3 = 24 => 16 (8 empty), dim=5.6, hs=10.1
[4/8 (w3, #7)]: 16 * 3 = 48 => 21 (27 empty), dim=5.5, hs=10.7
[5/8 (w4, #3)]: 21 * 4 = 84 => 40 (30 empty, 14 incl), dim=4.9, hs=11.9
[6/8 (w5, #9)]: 40 * 4 = 160 => 64 (64 empty, 32 incl), dim=4.3, hs=12.8
[7/8 (w6, #5)]: 64 * 4 = 256 => 67 (181 empty, 8 incl), dim=4.1, hs=13.3
[8/8 (w7, #1)]: 67 * 5 = 335 => 55 (254 empty, 26 incl), dim=4.1, hs=13.3
Total time: 0.481 sec, total intersections: 1,011, total inclusion tests: 7,174
Common plane time: 0.041 sec
Solutions: 55, dim=4.1, hs=13.3, max=67, maxin=335
f-vector: [0, 0, 4, 11, 14, 26]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.073 GiB, pagefile 0.067 GiB
Memory: pmem(rss=75399168, vms=68521984, num_page_faults=22167, peak_wset=78114816, wset=75399168, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=41592, nonpaged_pool=41592, pagefile=68521984, peak_pagefile=71479296, private=68521984)
