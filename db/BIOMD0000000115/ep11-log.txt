
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:12:24+02:00
Command line: ..\trop\ptcut.py BIOMD0000000115 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000115 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 31KE0FTNIH4QE
Effective epsilon: 1/11.0

Reading db\BIOMD0000000115\vector_field_q.txt
Reading db\BIOMD0000000115\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k4**k3 + x2**k3

Multiplying equation 2 with: k4**k3 + x2**k3
System is rational in variable x2.
System is rational in parameter k3,k4,k9,k10.
System is exponential in parameter k3.
Tropicalization time: 1.952 sec
Variables: x1, x2
Saving tropical system...
Creating polyhedra... 
Creation time: 0.043 sec
Saving polyhedra...

Sorting ascending...
Dimension: 2
Formulas: 2
Order: 1 0
Possible combinations: 2 * 3 = 6 (10^1)
[1/1 (#0, #1)]: 3 * 2 = 6 => 1 (4 empty, 1 incl), dim=1.0, hs=2.0
Total time: 0.001 sec, total intersections: 6, total inclusion tests: 2
Common plane time: 0.000 sec
Solutions: 1, dim=1.0, hs=2.0, max=1, maxin=6
f-vector: [0, 1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.067 GiB, pagefile 0.061 GiB
Memory: pmem(rss=72429568, vms=65421312, num_page_faults=20501, peak_wset=72433664, wset=72429568, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40096, nonpaged_pool=40096, pagefile=65421312, peak_pagefile=65421312, private=65421312)
