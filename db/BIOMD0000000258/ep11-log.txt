
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:19:51+02:00
Command line: ..\trop\ptcut.py BIOMD0000000258 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000258 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: ZYR0AD4Z1HKZ
Effective epsilon: 1/11.0

Reading db\BIOMD0000000258\vector_field_q.txt
Reading db\BIOMD0000000258\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k4*k5*k6*k7 + k4*k5*k6*x3 + k4*k5*k7*x2 + k4*k5*x2*x3 + k4*k6*k7*x2 + k4*k7*x2**2 + k5*k6*k7*x1 + k5*k6*x1*x3 + k6*k7*x1*x2

Multiplying equation 2 with: k4*k5*k6*k7 + k4*k5*k6*x3 + k4*k5*k7*x2 + k4*k5*x2*x3 + k4*k6*k7*x2 + k4*k7*x2**2 + k5*k6*k7*x1 + k5*k6*x1*x3 + k6*k7*x1*x2

Multiplying equation 3 with: k4*k5*k6*k7 + k4*k5*k6*x3 + k4*k5*k7*x2 + k4*k5*x2*x3 + k4*k6*k7*x2 + k4*k7*x2**2 + k5*k6*k7*x1 + k5*k6*x1*x3 + k6*k7*x1*x2

System is rational in variable x1,x2,x3.
System is rational in parameter k3,k4,k5,k6,k7,k10.
Tropicalization time: 5.103 sec
Variables: x1-x3
Saving tropical system...
Creating polyhedra... 
Creation time: 0.016 sec
Saving polyhedra...

Sorting ascending...
Dimension: 3
Formulas: 4
Order: 3 0 2 1
Possible combinations: 3 * 6 * 6 * 8 = 864 (10^3)
 Applying common planes (codim=0, hs=3)...
 [1/4 (#3)]: 3 => 3 (-), dim=2.0, hs=3.0
 [2/4 (#0)]: 6 => 6 (-), dim=2.0, hs=3.8
 [3/4 (#2)]: 6 => 6 (-), dim=2.0, hs=3.8
 [4/4 (#1)]: 8 => 8 (-), dim=2.0, hs=3.8
 Savings: factor 1.0, 0 formulas
[1/3 (#0, #3)]: 6 * 3 = 18 => 5 (7 empty, 6 incl), dim=1.0, hs=3.4
[2/3 (#2, w1)]: 6 * 5 = 30 => 3 (17 empty, 10 incl), dim=0.0, hs=3.0
 Applying common planes (codim=0, hs=3)...
 [1/2 (w2)]: 3 => 3 (-), dim=0.0, hs=3.0
 [2/2 (#1)]: 8 => 6 (2 incl), dim=2.0, hs=4.7
  Removed: 1.16,1.15
 Savings: factor 1.3333333333333333, 0 formulas
[3/3 (#1, w2)]: 6 * 3 = 18 => 3 (12 empty, 3 incl), dim=0.0, hs=3.0
Total time: 0.015 sec, total intersections: 100, total inclusion tests: 240
Common plane time: 0.007 sec
Solutions: 3, dim=0.0, hs=3.0, max=5, maxin=30
f-vector: [3]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.061 GiB
Memory: pmem(rss=72880128, vms=65867776, num_page_faults=20754, peak_wset=72884224, wset=72880128, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40232, nonpaged_pool=40232, pagefile=65867776, peak_pagefile=65867776, private=65867776)
