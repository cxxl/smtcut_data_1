
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T21:50:22+02:00
Command line: BIOMD0000000048 --ppone --ppsmt


Model: BIOMD0000000048
Logfile: db\BIOMD0000000048\ep11-os-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^21.9 in 29 bags
Bag sizes: 1 1 4 5 171 4 2 2 8 16 4 16 2 16 4 2 12 24 6 9 3 6 1 8 5 8 5 12 12

Filtering bags...
Collecting...
Bag 1/26, 4 polyhedra
Bag 2/26, 5 polyhedra
Bag 3/26, 171 polyhedra
Bag 4/26, 4 polyhedra
Bag 5/26, 2 polyhedra
Bag 6/26, 2 polyhedra
Bag 7/26, 8 polyhedra
Bag 8/26, 16 polyhedra
Bag 9/26, 4 polyhedra
Bag 10/26, 16 polyhedra
Bag 11/26, 2 polyhedra
Bag 12/26, 16 polyhedra
Bag 13/26, 4 polyhedra
Bag 14/26, 2 polyhedra
Bag 15/26, 12 polyhedra
Bag 16/26, 24 polyhedra
Bag 17/26, 6 polyhedra
Bag 18/26, 9 polyhedra
Bag 19/26, 3 polyhedra
Bag 20/26, 6 polyhedra
Bag 21/26, 8 polyhedra
Bag 22/26, 5 polyhedra
Bag 23/26, 8 polyhedra
Bag 24/26, 5 polyhedra => 1 dropped
Bag 25/26, 12 polyhedra
Bag 26/26, 12 polyhedra
Possible combinations after preprocessing: 10^21.8 in 27 bags
Bag sizes: 1 4 5 171 4 2 2 8 16 4 16 2 16 4 2 12 24 6 9 3 6 8 5 8 4 12 12
Time: 1.312 sec

Checking for superfluous polyhedra...
Bag 1/27, 171 polyhedra:
  Polyhedron 1... superfluous, 0.094 sec
  Polyhedron 2... required, 0.094 sec
  Polyhedron 3... superfluous, 0.031 sec
  Polyhedron 4... superfluous, 0.047 sec
  Polyhedron 5... superfluous, 0.031 sec
  Polyhedron 6... superfluous, 0.047 sec
  Polyhedron 7... superfluous, 0.031 sec
  Polyhedron 8... superfluous, 0.062 sec
  Polyhedron 9... superfluous, 0.062 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... superfluous, 0.000 sec
  Polyhedron 12... superfluous, 0.000 sec
  Polyhedron 13... superfluous, 0.000 sec
  Polyhedron 14... superfluous, 0.000 sec
  Polyhedron 15... superfluous, 0.000 sec
  Polyhedron 16... superfluous, 0.016 sec
  Polyhedron 17... superfluous, 0.000 sec
  Polyhedron 18... superfluous, 0.000 sec
  Polyhedron 19... superfluous, 0.031 sec
  Polyhedron 20... superfluous, 0.031 sec
  Polyhedron 21... superfluous, 0.047 sec
  Polyhedron 22... superfluous, 0.031 sec
  Polyhedron 23... superfluous, 0.016 sec
  Polyhedron 24... superfluous, 0.031 sec
  Polyhedron 25... superfluous, 0.031 sec
  Polyhedron 26... superfluous, 0.031 sec
  Polyhedron 27... superfluous, 0.031 sec
  Polyhedron 28... superfluous, 0.031 sec
  Polyhedron 29... superfluous, 0.016 sec
  Polyhedron 30... superfluous, 0.031 sec
  Polyhedron 31... superfluous, 0.016 sec
  Polyhedron 32... superfluous, 0.031 sec
  Polyhedron 33... superfluous, 0.031 sec
  Polyhedron 34... superfluous, 0.031 sec
  Polyhedron 35... superfluous, 0.047 sec
  Polyhedron 36... superfluous, 0.031 sec
  Polyhedron 37... superfluous, 0.062 sec
  Polyhedron 38... superfluous, 0.031 sec
  Polyhedron 39... superfluous, 0.047 sec
  Polyhedron 40... superfluous, 0.031 sec
  Polyhedron 41... superfluous, 0.031 sec
  Polyhedron 42... superfluous, 0.047 sec
  Polyhedron 43... superfluous, 0.062 sec
  Polyhedron 44... superfluous, 0.047 sec
  Polyhedron 45... superfluous, 0.078 sec
  Polyhedron 46... superfluous, 0.031 sec
  Polyhedron 47... superfluous, 0.047 sec
  Polyhedron 48... superfluous, 0.047 sec
  Polyhedron 49... superfluous, 0.031 sec
  Polyhedron 50... superfluous, 0.031 sec
  Polyhedron 51... superfluous, 0.094 sec
  Polyhedron 52... superfluous, 0.062 sec
  Polyhedron 53... superfluous, 0.078 sec
  Polyhedron 54... superfluous, 0.062 sec
  Polyhedron 55... superfluous, 0.031 sec
  Polyhedron 56... superfluous, 0.047 sec
  Polyhedron 57... superfluous, 0.047 sec
  Polyhedron 58... superfluous, 0.047 sec
  Polyhedron 59... superfluous, 0.016 sec
  Polyhedron 60... superfluous, 0.031 sec
  Polyhedron 61... superfluous, 0.078 sec
  Polyhedron 62... superfluous, 0.266 sec
  Polyhedron 63... superfluous, 0.031 sec
  Polyhedron 64... superfluous, 0.031 sec
  Polyhedron 65... superfluous, 0.047 sec
  Polyhedron 66... superfluous, 0.078 sec
  Polyhedron 67... superfluous, 0.062 sec
  Polyhedron 68... superfluous, 0.031 sec
  Polyhedron 69... superfluous, 0.031 sec
  Polyhedron 70... superfluous, 0.031 sec
  Polyhedron 71... superfluous, 0.031 sec
  Polyhedron 72... superfluous, 0.047 sec
  Polyhedron 73... superfluous, 0.031 sec
  Polyhedron 74... superfluous, 0.016 sec
  Polyhedron 75... superfluous, 0.094 sec
  Polyhedron 76... superfluous, 0.047 sec
  Polyhedron 77... superfluous, 0.125 sec
  Polyhedron 78... superfluous, 0.016 sec
  Polyhedron 79... superfluous, 0.031 sec
  Polyhedron 80... superfluous, 0.047 sec
  Polyhedron 81... superfluous, 0.047 sec
  Polyhedron 82... superfluous, 0.031 sec
  Polyhedron 83... superfluous, 0.031 sec
  Polyhedron 84... superfluous, 0.016 sec
  Polyhedron 85... superfluous, 0.109 sec
  Polyhedron 86... superfluous, 0.281 sec
  Polyhedron 87... superfluous, 0.031 sec
  Polyhedron 88... superfluous, 0.047 sec
  Polyhedron 89... superfluous, 0.062 sec
  Polyhedron 90... superfluous, 0.078 sec
  Polyhedron 91... superfluous, 0.016 sec
  Polyhedron 92... superfluous, 0.031 sec
  Polyhedron 93... superfluous, 0.047 sec
  Polyhedron 94... superfluous, 0.016 sec
  Polyhedron 95... superfluous, 0.031 sec
  Polyhedron 96... superfluous, 0.016 sec
  Polyhedron 97... superfluous, 0.016 sec
  Polyhedron 98... superfluous, 0.016 sec
  Polyhedron 99... superfluous, 0.016 sec
  Polyhedron 100... superfluous, 0.031 sec
  Polyhedron 101... superfluous, 0.016 sec
  Polyhedron 102... superfluous, 0.031 sec
  Polyhedron 103... superfluous, 0.031 sec
  Polyhedron 104... superfluous, 0.016 sec
  Polyhedron 105... superfluous, 0.016 sec
  Polyhedron 106... superfluous, 0.031 sec
  Polyhedron 107... superfluous, 0.016 sec
  Polyhedron 108... superfluous, 0.031 sec
  Polyhedron 109... superfluous, 0.016 sec
  Polyhedron 110... superfluous, 0.031 sec
  Polyhedron 111... superfluous, 0.047 sec
  Polyhedron 112... superfluous, 0.016 sec
  Polyhedron 113... superfluous, 0.031 sec
  Polyhedron 114... superfluous, 0.016 sec
  Polyhedron 115... superfluous, 0.016 sec
  Polyhedron 116... superfluous, 0.031 sec
  Polyhedron 117... superfluous, 0.016 sec
  Polyhedron 118... superfluous, 0.031 sec
  Polyhedron 119... superfluous, 0.016 sec
  Polyhedron 120... superfluous, 0.047 sec
  Polyhedron 121... superfluous, 0.031 sec
  Polyhedron 122... superfluous, 0.016 sec
  Polyhedron 123... superfluous, 0.016 sec
  Polyhedron 124... superfluous, 0.031 sec
  Polyhedron 125... superfluous, 0.016 sec
  Polyhedron 126... superfluous, 0.031 sec
  Polyhedron 127... superfluous, 0.016 sec
  Polyhedron 128... superfluous, 0.031 sec
  Polyhedron 129... superfluous, 0.047 sec
  Polyhedron 130... superfluous, 0.016 sec
  Polyhedron 131... superfluous, 0.016 sec
  Polyhedron 132... superfluous, 0.031 sec
  Polyhedron 133... superfluous, 0.016 sec
  Polyhedron 134... superfluous, 0.031 sec
  Polyhedron 135... superfluous, 0.016 sec
  Polyhedron 136... superfluous, 0.031 sec
  Polyhedron 137... superfluous, 0.016 sec
  Polyhedron 138... superfluous, 0.219 sec
  Polyhedron 139... superfluous, 0.016 sec
  Polyhedron 140... superfluous, 0.031 sec
  Polyhedron 141... superfluous, 0.016 sec
  Polyhedron 142... superfluous, 0.031 sec
  Polyhedron 143... superfluous, 0.016 sec
  Polyhedron 144... superfluous, 0.031 sec
  Polyhedron 145... superfluous, 0.016 sec
  Polyhedron 146... superfluous, 0.016 sec
  Polyhedron 147... superfluous, 0.031 sec
  Polyhedron 148... superfluous, 0.031 sec
  Polyhedron 149... superfluous, 0.016 sec
  Polyhedron 150... superfluous, 0.016 sec
  Polyhedron 151... superfluous, 0.031 sec
  Polyhedron 152... superfluous, 0.016 sec
  Polyhedron 153... superfluous, 0.031 sec
  Polyhedron 154... superfluous, 0.016 sec
  Polyhedron 155... superfluous, 0.031 sec
  Polyhedron 156... superfluous, 0.016 sec
  Polyhedron 157... superfluous, 0.031 sec
  Polyhedron 158... superfluous, 0.016 sec
  Polyhedron 159... superfluous, 0.031 sec
  Polyhedron 160... superfluous, 0.016 sec
  Polyhedron 161... superfluous, 0.031 sec
  Polyhedron 162... superfluous, 0.016 sec
  Polyhedron 163... superfluous, 0.016 sec
  Polyhedron 164... superfluous, 0.031 sec
  Polyhedron 165... superfluous, 0.203 sec
  Polyhedron 166... superfluous, 0.031 sec
  Polyhedron 167... superfluous, 0.016 sec
  Polyhedron 168... superfluous, 0.031 sec
  Polyhedron 169... superfluous, 0.016 sec
  Polyhedron 170... superfluous, 0.031 sec
  Polyhedron 171... superfluous, 0.016 sec
  => 1 polyhedra left
Bag 2/27, 24 polyhedra:
  Polyhedron 1... superfluous, 0.047 sec
  Polyhedron 2... superfluous, 0.031 sec
  Polyhedron 3... superfluous, 0.031 sec
  Polyhedron 4... superfluous, 0.031 sec
  Polyhedron 5... superfluous, 0.031 sec
  Polyhedron 6... superfluous, 0.109 sec
  Polyhedron 7... required, 0.047 sec
  Polyhedron 8... superfluous, 0.047 sec
  Polyhedron 9... superfluous, 0.062 sec
  Polyhedron 10... superfluous, 0.109 sec
  Polyhedron 11... superfluous, 0.094 sec
  Polyhedron 12... superfluous, 0.031 sec
  Polyhedron 13... superfluous, 0.016 sec
  Polyhedron 14... superfluous, 0.031 sec
  Polyhedron 15... superfluous, 0.047 sec
  Polyhedron 16... superfluous, 0.016 sec
  Polyhedron 17... superfluous, 0.016 sec
  Polyhedron 18... superfluous, 0.016 sec
  Polyhedron 19... superfluous, 0.016 sec
  Polyhedron 20... superfluous, 0.016 sec
  Polyhedron 21... superfluous, 0.016 sec
  Polyhedron 22... superfluous, 0.016 sec
  Polyhedron 23... superfluous, 0.031 sec
  Polyhedron 24... superfluous, 0.016 sec
  => 1 polyhedra left
Bag 3/27, 16 polyhedra:
  Polyhedron 1... required, 0.047 sec
  Polyhedron 2... superfluous, 0.047 sec
  Polyhedron 3... superfluous, 0.078 sec
  Polyhedron 4... superfluous, 0.078 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.047 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.078 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.016 sec
  Polyhedron 11... superfluous, 0.031 sec
  Polyhedron 12... superfluous, 0.078 sec
  Polyhedron 13... superfluous, 0.062 sec
  Polyhedron 14... superfluous, 0.062 sec
  Polyhedron 15... superfluous, 0.047 sec
  Polyhedron 16... superfluous, 0.078 sec
  => 1 polyhedra left
Bag 4/27, 16 polyhedra:
  Polyhedron 1... required, 0.125 sec
  Polyhedron 2... superfluous, 0.078 sec
  Polyhedron 3... superfluous, 0.078 sec
  Polyhedron 4... superfluous, 0.078 sec
  Polyhedron 5... superfluous, 0.078 sec
  Polyhedron 6... superfluous, 0.078 sec
  Polyhedron 7... superfluous, 0.078 sec
  Polyhedron 8... superfluous, 0.078 sec
  Polyhedron 9... superfluous, 0.062 sec
  Polyhedron 10... superfluous, 0.047 sec
  Polyhedron 11... superfluous, 0.047 sec
  Polyhedron 12... superfluous, 0.062 sec
  Polyhedron 13... superfluous, 0.062 sec
  Polyhedron 14... superfluous, 0.031 sec
  Polyhedron 15... superfluous, 0.062 sec
  Polyhedron 16... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 5/27, 16 polyhedra:
  Polyhedron 1... required, 0.047 sec
  Polyhedron 2... superfluous, 0.047 sec
  Polyhedron 3... superfluous, 0.047 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.062 sec
  Polyhedron 6... superfluous, 0.047 sec
  Polyhedron 7... superfluous, 0.031 sec
  Polyhedron 8... superfluous, 0.047 sec
  Polyhedron 9... superfluous, 0.047 sec
  Polyhedron 10... superfluous, 0.047 sec
  Polyhedron 11... superfluous, 0.031 sec
  Polyhedron 12... superfluous, 0.047 sec
  Polyhedron 13... superfluous, 0.047 sec
  Polyhedron 14... superfluous, 0.031 sec
  Polyhedron 15... superfluous, 0.031 sec
  Polyhedron 16... superfluous, 0.016 sec
  => 1 polyhedra left
Bag 6/27, 12 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.047 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.031 sec
  Polyhedron 5... superfluous, 0.031 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... superfluous, 0.031 sec
  Polyhedron 8... superfluous, 0.047 sec
  Polyhedron 9... required, 0.016 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... superfluous, 0.062 sec
  Polyhedron 12... superfluous, 0.031 sec
  => 1 polyhedra left
Bag 7/27, 12 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.031 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... required, 0.000 sec
  Polyhedron 8... superfluous, 0.016 sec
  Polyhedron 9... superfluous, 0.000 sec
  Polyhedron 10... required, 0.016 sec
  Polyhedron 11... superfluous, 0.000 sec
  Polyhedron 12... superfluous, 0.000 sec
  => 3 polyhedra left
Bag 8/27, 12 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.000 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... superfluous, 0.000 sec
  Polyhedron 12... required, 0.016 sec
  => 1 polyhedra left
Bag 9/27, 9 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.031 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... required, 0.016 sec
  Polyhedron 6... superfluous, 0.031 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 10/27, 8 polyhedra:
  Polyhedron 1... required, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 11/27, 8 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 12/27, 8 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 13/27, 6 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.031 sec
  => 1 polyhedra left
Bag 14/27, 6 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... required, 0.000 sec
  Polyhedron 4... superfluous, 0.031 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 15/27, 5 polyhedra:
  Polyhedron 1... required, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  => 1 polyhedra left
Bag 16/27, 5 polyhedra:
  Polyhedron 1... required, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 17/27, 4 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... required, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.016 sec
  => 1 polyhedra left
Bag 18/27, 4 polyhedra:
  Polyhedron 1... required, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  => 2 polyhedra left
Bag 19/27, 4 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... required, 0.016 sec
  Polyhedron 4... superfluous, 0.016 sec
  => 1 polyhedra left
Bag 20/27, 4 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 21/27, 4 polyhedra:
  Polyhedron 1... required, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 22/27, 3 polyhedra:
  Polyhedron 1... required, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 23/27, 2 polyhedra:
  Polyhedron 1... required, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 24/27, 2 polyhedra:
  Polyhedron 1... required, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  => 1 polyhedra left
Bag 25/27, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 1 polyhedra left
Bag 26/27, 2 polyhedra:
  Polyhedron 1... required, 0.000 sec
  Polyhedron 2... required, 0.016 sec
  => 2 polyhedra left
Possible combinations after preprocessing: 12 in 4 bags
Bag sizes: 1 3 2 2
Time: 10.938 sec

Minimizing (17,86)... (17,20)


Running SMT solver (1)... 0.000 sec
Checking... Minimizing (20,31)... (20,10), 0.031 sec
Inserting... 0.000 sec
[1 ph, mem 107/101, smt 0.000, isect 0.031, ins 0.000, tot 0.062]

Running SMT solver (2)... 0.000 sec
Checking... Minimizing (20,31)... (20,12), 0.047 sec
Inserting... 0.000 sec
[2 ph, mem 112/106, smt 0.000, isect 0.078, ins 0.000, tot 0.109]

Running SMT solver (3)... 0.000 sec
Checking... Minimizing (20,31)... (20,12), 0.031 sec
Inserting... 0.000 sec
[3 ph, mem 115/109, smt 0.000, isect 0.109, ins 0.000, tot 0.156]

Running SMT solver (4)... 0.000 sec
Checking... Minimizing (20,31)... (20,10), 0.031 sec
Inserting... 0.000 sec
[4 ph, mem 117/111, smt 0.000, isect 0.141, ins 0.000, tot 0.188]

Running SMT solver (5)... 0.000 sec
Checking... Minimizing (20,30)... (20,10), 0.031 sec
Inserting... 0.000 sec
[5 ph, mem 112/105, smt 0.000, isect 0.172, ins 0.000, tot 0.234]

Running SMT solver (6)... 0.000 sec
No more solutions found

5 polyhedra, 6 rounds, smt 0.000 sec, isect 0.172 sec, insert 0.000 sec, total 0.234 sec
Preprocessing 12.312 sec, super total 12.547 sec
Comparing... 0.297 sec.  Solution matches input.
Total contains() calls: 10, full: 1, ratio: 10.000%
