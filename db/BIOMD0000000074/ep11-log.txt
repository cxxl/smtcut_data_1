
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.4 (default, Aug  9 2019, 18:34:13) [MSC v.1915 64 bit (AMD64)]
numpy 1.16.5, sympy 1.4, gmpy2 2.1.0a5
It is now 2020-04-08T08:27:56+02:00
Command line: ..\trop\ptcut.py BIOMD0000000074 -e11 --rat --no-cache=n --maxruntime 86400 --maxmem 18669M --comment "concurrent=6, timeout=86400"

----------------------------------------------------------------------
Solving BIOMD0000000074 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=86400
Random seed: 1EHVXK2KVH9OP
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 19
Formulas: 19
Order: 15 18 0 2 5 6 7 9 11 13 14 16 17 1 10 3 12 4 8
Possible combinations: 3 * 3 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 6 * 10 * 10 * 18 * 20 * 24 * 24 = 1,174,136,684,544,000 (10^15)
 Applying common planes (codim=0, hs=1)...
 [1/19 (#15)]: 3 => 3 (-), dim=18.0, hs=3.3
 [2/19 (#18)]: 3 => 3 (-), dim=18.0, hs=3.3
 [3/19 (#0)]: 4 => 4 (-), dim=18.0, hs=2.2
 [4/19 (#2)]: 4 => 4 (-), dim=18.0, hs=3.2
 [5/19 (#5)]: 4 => 4 (-), dim=18.0, hs=3.2
 [6/19 (#6)]: 4 => 4 (-), dim=18.0, hs=3.2
 [7/19 (#7)]: 4 => 4 (-), dim=18.0, hs=3.2
 [8/19 (#9)]: 4 => 4 (-), dim=18.0, hs=3.2
 [9/19 (#11)]: 4 => 4 (-), dim=18.0, hs=3.2
 [10/19 (#13)]: 4 => 4 (-), dim=18.0, hs=3.2
 [11/19 (#14)]: 4 => 4 (-), dim=18.0, hs=3.2
 [12/19 (#16)]: 4 => 4 (-), dim=18.0, hs=3.2
 [13/19 (#17)]: 6 => 6 (-), dim=18.0, hs=4.3
 [14/19 (#1)]: 10 => 10 (-), dim=18.0, hs=4.8
 [15/19 (#10)]: 10 => 10 (-), dim=18.0, hs=5.4
 [16/19 (#3)]: 18 => 18 (-), dim=18.0, hs=6.1
 [17/19 (#12)]: 20 => 20 (-), dim=18.0, hs=6.4
 [18/19 (#4)]: 24 => 24 (-), dim=18.0, hs=6.7
 [19/19 (#8)]: 24 => 24 (-), dim=18.0, hs=6.7
 Savings: factor 1.0, 0 formulas
[1/18 (#15, #18)]: 3 * 3 = 9 => 9 (-), dim=17.0, hs=5.7
[2/18 (w1, #0)]: 9 * 4 = 36 => 18 (6 empty, 12 incl), dim=16.0, hs=5.8
[3/18 (w2, #2)]: 18 * 4 = 72 => 72 (-), dim=15.0, hs=8.1
[4/18 (w3, #5)]: 72 * 4 = 288 => 288 (-), dim=14.0, hs=10.3
[5/18 (w4, #6)]: 288 * 4 = 1152 => 1152 (-), dim=13.0, hs=12.6
[6/18 (w5, #7)]: 1152 * 4 = 4608 => 1728 (2880 incl), dim=12.2, hs=13.7
[7/18 (w6, #9)]: 1728 * 4 = 6912 => 6912 (-), dim=11.2, hs=15.9
[8/18 (w7, #11)]: 6912 * 4 = 27648 => 27648 (-), dim=10.2, hs=18.2
