
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 0-0-0
\ dimension 0; is compact
MAXIMIZE
Subject To
  eq0: +1 x3 = 0   \ x3 = 1.0
  eq1: +1 x2 = 0   \ x2 = 1.0
  eq2: +1 x1 = 0   \ x1 = 1.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

