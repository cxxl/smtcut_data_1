
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:11:07+02:00
Command line: ..\trop\ptcut.py BIOMD0000000079 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000079 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 1A23BQX1JBXZ4
Effective epsilon: 1/11.0

Reading db\BIOMD0000000079\vector_field_q.txt
Reading db\BIOMD0000000079\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k4 + x1

Multiplying equation 2 with: -k6*k8 - k6*x2 + k8*x2 - k8 + x2**2 - x2

Multiplying equation 3 with: -k10*k12 - k10*x3 + k12*x3 - k12 + x3**2 - x3
System is rational in variable x1,x2,x3.
System is rational in parameter k1,k4,k6,k8,k10,k12.
Tropicalization time: 1.727 sec
Variables: x1-x3
Saving tropical system...
Creating polyhedra... 
Creation time: 0.016 sec
Saving polyhedra...

Sorting ascending...
Dimension: 3
Formulas: 3
Order: 0 1 2
Possible combinations: 2 * 5 * 5 = 50 (10^2)
[1/2 (#1, #0)]: 5 * 2 = 10 => 4 (1 empty, 5 incl), dim=1.5, hs=3.2
[2/2 (#2, w1)]: 5 * 4 = 20 => 4 (6 empty, 10 incl), dim=1.0, hs=3.5
Total time: 0.005 sec, total intersections: 30, total inclusion tests: 66
Common plane time: 0.000 sec
Solutions: 4, dim=1.0, hs=3.5, max=4, maxin=20
f-vector: [0, 4]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.061 GiB
Memory: pmem(rss=72781824, vms=65609728, num_page_faults=20540, peak_wset=72785920, wset=72781824, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40096, nonpaged_pool=40096, pagefile=65609728, peak_pagefile=65609728, private=65609728)
