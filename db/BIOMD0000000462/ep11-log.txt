
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:29:29+02:00
Command line: ..\trop\ptcut.py BIOMD0000000462 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000462 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 2QM927N908B3N
Effective epsilon: 1/11.0

Reading db\BIOMD0000000462\vector_field_q.txt
Reading db\BIOMD0000000462\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k8**2 + x3**2

Multiplying equation 3 with: k8**2 + x3**2

System is rational in variable x3.
System is rational in parameter k8,k10.
Tropicalization time: 1.620 sec
Variables: x1-x3, x6
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.
Creation time: 0.127 sec
Saving polyhedra...

Sorting ascending...
Dimension: 4
Formulas: 3
Order: 1 2 0
Possible combinations: 5 * 6 * 13 = 390 (10^3)
[1/2 (#2, #1)]: 6 * 5 = 30 => 11 (10 empty, 9 incl), dim=2.1, hs=3.6
[2/2 (#0, w1)]: 13 * 11 = 143 => 12 (96 empty, 35 incl), dim=2.0, hs=4.6
Total time: 0.031 sec, total intersections: 173, total inclusion tests: 405
Common plane time: 0.000 sec
Solutions: 12, dim=2.0, hs=4.6, max=12, maxin=143
f-vector: [0, 0, 12]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.061 GiB
Memory: pmem(rss=72790016, vms=65712128, num_page_faults=21463, peak_wset=72794112, wset=72790016, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40096, nonpaged_pool=40096, pagefile=65712128, peak_pagefile=65712128, private=65712128)
