
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T19:28:11+02:00
Command line: BIOMD0000000397 --z3


Model: BIOMD0000000397
Logfile: db\BIOMD0000000397\ep11-z3-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^38.5 in 76 bags
Bag sizes: 4 4 1 1 1 1 1 17 17 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 4 9 4 4 8 4 4 4 4 7 7 4 2 2 2 4 9 4 2 4 4 2 4 1 1 4 20 7 6 4 3 5 6 12 5 11 5 4 7 3 10 9 5 22 22 2 4 6 11 2 3 4

Minimizing (20,0)... (20,0)


Running SMT solver (1)... 0.031 sec
No solutions found

0 polyhedra, 1 rounds, smt 0.031 sec, isect 0.000 sec, insert 0.000 sec, total 0.094 sec
Preprocessing 0.062 sec, super total 0.156 sec
Comparing... 0.047 sec.  Solution matches input.
Total contains() calls: 0, full: 0, ratio: 0.000%
