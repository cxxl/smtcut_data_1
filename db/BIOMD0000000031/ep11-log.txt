
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:04:22+02:00
Command line: ..\trop\ptcut.py BIOMD0000000031 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000031 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 39L0OQ95GIAED
Effective epsilon: 1/11.0

Reading db\BIOMD0000000031\vector_field_q.txt
Reading db\BIOMD0000000031\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k2*k6*k8*k9 + k2*k6*k8*x1 + k2*k6*k9*x2 + k2*k8*k9*x3 + k6*k8*k9*x1 + k6*k8*x1**2 + k6*k9*x1*x2 + k8*k9*x1*x3

Multiplying equation 2 with: k2*k4*k6*k8*k9 + k2*k4*k6*k8*x1 + k2*k4*k6*k9*x2 + k2*k4*k8*k9*x3 + k2*k6*k8*k9*x2 + k2*k6*k8*x1*x2 + k2*k6*k9*x2**2 + k2*k8*k9*x2*x3 + k4*k6*k8*k9*x1 + k4*k6*k8*x1**2 + k4*k6*k9*x1*x2 + k4*k8*k9*x1*x3 + k6*k8*k9*x1*x2 + k6*k8*x1**2*x2 + k6*k9*x1*x2**2 + k8*k9*x1*x2*x3

Multiplying equation 3 with: k4*k6*k8*k9 + k4*k6*k8*x1 + k4*k6*k9*x2 + k4*k8*k9*x3 + k6*k8*k9*x2 + k6*k8*x1*x2 + k6*k9*x2**2 + k8*k9*x2*x3

System is rational in variable x1,x2,x3.
System is rational in parameter k2,k4,k6,k8,k9.
Tropicalization time: 10.546 sec
Variables: x1-x6
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 0.256 sec
Saving polyhedra...

Sorting ascending...
Dimension: 6
Formulas: 7
Order: 3 4 5 6 0 2 1
Possible combinations: 1 * 1 * 1 * 3 * 6 * 7 * 29 = 3,654 (10^4)
 Applying common planes (codim=3, hs=6)...
 [1/4 (#6)]: 3 => 3 (-), dim=2.0, hs=6.0
 [2/4 (#0)]: 6 => 4 (2 empty), dim=2.0, hs=7.0
  Removed: 0.2,0.6
 [3/4 (#2)]: 7 => 3 (1 empty, 3 incl), dim=2.0, hs=7.0
  Removed: 2.1,2.4,2.5,2.6
 [4/4 (#1)]: 29 => 4 (19 empty, 6 incl), dim=2.0, hs=7.0
  Removed: 1.1,1.2,1.66,1.74,1.75,1.15,1.80,1.20,1.84,1.85,1.27,1.28,1.91,1.93,1.95,1.32,1.98,1.99,1.101,1.38,1.102,1.42,1.51,1.53,1.57
 Savings: factor 25.375 (10^1), 3 formulas
[1/3 (#6, #2)]: 3 * 3 = 9 => 3 (5 empty, 1 incl), dim=1.0, hs=6.3
 Applying common planes (codim=3, hs=4)...
 [1/3 (w1)]: 3 => 3 (-), dim=1.0, hs=6.7
 [2/3 (#0)]: 4 => 4 (-), dim=2.0, hs=7.0
 [3/3 (#1)]: 4 => 4 (-), dim=2.0, hs=7.0
 Savings: factor 1.0, 0 formulas
[2/3 (#0, w1)]: 4 * 3 = 12 => 1 (11 empty), dim=0.0, hs=6.0
 Applying common planes (codim=6, hs=6)...
 [1/1 (#1)]: 4 => 1 (3 empty), dim=0.0, hs=6.0
  Removed: 1.48,1.0,1.11
 Savings: factor 4.0, 1 formulas
Total time: 0.019 sec, total intersections: 81, total inclusion tests: 91
Common plane time: 0.015 sec
Solutions: 1, dim=0.0, hs=6.0, max=3, maxin=12
f-vector: [1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.062 GiB
Memory: pmem(rss=73510912, vms=66453504, num_page_faults=21011, peak_wset=73515008, wset=73510912, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40368, nonpaged_pool=40368, pagefile=66453504, peak_pagefile=66486272, private=66453504)
