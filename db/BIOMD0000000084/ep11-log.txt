
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:11:13+02:00
Command line: ..\trop\ptcut.py BIOMD0000000084 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000084 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 2GUVV770YBZZL
Effective epsilon: 1/11.0

Reading db\BIOMD0000000084\vector_field_q.txt
Reading db\BIOMD0000000084\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k3*k5 + k3*x2 + k5*x1 + x1*x2

Multiplying equation 2 with: k3*k5 + k3*x2 + k5*x1 + x1*x2

Multiplying equation 3 with: k7*k9 + k7*x4 + k9*x3 + x3*x4

Multiplying equation 4 with: k7*k9 + k7*x4 + k9*x3 + x3*x4

Multiplying equation 5 with: k11*k13 + k11*x6 + k13*x5 + x5*x6

Multiplying equation 6 with: k11*k13 + k11*x6 + k13*x5 + x5*x6

Multiplying equation 7 with: k15*k17*k18 + k15*k17*k19 + k15*k19*x8 + k17*k18*x7 + k17*k19*x7 + k19*x7*x8

Multiplying equation 8 with: k15*k17*k18 + k15*k17*k19 + k15*k19*x8 + k17*k18*x7 + k17*k19*x7 + k19*x7*x8

System is rational in variable x1,x2,x3,x4,x5,x6,x7,x8.
System is rational in parameter k1,k3,k5,k7,k9,k11,k13,k15,k17,k18,k19.
Term in equation 7 is zero since k18=0
Term in equation 8 is zero since k18=0
Tropicalization time: 3.447 sec
Variables: x1-x8
Saving tropical system...
Creating polyhedra... 
Creation time: 0.032 sec
Saving polyhedra...

Sorting ascending...
Dimension: 8
Formulas: 12
Order: 0 1 8 9 10 11 2 3 4 5 6 7
Possible combinations: 2 * 2 * 2 * 2 * 2 * 2 * 4 * 4 * 4 * 4 * 4 * 4 = 262,144 (10^5)
 Applying common planes (codim=0, hs=8)...
 [1/12 (#0)]: 2 => 2 (-), dim=7.0, hs=8.5
 [2/12 (#1)]: 2 => 2 (-), dim=7.0, hs=8.5
 [3/12 (#8)]: 2 => 2 (-), dim=7.0, hs=8.0
 [4/12 (#9)]: 2 => 2 (-), dim=7.0, hs=8.0
 [5/12 (#10)]: 2 => 2 (-), dim=7.0, hs=8.0
 [6/12 (#11)]: 2 => 1 (1 empty), dim=7.0, hs=8.0
  Removed: 11.1
 [7/12 (#2)]: 4 => 2 (2 empty), dim=7.0, hs=8.5
  Removed: 2.2,2.3
 [8/12 (#3)]: 4 => 2 (2 empty), dim=7.0, hs=8.5
  Removed: 3.2,3.3
 [9/12 (#4)]: 4 => 2 (2 incl), dim=7.0, hs=9.0
  Removed: 4.2,4.3
 [10/12 (#5)]: 4 => 2 (2 incl), dim=7.0, hs=9.0
  Removed: 5.2,5.3
 [11/12 (#6)]: 4 => 2 (2 incl), dim=7.0, hs=9.0
  Removed: 6.2,6.3
 [12/12 (#7)]: 4 => 2 (2 incl), dim=7.0, hs=9.0
  Removed: 7.2,7.3
 Applying common planes (codim=1, hs=8)...
 [1/11 (#0)]: 2 => 1 (1 empty), dim=6.0, hs=8.0
  Removed: 0.1
 [2/11 (#1)]: 2 => 1 (1 empty), dim=6.0, hs=8.0
  Removed: 1.1
 [3/11 (#8)]: 2 => 2 (-), dim=6.0, hs=8.0
 [4/11 (#9)]: 2 => 2 (-), dim=6.0, hs=8.0
 [5/11 (#10)]: 2 => 2 (-), dim=6.0, hs=8.0
 [6/11 (#2)]: 2 => 2 (-), dim=6.0, hs=8.5
 [7/11 (#3)]: 2 => 2 (-), dim=6.0, hs=8.5
 [8/11 (#4)]: 2 => 2 (-), dim=6.0, hs=9.0
 [9/11 (#5)]: 2 => 2 (-), dim=6.0, hs=9.0
 [10/11 (#6)]: 2 => 2 (-), dim=6.0, hs=9.0
 [11/11 (#7)]: 2 => 2 (-), dim=6.0, hs=9.0
 Applying common planes (codim=2, hs=8)...
 [1/9 (#8)]: 2 => 2 (-), dim=5.0, hs=8.0
 [2/9 (#9)]: 2 => 2 (-), dim=5.0, hs=8.0
 [3/9 (#10)]: 2 => 2 (-), dim=5.0, hs=8.0
 [4/9 (#2)]: 2 => 2 (-), dim=5.0, hs=8.5
 [5/9 (#3)]: 2 => 2 (-), dim=5.0, hs=8.5
 [6/9 (#4)]: 2 => 2 (-), dim=5.0, hs=9.0
 [7/9 (#5)]: 2 => 2 (-), dim=5.0, hs=9.0
 [8/9 (#6)]: 2 => 2 (-), dim=5.0, hs=9.0
 [9/9 (#7)]: 2 => 2 (-), dim=5.0, hs=9.0
 Savings: factor 512 (10^3), 3 formulas
[1/8 (#8, #9)]: 2 * 2 = 4 => 4 (-), dim=4.0, hs=8.0
[2/8 (w1, #10)]: 4 * 2 = 8 => 8 (-), dim=3.0, hs=8.0
[3/8 (w2, #2)]: 8 * 2 = 16 => 4 (12 empty), dim=2.0, hs=8.0
 Applying common planes (codim=2, hs=2)...
 [1/6 (w3)]: 4 => 4 (-), dim=2.0, hs=8.0
 [2/6 (#3)]: 2 => 1 (1 empty), dim=4.0, hs=8.0
  Removed: 3.1
 [3/6 (#4)]: 2 => 2 (-), dim=3.0, hs=8.5
 [4/6 (#5)]: 2 => 2 (-), dim=3.0, hs=8.5
 [5/6 (#6)]: 2 => 2 (-), dim=3.0, hs=9.0
 [6/6 (#7)]: 2 => 2 (-), dim=3.0, hs=9.0
 Savings: factor 2.0, 1 formulas
[4/7 (w3, #4)]: 4 * 2 = 8 => 2 (6 empty), dim=1.0, hs=8.0
 Applying common planes (codim=2, hs=2)...
 [1/4 (w4)]: 2 => 2 (-), dim=1.0, hs=8.0
 [2/4 (#5)]: 2 => 1 (1 empty), dim=2.0, hs=8.0
  Removed: 5.1
 [3/4 (#6)]: 2 => 2 (-), dim=1.0, hs=8.5
 [4/4 (#7)]: 2 => 2 (-), dim=1.0, hs=8.5
 Savings: factor 2.0, 1 formulas
[5/6 (w4, #6)]: 2 * 2 = 4 => 1 (1 empty, 2 incl), dim=1.0, hs=9.0
 Applying common planes (codim=7, hs=9)...
 [1/1 (#7)]: 2 => 1 (1 incl), dim=1.0, hs=9.0
  Removed: 7.1
 Savings: factor 2.0, 1 formulas
Total time: 0.063 sec, total intersections: 140, total inclusion tests: 175
Common plane time: 0.048 sec
Solutions: 1, dim=1.0, hs=9.0, max=8, maxin=16
f-vector: [0, 1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.062 GiB
Memory: pmem(rss=73318400, vms=66310144, num_page_faults=21261, peak_wset=73322496, wset=73318400, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40368, nonpaged_pool=40368, pagefile=66310144, peak_pagefile=66310144, private=66310144)
