
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.4 (default, Aug  9 2019, 18:34:13) [MSC v.1915 64 bit (AMD64)]
numpy 1.16.5, sympy 1.4, gmpy2 2.1.0a5
It is now 2020-04-04T21:53:47+02:00
Command line: ..\trop\ptcut.py BIOMD0000000441 -e11 --rat --no-cache=n --maxruntime 10800 --maxmem 11513M --comment "concurrent=6, timeout=10800"

----------------------------------------------------------------------
Solving BIOMD0000000441 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=10800
Random seed: 3UTFO5O5KH0ZA
Effective epsilon: 1/11.0

Reading db\BIOMD0000000441\vector_field_q.txt
Reading db\BIOMD0000000441\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k16*k3*k4 + k16*k3*x8 + k16*k4*x1 + k16*x1*x8 + k3*k4*x2 + k3*x2*x8 + k4*x1*x2 + x1*x2*x8

Multiplying equation 2 with: k16*k3*k4 + k16*k3*x8 + k16*k4*x1 + k16*x1*x8 + k3*k4*x2 + k3*x2*x8 + k4*x1*x2 + x1*x2*x8

Multiplying equation 3 with: k20*k6*k7 + k20*k6*x8 + k20*k7*x3 + k20*k7*x4 + k20*x3*x8 + k20*x4*x8 + k6*k7*x4 + k6*k7*x5 + k6*x4*x8 + k6*x5*x8 + k7*x3*x4 + k7*x3*x5 + k7*x4**2 + k7*x4*x5 + x3*x4*x8 + x3*x5*x8 + x4**2*x8 + x4*x5*x8

Multiplying equation 4 with: k10*k18*k20*k6*k7*k9 + k10*k18*k20*k6*k7*x3 + k10*k18*k20*k6*k7*x4 + k10*k18*k20*k6*k9*x8 + k10*k18*k20*k6*x3*x8 + k10*k18*k20*k6*x4*x8 + k10*k18*k20*k7*k9*x3 + k10*k18*k20*k7*k9*x4 + k10*k18*k20*k7*x3**2 + 2*k10*k18*k20*k7*x3*x4 + k10*k18*k20*k7*x4**2 + k10*k18*k20*k9*x3*x8 + k10*k18*k20*k9*x4*x8 + k10*k18*k20*x3**2*x8 + 2*k10*k18*k20*x3*x4*x8 + k10*k18*k20*x4**2*x8 + k10*k18*k6*k7*k9*x4 + k10*k18*k6*k7*k9*x5 + k10*k18*k6*k7*x3*x4 + k10*k18*k6*k7*x3*x5 + k10*k18*k6*k7*x4**2 + k10*k18*k6*k7*x4*x5 + k10*k18*k6*k9*x4*x8 + k10*k18*k6*k9*x5*x8 + k10*k18*k6*x3*x4*x8 + k10*k18*k6*x3*x5*x8 + k10*k18*k6*x4**2*x8 + k10*k18*k6*x4*x5*x8 + k10*k18*k7*k9*x3*x4 + k10*k18*k7*k9*x3*x5 + k10*k18*k7*k9*x4**2 + k10*k18*k7*k9*x4*x5 + k10*k18*k7*x3**2*x4 + k10*k18*k7*x3**2*x5 + 2*k10*k18*k7*x3*x4**2 + 2*k10*k18*k7*x3*x4*x5 + k10*k18*k7*x4**3 + k10*k18*k7*x4**2*x5 + k10*k18*k9*x3*x4*x8 + k10*k18*k9*x3*x5*x8 + k10*k18*k9*x4**2*x8 + k10*k18*k9*x4*x5*x8 + k10*k18*x3**2*x4*x8 + k10*k18*x3**2*x5*x8 + 2*k10*k18*x3*x4**2*x8 + 2*k10*k18*x3*x4*x5*x8 + k10*k18*x4**3*x8 + k10*k18*x4**2*x5*x8 + k10*k20*k6*k7*k9*x4 + k10*k20*k6*k7*k9*x5 + k10*k20*k6*k7*x3*x4 + k10*k20*k6*k7*x3*x5 + k10*k20*k6*k7*x4**2 + k10*k20*k6*k7*x4*x5 + k10*k20*k6*k9*x4*x8 + k10*k20*k6*k9*x5*x8 + k10*k20*k6*x3*x4*x8 + k10*k20*k6*x3*x5*x8 + k10*k20*k6*x4**2*x8 + k10*k20*k6*x4*x5*x8 + k10*k20*k7*k9*x3*x4 + k10*k20*k7*k9*x3*x5 + k10*k20*k7*k9*x4**2 + k10*k20*k7*k9*x4*x5 + k10*k20*k7*x3**2*x4 + k10*k20*k7*x3**2*x5 + 2*k10*k20*k7*x3*x4**2 + 2*k10*k20*k7*x3*x4*x5 + k10*k20*k7*x4**3 + k10*k20*k7*x4**2*x5 + k10*k20*k9*x3*x4*x8 + k10*k20*k9*x3*x5*x8 + k10*k20*k9*x4**2*x8 + k10*k20*k9*x4*x5*x8 + k10*k20*x3**2*x4*x8 + k10*k20*x3**2*x5*x8 + 2*k10*k20*x3*x4**2*x8 + 2*k10*k20*x3*x4*x5*x8 + k10*k20*x4**3*x8 + k10*k20*x4**2*x5*x8 + k10*k6*k7*k9*x4**2 + 2*k10*k6*k7*k9*x4*x5 + k10*k6*k7*k9*x5**2 + k10*k6*k7*x3*x4**2 + 2*k10*k6*k7*x3*x4*x5 + k10*k6*k7*x3*x5**2 + k10*k6*k7*x4**3 + 2*k10*k6*k7*x4**2*x5 + k10*k6*k7*x4*x5**2 + k10*k6*k9*x4**2*x8 + 2*k10*k6*k9*x4*x5*x8 + k10*k6*k9*x5**2*x8 + k10*k6*x3*x4**2*x8 + 2*k10*k6*x3*x4*x5*x8 + k10*k6*x3*x5**2*x8 + k10*k6*x4**3*x8 + 2*k10*k6*x4**2*x5*x8 + k10*k6*x4*x5**2*x8 + k10*k7*k9*x3*x4**2 + 2*k10*k7*k9*x3*x4*x5 + k10*k7*k9*x3*x5**2 + k10*k7*k9*x4**3 + 2*k10*k7*k9*x4**2*x5 + k10*k7*k9*x4*x5**2 + k10*k7*x3**2*x4**2 + 2*k10*k7*x3**2*x4*x5 + k10*k7*x3**2*x5**2 + 2*k10*k7*x3*x4**3 + 4*k10*k7*x3*x4**2*x5 + 2*k10*k7*x3*x4*x5**2 + k10*k7*x4**4 + 2*k10*k7*x4**3*x5 + k10*k7*x4**2*x5**2 + k10*k9*x3*x4**2*x8 + 2*k10*k9*x3*x4*x5*x8 + k10*k9*x3*x5**2*x8 + k10*k9*x4**3*x8 + 2*k10*k9*x4**2*x5*x8 + k10*k9*x4*x5**2*x8 + k10*x3**2*x4**2*x8 + 2*k10*x3**2*x4*x5*x8 + k10*x3**2*x5**2*x8 + 2*k10*x3*x4**3*x8 + 4*k10*x3*x4**2*x5*x8 + 2*k10*x3*x4*x5**2*x8 + k10*x4**4*x8 + 2*k10*x4**3*x5*x8 + k10*x4**2*x5**2*x8 + k18*k20*k6*k7*k9*x8 + k18*k20*k6*k7*x3*x8 + k18*k20*k6*k7*x4*x8 + k18*k20*k6*k9*x8**2 + k18*k20*k6*x3*x8**2 + k18*k20*k6*x4*x8**2 + k18*k20*k7*k9*x3*x8 + k18*k20*k7*k9*x4*x8 + k18*k20*k7*x3**2*x8 + 2*k18*k20*k7*x3*x4*x8 + k18*k20*k7*x4**2*x8 + k18*k20*k9*x3*x8**2 + k18*k20*k9*x4*x8**2 + k18*k20*x3**2*x8**2 + 2*k18*k20*x3*x4*x8**2 + k18*k20*x4**2*x8**2 + k18*k6*k7*k9*x4*x8 + k18*k6*k7*k9*x5*x8 + k18*k6*k7*x3*x4*x8 + k18*k6*k7*x3*x5*x8 + k18*k6*k7*x4**2*x8 + k18*k6*k7*x4*x5*x8 + k18*k6*k9*x4*x8**2 + k18*k6*k9*x5*x8**2 + k18*k6*x3*x4*x8**2 + k18*k6*x3*x5*x8**2 + k18*k6*x4**2*x8**2 + k18*k6*x4*x5*x8**2 + k18*k7*k9*x3*x4*x8 + k18*k7*k9*x3*x5*x8 + k18*k7*k9*x4**2*x8 + k18*k7*k9*x4*x5*x8 + k18*k7*x3**2*x4*x8 + k18*k7*x3**2*x5*x8 + 2*k18*k7*x3*x4**2*x8 + 2*k18*k7*x3*x4*x5*x8 + k18*k7*x4**3*x8 + k18*k7*x4**2*x5*x8 + k18*k9*x3*x4*x8**2 + k18*k9*x3*x5*x8**2 + k18*k9*x4**2*x8**2 + k18*k9*x4*x5*x8**2 + k18*x3**2*x4*x8**2 + k18*x3**2*x5*x8**2 + 2*k18*x3*x4**2*x8**2 + 2*k18*x3*x4*x5*x8**2 + k18*x4**3*x8**2 + k18*x4**2*x5*x8**2 + k20*k6*k7*k9*x4*x8 + k20*k6*k7*k9*x5*x8 + k20*k6*k7*x3*x4*x8 + k20*k6*k7*x3*x5*x8 + k20*k6*k7*x4**2*x8 + k20*k6*k7*x4*x5*x8 + k20*k6*k9*x4*x8**2 + k20*k6*k9*x5*x8**2 + k20*k6*x3*x4*x8**2 + k20*k6*x3*x5*x8**2 + k20*k6*x4**2*x8**2 + k20*k6*x4*x5*x8**2 + k20*k7*k9*x3*x4*x8 + k20*k7*k9*x3*x5*x8 + k20*k7*k9*x4**2*x8 + k20*k7*k9*x4*x5*x8 + k20*k7*x3**2*x4*x8 + k20*k7*x3**2*x5*x8 + 2*k20*k7*x3*x4**2*x8 + 2*k20*k7*x3*x4*x5*x8 + k20*k7*x4**3*x8 + k20*k7*x4**2*x5*x8 + k20*k9*x3*x4*x8**2 + k20*k9*x3*x5*x8**2 + k20*k9*x4**2*x8**2 + k20*k9*x4*x5*x8**2 + k20*x3**2*x4*x8**2 + k20*x3**2*x5*x8**2 + 2*k20*x3*x4**2*x8**2 + 2*k20*x3*x4*x5*x8**2 + k20*x4**3*x8**2 + k20*x4**2*x5*x8**2 + k6*k7*k9*x4**2*x8 + 2*k6*k7*k9*x4*x5*x8 + k6*k7*k9*x5**2*x8 + k6*k7*x3*x4**2*x8 + 2*k6*k7*x3*x4*x5*x8 + k6*k7*x3*x5**2*x8 + k6*k7*x4**3*x8 + 2*k6*k7*x4**2*x5*x8 + k6*k7*x4*x5**2*x8 + k6*k9*x4**2*x8**2 + 2*k6*k9*x4*x5*x8**2 + k6*k9*x5**2*x8**2 + k6*x3*x4**2*x8**2 + 2*k6*x3*x4*x5*x8**2 + k6*x3*x5**2*x8**2 + k6*x4**3*x8**2 + 2*k6*x4**2*x5*x8**2 + k6*x4*x5**2*x8**2 + k7*k9*x3*x4**2*x8 + 2*k7*k9*x3*x4*x5*x8 + k7*k9*x3*x5**2*x8 + k7*k9*x4**3*x8 + 2*k7*k9*x4**2*x5*x8 + k7*k9*x4*x5**2*x8 + k7*x3**2*x4**2*x8 + 2*k7*x3**2*x4*x5*x8 + k7*x3**2*x5**2*x8 + 2*k7*x3*x4**3*x8 + 4*k7*x3*x4**2*x5*x8 + 2*k7*x3*x4*x5**2*x8 + k7*x4**4*x8 + 2*k7*x4**3*x5*x8 + k7*x4**2*x5**2*x8 + k9*x3*x4**2*x8**2 + 2*k9*x3*x4*x5*x8**2 + k9*x3*x5**2*x8**2 + k9*x4**3*x8**2 + 2*k9*x4**2*x5*x8**2 + k9*x4*x5**2*x8**2 + x3**2*x4**2*x8**2 + 2*x3**2*x4*x5*x8**2 + x3**2*x5**2*x8**2 + 2*x3*x4**3*x8**2 + 4*x3*x4**2*x5*x8**2 + 2*x3*x4*x5**2*x8**2 + x4**4*x8**2 + 2*x4**3*x5*x8**2 + x4**2*x5**2*x8**2

Multiplying equation 5 with: k10*k18*k9 + k10*k18*x3 + k10*k18*x4 + k10*k9*x4 + k10*k9*x5 + k10*x3*x4 + k10*x3*x5 + k10*x4**2 + k10*x4*x5 + k18*k9*x8 + k18*x3*x8 + k18*x4*x8 + k9*x4*x8 + k9*x5*x8 + x3*x4*x8 + x3*x5*x8 + x4**2*x8 + x4*x5*x8

Multiplying equation 6 with: k12*k24 + k12*x7 + k12*x8 + k24*x6 + k24*x7 + x6*x7 + x6*x8 + x7**2 + x7*x8

Multiplying equation 7 with: k12*k14*k22*k24 + k12*k14*k22*x7 + k12*k14*k22*x8 + k12*k14*k24*x7 + k12*k14*k24*x8 + k12*k14*x7**2 + 2*k12*k14*x7*x8 + k12*k14*x8**2 + k12*k22*k24*x6 + k12*k22*k24*x7 + k12*k22*x6*x7 + k12*k22*x6*x8 + k12*k22*x7**2 + k12*k22*x7*x8 + k12*k24*x6*x7 + k12*k24*x6*x8 + k12*k24*x7**2 + k12*k24*x7*x8 + k12*x6*x7**2 + 2*k12*x6*x7*x8 + k12*x6*x8**2 + k12*x7**3 + 2*k12*x7**2*x8 + k12*x7*x8**2 + k14*k22*k24*x6 + k14*k22*k24*x7 + k14*k22*x6*x7 + k14*k22*x6*x8 + k14*k22*x7**2 + k14*k22*x7*x8 + k14*k24*x6*x7 + k14*k24*x6*x8 + k14*k24*x7**2 + k14*k24*x7*x8 + k14*x6*x7**2 + 2*k14*x6*x7*x8 + k14*x6*x8**2 + k14*x7**3 + 2*k14*x7**2*x8 + k14*x7*x8**2 + k22*k24*x6**2 + 2*k22*k24*x6*x7 + k22*k24*x7**2 + k22*x6**2*x7 + k22*x6**2*x8 + 2*k22*x6*x7**2 + 2*k22*x6*x7*x8 + k22*x7**3 + k22*x7**2*x8 + k24*x6**2*x7 + k24*x6**2*x8 + 2*k24*x6*x7**2 + 2*k24*x6*x7*x8 + k24*x7**3 + k24*x7**2*x8 + x6**2*x7**2 + 2*x6**2*x7*x8 + x6**2*x8**2 + 2*x6*x7**3 + 4*x6*x7**2*x8 + 2*x6*x7*x8**2 + x7**4 + 2*x7**3*x8 + x7**2*x8**2

Multiplying equation 8 with: k14*k22 + k14*x7 + k14*x8 + k22*x6 + k22*x7 + x6*x7 + x6*x8 + x7**2 + x7*x8

System is rational in variable x1,x2,x3,x4,x5,x6,x7,x8.
System is rational in parameter k3,k4,k6,k7,k9,k10,k12,k14,k16,k18,k20,k22,k24.
Tropicalization time: 242.052 sec
Variables: x1-x11
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 66.933 sec
Saving polyhedra...

Sorting ascending...
Dimension: 11
Formulas: 14
Order: 8 9 10 13 11 12 5 7 0 1 2 4 6 3
Possible combinations: 1 * 1 * 1 * 2 * 3 * 3 * 7 * 7 * 12 * 12 * 14 * 14 * 14 * 28 = 9,758,278,656 (10^10)
 Applying common planes (codim=3, hs=11)...
 [1/11 (#13)]: 2 => 2 (-), dim=7.0, hs=11.0
 [2/11 (#11)]: 3 => 3 (-), dim=7.0, hs=11.0
 [3/11 (#12)]: 3 => 3 (-), dim=7.0, hs=11.0
 [4/11 (#5)]: 7 => 7 (-), dim=7.0, hs=12.7
 [5/11 (#7)]: 7 => 7 (-), dim=7.0, hs=12.9
 [6/11 (#0)]: 12 => 4 (2 empty, 6 incl), dim=7.2, hs=12.2
  Removed: 0.1,0.3,0.4,0.5,0.6,0.9,0.10,0.11
 [7/11 (#1)]: 12 => 4 (2 empty, 6 incl), dim=7.2, hs=12.2
  Removed: 1.1,1.3,1.4,1.5,1.6,1.9,1.10,1.11
 [8/11 (#2)]: 14 => 14 (-), dim=7.0, hs=13.3
 [9/11 (#4)]: 14 => 14 (-), dim=7.0, hs=13.3
 [10/11 (#6)]: 14 => 14 (-), dim=7.0, hs=12.3
 [11/11 (#3)]: 28 => 28 (-), dim=7.0, hs=12.8
 Savings: factor 9.0, 3 formulas
[1/10 (#11, #13)]: 3 * 2 = 6 => 6 (-), dim=6.0, hs=11.0
[2/10 (w1, #12)]: 6 * 3 = 18 => 18 (-), dim=5.0, hs=11.0
[3/10 (w2, #0)]: 18 * 4 = 72 => 21 (51 empty), dim=4.6, hs=11.9
 Applying common planes (codim=0, hs=2)...
 [1/8 (w3)]: 21 => 21 (-), dim=4.6, hs=11.9
 [2/8 (#5)]: 7 => 7 (-), dim=7.0, hs=14.7
 [3/8 (#7)]: 7 => 7 (-), dim=7.0, hs=14.9
 [4/8 (#1)]: 4 => 2 (2 incl), dim=7.5, hs=13.0
  Removed: 1.0,1.2
 [5/8 (#2)]: 14 => 12 (2 incl), dim=7.0, hs=14.6
  Removed: 2.10,2.3
 [6/8 (#4)]: 14 => 13 (1 incl), dim=7.0, hs=14.7
  Removed: 4.14
 [7/8 (#6)]: 14 => 14 (-), dim=7.0, hs=14.3
 [8/8 (#3)]: 28 => 22 (6 incl), dim=7.0, hs=14.0
  Removed: 3.3012,3.75,3.4589,3.785,3.149,3.4159
 Savings: factor 3.198135198135198, 0 formulas
[4/10 (w3, #1)]: 21 * 2 = 42 => 21 (9 empty, 12 incl), dim=4.6, hs=11.9
[5/10 (w4, #5)]: 21 * 7 = 147 => 29 (88 empty, 30 incl), dim=3.5, hs=12.4
[6/10 (w5, #7)]: 29 * 7 = 203 => 15 (140 empty, 48 incl), dim=2.5, hs=12.2
[7/10 (w6, #2)]: 15 * 12 = 180 => 14 (153 empty, 13 incl), dim=1.4, hs=12.2
[8/10 (w7, #4)]: 14 * 13 = 182 => 3 (177 empty, 2 incl), dim=0.7, hs=11.7
 Applying common planes (codim=2, hs=4)...
 [1/3 (w8)]: 3 => 3 (-), dim=0.7, hs=11.7
 [2/3 (#6)]: 14 => 5 (6 empty, 3 incl), dim=5.0, hs=13.6
  Removed: 6.0,6.1,6.705,6.393,6.266,6.792,6.59,6.699,6.30
 [3/3 (#3)]: 22 => 5 (17 empty), dim=5.0, hs=14.0
  Removed: 3.0,3.1,3.3109,3.3207,3.296,3.4745,3.5130,3.4714,3.908,3.528,3.3120,3.3220,3.312,3.4697,3.3354,3.2236,3.1407
 Savings: factor 12.32 (10^1), 0 formulas
[9/10 (#6, w8)]: 5 * 3 = 15 => 3 (10 empty, 2 incl), dim=0.7, hs=11.7
 Applying common planes (codim=0, hs=1)...
 [1/2 (w9)]: 3 => 3 (-), dim=0.7, hs=11.7
 [2/2 (#3)]: 5 => 5 (-), dim=5.0, hs=14.0
 Savings: factor 1.0, 0 formulas
[10/10 (#3, w9)]: 5 * 3 = 15 => 3 (11 empty, 1 incl), dim=0.7, hs=11.7
Total time: 0.441 sec, total intersections: 1,152, total inclusion tests: 4,969
Common plane time: 0.179 sec
Solutions: 3, dim=0.7, hs=11.7, max=29, maxin=203
f-vector: [1, 2]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.081 GiB, pagefile 0.076 GiB
Memory: pmem(rss=74604544, vms=68407296, num_page_faults=32346, peak_wset=86695936, wset=74604544, peak_paged_pool=538552, paged_pool=538376, peak_nonpaged_pool=37872, nonpaged_pool=37872, pagefile=68407296, peak_pagefile=81412096, private=68407296)
