
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 8-8-17-4906-17-8-791-8-0-0-0-2-2-0
\ dimension 0; is compact
MAXIMIZE
Subject To
  eq0: +1 x11 = -3   \ x11 = 1331.0
  eq1: +1 x10 = -3   \ x10 = 1331.0
  eq2: +1 x9 = -2    \ x9 = 121.0
  eq3: +1 x8 = -2    \ x8 = 121.0
  eq4: +2 x7 = -5    \ x7 = 401.312
  eq5: +1 x6 = -3    \ x6 = 1331.0
  eq6: +1 x5 = -2    \ x5 = 121.0
  eq7: +2 x4 = -5    \ x4 = 401.312
  eq8: +1 x3 = -3    \ x3 = 1331.0
  eq9: +1 x2 = -3    \ x2 = 1331.0
  eq10: +1 x1 = 0    \ x1 = 1.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

\ combo: 7-7-15-3920-15-8-791-8-0-0-0-2-2-1
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x7 >= 1         \ x7**-1 <= 0.0909091
  ie1: +1 x7 >= -2        \ x7 <= 121.0
  eq0: +1 x11 = -3        \ x11 = 1331.0
  eq1: +1 x10 = -3        \ x10 = 1331.0
  eq2: +1 x9 = -2         \ x9 = 121.0
  eq3: +2 x7 -1 x8 = -3   \ x7**2 * x8**-1 = 1331.0
  eq4: +1 x6 = -3         \ x6 = 1331.0
  eq5: +1 x5 = -2         \ x5 = 121.0
  eq6: +2 x4 = -5         \ x4 = 401.312
  eq7: +1 x3 = -3         \ x3 = 1331.0
  eq8: +1 x2 = -2         \ x2 = 121.0
  eq9: +1 x1 = -3         \ x1 = 1331.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

\ combo: 7-7-15-3920-15-2-250-2-0-0-0-2-2-1
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x4 >= 1          \ x4**-1 <= 0.0909091
  ie1: +2 x4 >= -5         \ x4 <= 401.312
  eq0: +1 x11 = -3         \ x11 = 1331.0
  eq1: +1 x10 = -3         \ x10 = 1331.0
  eq2: +1 x9 = -2          \ x9 = 121.0
  eq3: +4 x4 -1 x8 = -11   \ x4**4 * x8**-1 = 2.85312e+11
  eq4: +2 x4 -1 x7 = -4    \ x4**2 * x7**-1 = 14641.0
  eq5: +1 x6 = -3          \ x6 = 1331.0
  eq6: +2 x4 -1 x5 = -3    \ x4**2 * x5**-1 = 1331.0
  eq7: +1 x3 = -3          \ x3 = 1331.0
  eq8: +1 x2 = -2          \ x2 = 121.0
  eq9: +1 x1 = -3          \ x1 = 1331.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

