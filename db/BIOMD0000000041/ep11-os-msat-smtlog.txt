
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T21:49:57+02:00
Command line: BIOMD0000000041 --ppone --ppsmt


Model: BIOMD0000000041
Logfile: db\BIOMD0000000041\ep11-os-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^13.6 in 13 bags
Bag sizes: 48 54 13 14 14 22 14 13 7 2 4 6 4

Filtering bags...
Collecting...
Bag 1/13, 48 polyhedra
Bag 2/13, 54 polyhedra
Bag 3/13, 13 polyhedra
Bag 4/13, 14 polyhedra
Bag 5/13, 14 polyhedra
Bag 6/13, 22 polyhedra
Bag 7/13, 14 polyhedra
Bag 8/13, 13 polyhedra
Bag 9/13, 7 polyhedra
Bag 10/13, 2 polyhedra
Bag 11/13, 4 polyhedra
Bag 12/13, 6 polyhedra
Bag 13/13, 4 polyhedra
Possible combinations after preprocessing: 10^13.6 in 14 bags
Bag sizes: 1 48 54 13 14 14 22 14 13 7 2 4 6 4
Time: 0.531 sec

Checking for superfluous polyhedra...
Bag 1/14, 54 polyhedra:
  Polyhedron 1... superfluous, 0.031 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... superfluous, 0.016 sec
  Polyhedron 12... superfluous, 0.016 sec
  Polyhedron 13... superfluous, 0.000 sec
  Polyhedron 14... superfluous, 0.016 sec
  Polyhedron 15... superfluous, 0.016 sec
  Polyhedron 16... superfluous, 0.000 sec
  Polyhedron 17... superfluous, 0.016 sec
  Polyhedron 18... superfluous, 0.016 sec
  Polyhedron 19... superfluous, 0.000 sec
  Polyhedron 20... superfluous, 0.016 sec
  Polyhedron 21... superfluous, 0.000 sec
  Polyhedron 22... superfluous, 0.016 sec
  Polyhedron 23... superfluous, 0.000 sec
  Polyhedron 24... superfluous, 0.016 sec
  Polyhedron 25... superfluous, 0.000 sec
  Polyhedron 26... superfluous, 0.016 sec
  Polyhedron 27... superfluous, 0.016 sec
  Polyhedron 28... superfluous, 0.000 sec
  Polyhedron 29... superfluous, 0.016 sec
  Polyhedron 30... superfluous, 0.000 sec
  Polyhedron 31... superfluous, 0.016 sec
  Polyhedron 32... superfluous, 0.000 sec
  Polyhedron 33... superfluous, 0.016 sec
  Polyhedron 34... superfluous, 0.000 sec
  Polyhedron 35... superfluous, 0.016 sec
  Polyhedron 36... superfluous, 0.000 sec
  Polyhedron 37... superfluous, 0.016 sec
  Polyhedron 38... superfluous, 0.000 sec
  Polyhedron 39... superfluous, 0.016 sec
  Polyhedron 40... superfluous, 0.016 sec
  Polyhedron 41... superfluous, 0.000 sec
  Polyhedron 42... superfluous, 0.016 sec
  Polyhedron 43... superfluous, 0.000 sec
  Polyhedron 44... required, 0.016 sec
  Polyhedron 45... superfluous, 0.016 sec
  Polyhedron 46... superfluous, 0.000 sec
  Polyhedron 47... superfluous, 0.000 sec
  Polyhedron 48... superfluous, 0.016 sec
  Polyhedron 49... required, 0.016 sec
  Polyhedron 50... superfluous, 0.000 sec
  Polyhedron 51... superfluous, 0.016 sec
  Polyhedron 52... superfluous, 0.000 sec
  Polyhedron 53... superfluous, 0.016 sec
  Polyhedron 54... superfluous, 0.000 sec
  => 2 polyhedra left
Bag 2/14, 48 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.016 sec
  Polyhedron 9... superfluous, 0.000 sec
  Polyhedron 10... superfluous, 0.016 sec
  Polyhedron 11... superfluous, 0.000 sec
  Polyhedron 12... superfluous, 0.016 sec
  Polyhedron 13... superfluous, 0.000 sec
  Polyhedron 14... superfluous, 0.016 sec
  Polyhedron 15... superfluous, 0.016 sec
  Polyhedron 16... superfluous, 0.000 sec
  Polyhedron 17... superfluous, 0.016 sec
  Polyhedron 18... superfluous, 0.016 sec
  Polyhedron 19... superfluous, 0.000 sec
  Polyhedron 20... superfluous, 0.016 sec
  Polyhedron 21... superfluous, 0.000 sec
  Polyhedron 22... superfluous, 0.000 sec
  Polyhedron 23... superfluous, 0.016 sec
  Polyhedron 24... superfluous, 0.000 sec
  Polyhedron 25... superfluous, 0.016 sec
  Polyhedron 26... superfluous, 0.000 sec
  Polyhedron 27... superfluous, 0.016 sec
  Polyhedron 28... superfluous, 0.000 sec
  Polyhedron 29... superfluous, 0.016 sec
  Polyhedron 30... superfluous, 0.000 sec
  Polyhedron 31... superfluous, 0.000 sec
  Polyhedron 32... superfluous, 0.016 sec
  Polyhedron 33... superfluous, 0.000 sec
  Polyhedron 34... superfluous, 0.000 sec
  Polyhedron 35... superfluous, 0.016 sec
  Polyhedron 36... superfluous, 0.000 sec
  Polyhedron 37... superfluous, 0.000 sec
  Polyhedron 38... superfluous, 0.016 sec
  Polyhedron 39... superfluous, 0.000 sec
  Polyhedron 40... superfluous, 0.000 sec
  Polyhedron 41... superfluous, 0.016 sec
  Polyhedron 42... superfluous, 0.000 sec
  Polyhedron 43... superfluous, 0.016 sec
  Polyhedron 44... required, 0.000 sec
  Polyhedron 45... superfluous, 0.016 sec
  Polyhedron 46... superfluous, 0.000 sec
  Polyhedron 47... required, 0.016 sec
  Polyhedron 48... superfluous, 0.000 sec
  => 2 polyhedra left
Bag 3/14, 22 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.016 sec
  Polyhedron 11... superfluous, 0.000 sec
  Polyhedron 12... superfluous, 0.016 sec
  Polyhedron 13... superfluous, 0.000 sec
  Polyhedron 14... superfluous, 0.016 sec
  Polyhedron 15... superfluous, 0.000 sec
  Polyhedron 16... superfluous, 0.016 sec
  Polyhedron 17... superfluous, 0.000 sec
  Polyhedron 18... superfluous, 0.016 sec
  Polyhedron 19... superfluous, 0.000 sec
  Polyhedron 20... superfluous, 0.000 sec
  Polyhedron 21... required, 0.016 sec
  Polyhedron 22... required, 0.000 sec
  => 2 polyhedra left
Bag 4/14, 14 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... required, 0.000 sec
  Polyhedron 11... superfluous, 0.016 sec
  Polyhedron 12... superfluous, 0.000 sec
  Polyhedron 13... required, 0.000 sec
  Polyhedron 14... superfluous, 0.000 sec
  => 2 polyhedra left
Bag 5/14, 14 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.016 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.000 sec
  Polyhedron 10... superfluous, 0.016 sec
  Polyhedron 11... superfluous, 0.000 sec
  Polyhedron 12... required, 0.000 sec
  Polyhedron 13... required, 0.016 sec
  Polyhedron 14... superfluous, 0.000 sec
  => 2 polyhedra left
Bag 6/14, 14 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.016 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... required, 0.000 sec
  Polyhedron 11... required, 0.000 sec
  Polyhedron 12... superfluous, 0.016 sec
  Polyhedron 13... required, 0.000 sec
  Polyhedron 14... superfluous, 0.000 sec
  => 3 polyhedra left
Bag 7/14, 13 polyhedra:
  Polyhedron 1... superfluous, 0.016 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.016 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... required, 0.000 sec
  Polyhedron 12... required, 0.000 sec
  Polyhedron 13... superfluous, 0.016 sec
  => 2 polyhedra left
Bag 8/14, 13 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.016 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.000 sec
  Polyhedron 7... superfluous, 0.000 sec
  Polyhedron 8... superfluous, 0.000 sec
  Polyhedron 9... superfluous, 0.016 sec
  Polyhedron 10... superfluous, 0.000 sec
  Polyhedron 11... required, 0.000 sec
  Polyhedron 12... required, 0.000 sec
  Polyhedron 13... superfluous, 0.016 sec
  => 2 polyhedra left
Bag 9/14, 7 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... superfluous, 0.000 sec
  Polyhedron 5... superfluous, 0.000 sec
  Polyhedron 6... superfluous, 0.016 sec
  Polyhedron 7... required, 0.000 sec
  => 1 polyhedra left
Bag 10/14, 6 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.000 sec
  Polyhedron 4... required, 0.000 sec
  Polyhedron 5... required, 0.000 sec
  Polyhedron 6... superfluous, 0.000 sec
  => 2 polyhedra left
Bag 11/14, 4 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... required, 0.000 sec
  Polyhedron 4... required, 0.000 sec
  => 2 polyhedra left
Bag 12/14, 4 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... superfluous, 0.000 sec
  Polyhedron 3... superfluous, 0.016 sec
  Polyhedron 4... required, 0.000 sec
  => 1 polyhedra left
Bag 13/14, 2 polyhedra:
  Polyhedron 1... superfluous, 0.000 sec
  Polyhedron 2... required, 0.000 sec
  => 1 polyhedra left
Possible combinations after preprocessing: 1536 in 11 bags
Bag sizes: 1 2 2 2 2 2 3 2 2 2 2
Time: 1.391 sec

Minimizing (2,6)... (2,5)


Running SMT solver (1)... 0.000 sec
Checking... Minimizing (8,38)... (8,6), 0.016 sec
Inserting... 0.000 sec
[1 ph, mem 67/61, smt 0.000, isect 0.016, ins 0.000, tot 0.031]

Running SMT solver (2)... 0.000 sec
Checking... Minimizing (9,37)... (9,2), 0.000 sec
Inserting... 0.000 sec
[2 ph, mem 71/65, smt 0.000, isect 0.016, ins 0.000, tot 0.062]

Running SMT solver (3)... 0.000 sec
Checking... Minimizing (9,30)... (9,4), 0.016 sec
Inserting... 0.000 sec
[3 ph, mem 74/68, smt 0.000, isect 0.031, ins 0.000, tot 0.078]

Running SMT solver (4)... 0.000 sec
Checking... Minimizing (9,29)... (9,3), 0.016 sec
Inserting... 0.000 sec
[4 ph, mem 70/64, smt 0.000, isect 0.047, ins 0.000, tot 0.094]

Running SMT solver (5)... 0.000 sec
No more solutions found

4 polyhedra, 5 rounds, smt 0.000 sec, isect 0.047 sec, insert 0.000 sec, total 0.094 sec
Preprocessing 1.938 sec, super total 2.031 sec
Comparing... 0.047 sec.  Solution matches input.
Total contains() calls: 6, full: 1, ratio: 16.667%
