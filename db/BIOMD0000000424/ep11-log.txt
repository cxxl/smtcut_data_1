
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:24:53+02:00
Command line: ..\trop\ptcut.py BIOMD0000000424 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000424 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 3EUQ16XOG5AD9
Effective epsilon: 1/11.0

Reading db\BIOMD0000000424\vector_field_q.txt
Reading db\BIOMD0000000424\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 5 with: k23 + x6

Multiplying equation 6 with: k23 + x6

Multiplying equation 7 with: k35 + x54

Multiplying equation 14 with: k44*k46 + k44*x53 + k46*x14 + x14*x53

Multiplying equation 16 with: k48*k50 + k48*x52 + k50*x16 + x16*x52

Multiplying equation 18 with: k53 + x18

Multiplying equation 19 with: k53**2 + k53*x18 + k53*x19 + x18*x19

Multiplying equation 25 with: k60*k62 + k60*x26 + k62*x25 + x25*x26

Multiplying equation 26 with: k60**2*k62**2 + k60**2*k62*x26 + k60**2*k62*x51 + k60**2*x26*x51 + k60*k62**2*x25 + k60*k62**2*x26 + k60*k62*x25*x26 + k60*k62*x25*x51 + k60*k62*x26**2 + k60*k62*x26*x51 + k60*x25*x26*x51 + k60*x26**2*x51 + k62**2*x25*x26 + k62*x25*x26**2 + k62*x25*x26*x51 + x25*x26**2*x51

Multiplying equation 30 with: k80 + x30

Multiplying equation 34 with: k72 + x34

Multiplying equation 38 with: k72 + x34

Multiplying equation 40 with: k80**2 + k80*x30 + k80*x40 + x30*x40

Multiplying equation 45 with: k53 + x19

Multiplying equation 46 with: k80 + x40

Multiplying equation 51 with: k60*k62 + k60*x51 + k62*x26 + x26*x51

Multiplying equation 52 with: k48*k50 + k48*x52 + k50*x16 + x16*x52

Multiplying equation 53 with: k44*k46 + k44*x53 + k46*x14 + x14*x53

Multiplying equation 54 with: k35 + x54

System is rational in variable x6,x14,x16,x18,x19,x25,x26,x30,x34,x40,x51,x52,x53,x54.
System is rational in parameter k23,k35,k44,k46,k48,k50,k53,k60,k62,k72,k80,k115.
Term in equation 2 is zero since k12=0
Term in equation 6 is zero since k41=0
Term in equation 6 is zero since k41=0
Term in equation 10 is zero since k33=0
Term in equation 13 is zero since k11=0
Term in equation 28 is zero since k12=0
Term in equation 29 is zero since k39=0
Term in equation 31 is zero since k41=0
Term in equation 34 is zero since k11=0
Term in equation 34 is zero since k11=0
Term in equation 42 is zero since k39=0
Term in equation 42 is zero since k41=0
Term in equation 49 is zero since k33=0
Term in equation 54 is zero since k33=0
Term in equation 54 is zero since k33=0
Term in equation 64 is zero since k124=0
Tropicalization time: 27.516 sec
Variables: x1-x55
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 3.569 sec
Saving polyhedra...

Sorting ascending...
Dimension: 55
Formulas: 65
Order: 61 0 12 19 20 21 26 33 34 36 38 39 40 41 44 45 47 52 6 8 9 10 14 15 18 24 29 30 46 51 57 59 25 53 62 1 5 7 11 22 27 35 42 43 48 50 2 3 28 32 56 63 54 55 60 64 13 49 16 37 58 23 17 31 4
Possible combinations: 0 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 3 * 3 * 3 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 4 * 6 * 6 * 6 * 6 * 6 * 6 * 7 * 7 * 7 * 7 * 8 * 8 * 11 * 11 * 12 * 16 * 24 * 30 * 48 = 0
 Applying common planes (codim=17, hs=58)...
 [1/48 (#61)]: 0 => 0 (-), dim=0.0, hs=0.0
Total time: 0.044 sec, total intersections: 0, total inclusion tests: 0
Common plane time: 0.044 sec
Solutions: 0, max=0, maxin=0
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.115 GiB, pagefile 0.111 GiB
Memory: pmem(rss=81428480, vms=74723328, num_page_faults=33731, peak_wset=123056128, wset=81428480, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=45272, nonpaged_pool=43904, pagefile=74723328, peak_pagefile=118652928, private=74723328)
