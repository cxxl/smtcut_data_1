
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 0-0-0-2-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x3 >= -1       \ x3**-1 <= 11.0
  ie1: +1 x3 >= 0        \ x3 <= 1.0
  eq0: +1 x3 +1 x6 = 1   \ x3 * x6 = 0.0909091
  eq1: +1 x3 +1 x5 = 1   \ x3 * x5 = 0.0909091
  eq2: +1 x3 +1 x4 = 1   \ x3 * x4 = 0.0909091
  eq3: +1 x2 = 0         \ x2 = 1.0
  eq4: +1 x1 = 0         \ x1 = 1.0
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

