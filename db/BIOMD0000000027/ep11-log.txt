
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:03:12+02:00
Command line: ..\trop\ptcut.py BIOMD0000000027 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000027 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 445ES89ZFU71
Effective epsilon: 1/11.0

Reading db\BIOMD0000000027\vector_field_q.txt
Reading db\BIOMD0000000027\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k2*k4*k6*k8*k9 + k2*k4*k6*k8*x1 + k2*k4*k6*k9*x2 + k2*k4*k8*k9*x3 + k2*k6*k8*k9*x2 + k2*k6*k8*x1*x2 + k2*k6*k9*x2**2 + k2*k8*k9*x2*x3 + k4*k6*k8*k9*x1 + k4*k6*k8*x1**2 + k4*k6*k9*x1*x2 + k4*k8*k9*x1*x3

Multiplying equation 2 with: k2*k4*k6*k8*k9 + k2*k4*k6*k8*x1 + k2*k4*k6*k9*x2 + k2*k4*k8*k9*x3 + k2*k6*k8*k9*x2 + k2*k6*k8*x1*x2 + k2*k6*k9*x2**2 + k2*k8*k9*x2*x3 + k4*k6*k8*k9*x1 + k4*k6*k8*x1**2 + k4*k6*k9*x1*x2 + k4*k8*k9*x1*x3

Multiplying equation 3 with: k2*k4*k6*k8*k9 + k2*k4*k6*k8*x1 + k2*k4*k6*k9*x2 + k2*k4*k8*k9*x3 + k2*k6*k8*k9*x2 + k2*k6*k8*x1*x2 + k2*k6*k9*x2**2 + k2*k8*k9*x2*x3 + k4*k6*k8*k9*x1 + k4*k6*k8*x1**2 + k4*k6*k9*x1*x2 + k4*k8*k9*x1*x3

System is rational in variable x1,x2,x3.
System is rational in parameter k2,k4,k6,k8,k9.
Tropicalization time: 7.042 sec
Variables: x1-x5
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 0.106 sec
Saving polyhedra...

Sorting ascending...
Dimension: 5
Formulas: 6
Order: 3 4 5 0 2 1
Possible combinations: 1 * 1 * 3 * 8 * 8 * 13 = 2,496 (10^3)
 Applying common planes (codim=2, hs=5)...
 [1/4 (#5)]: 3 => 3 (-), dim=2.0, hs=5.0
 [2/4 (#0)]: 8 => 4 (4 empty), dim=2.0, hs=6.0
  Removed: 0.8,0.9,0.2,0.6
 [3/4 (#2)]: 8 => 4 (2 empty, 2 incl), dim=2.0, hs=6.0
  Removed: 2.8,2.2,2.6,2.7
 [4/4 (#1)]: 13 => 6 (3 empty, 4 incl), dim=2.0, hs=6.2
  Removed: 1.1,1.11,1.17,1.18,1.25,1.30,1.31
 Savings: factor 8.666666666666666, 2 formulas
[1/3 (#0, #5)]: 4 * 3 = 12 => 4 (8 empty), dim=1.0, hs=5.5
[2/3 (w1, #2)]: 4 * 4 = 16 => 3 (12 empty, 1 incl), dim=0.0, hs=5.0
 Applying common planes (codim=0, hs=4)...
 [1/2 (w2)]: 3 => 3 (-), dim=0.0, hs=5.0
 [2/2 (#1)]: 6 => 3 (1 empty, 2 incl), dim=2.0, hs=6.7
  Removed: 1.0,1.28,1.7
 Savings: factor 2.0, 0 formulas
[3/3 (w2, #1)]: 3 * 3 = 9 => 3 (6 empty), dim=0.0, hs=5.0
Total time: 0.020 sec, total intersections: 78, total inclusion tests: 124
Common plane time: 0.013 sec
Solutions: 3, dim=0.0, hs=5.0, max=4, maxin=16
f-vector: [3]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.062 GiB
Memory: pmem(rss=73129984, vms=66174976, num_page_faults=20792, peak_wset=73134080, wset=73129984, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40232, nonpaged_pool=40232, pagefile=66174976, peak_pagefile=66174976, private=66174976)
