
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:20:11+02:00
Command line: ..\trop\ptcut.py BIOMD0000000290 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000290 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: GIWOXJGTKGDR
Effective epsilon: 1/11.0

Reading db\BIOMD0000000290\vector_field_q.txt
Reading db\BIOMD0000000290\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k2 + x4

Multiplying equation 4 with: k2 + x4

System is rational in variable x4.
System is rational in parameter k2,k17.
Tropicalization time: 1.161 sec
Variables: x1-x4
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.
Creation time: 0.007 sec
Saving polyhedra...

Sorting ascending...
Dimension: 4
Formulas: 4
Order: 2 1 3 0
Possible combinations: 1 * 2 * 2 * 4 = 16 (10^1)
 Applying common planes (codim=2, hs=3)...
 [1/3 (#1)]: 2 => 2 (-), dim=1.0, hs=4.0
 [2/3 (#3)]: 2 => 2 (-), dim=2.0, hs=4.0
 [3/3 (#0)]: 4 => 2 (2 empty), dim=1.0, hs=4.5
  Removed: 0.0,0.2
 Savings: factor 2.0, 1 formulas
[1/2 (#1, #3)]: 2 * 2 = 4 => 3 (1 empty), dim=1.0, hs=4.3
[2/2 (w1, #0)]: 3 * 2 = 6 => 1 (2 empty, 3 incl), dim=0.0, hs=4.0
Total time: 0.006 sec, total intersections: 18, total inclusion tests: 18
Common plane time: 0.004 sec
Solutions: 1, dim=0.0, hs=4.0, max=3, maxin=6
f-vector: [1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.067 GiB, pagefile 0.061 GiB
Memory: pmem(rss=72286208, vms=65269760, num_page_faults=20743, peak_wset=72290304, wset=72286208, peak_paged_pool=545632, paged_pool=545456, peak_nonpaged_pool=40096, nonpaged_pool=40096, pagefile=65269760, peak_pagefile=65269760, private=65269760)
