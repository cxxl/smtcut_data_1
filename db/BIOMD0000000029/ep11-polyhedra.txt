[
  ["#0",
    [[0,0,0,0,1,1,0,0,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,1,0,1,1,1,0,1,1,0,1,1,0,1,0,1],
     [0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1],
     [0,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1],
     [1,0,1,0,0,0,1,1,0,1,1,0,1,1,0,1,0,1],
     [1,1,1,0,0,0,1,1,1,1,1,1,0,1,1,1,1,0],
     [0,1,1,0,1,1,0,0,0,0,0,1,1,1,1,1,1,1],
     [0,1,1,0,1,1,0,0,1,1,1,0,0,0,1,1,1,1],
     [1,0,1,1,0,1,0,1,0,0,0,0,1,1,0,1,0,1],
     [1,1,0,1,1,1,0,1,0,0,0,1,1,1,1,0,1,1],
     [1,1,1,1,1,1,0,1,0,0,0,1,1,0,1,1,1,1],
     [1,0,1,1,0,1,1,0,0,1,1,0,0,0,0,1,0,1],
     [1,1,1,1,1,0,1,0,1,1,1,0,0,0,1,1,1,0],
     [1,1,1,1,1,1,1,0,1,1,0,0,0,0,1,1,1,1],
     [1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,0,0,1],
     [1,1,0,1,1,1,1,1,1,0,1,1,1,1,0,0,1,1],
     [1,0,1,1,0,1,1,1,0,1,1,0,1,1,0,1,0,0],
     [1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,0,0]], [
    [[[1,1,0,-1,0,1,-1]], [[1,0,0,1,0,0,0],[0,0,1,-1,0,0,0],[1,0,0,0,1,0,0],[1,1,0,0,0,0,0]], 0],
    [[[2,1,0,-1,1,1,-1]], [[2,0,0,1,0,0,0],[0,0,1,-1,0,0,0],[-1,0,0,0,-1,0,0],[1,1,0,0,0,0,0],[0,0,0,1,-1,0,0]], 1],
    [[[2,1,0,0,0,1,-1]], [[1,1,0,0,0,0,0],[2,0,0,1,0,0,0],[0,0,1,-1,0,0,0],[-1,0,0,-1,0,0,0],[0,0,0,-1,1,0,0]], 2],
    [[[1,1,-1,0,0,1,-1]], [[0,0,-1,1,0,0,0],[1,0,1,0,0,0,0],[1,1,0,0,0,0,0],[1,0,0,0,1,0,0]], 4],
    [[[2,1,-1,0,1,1,-1]], [[0,0,-1,1,0,0,0],[0,0,1,0,-1,0,0],[1,1,0,0,0,0,0],[2,0,1,0,0,0,0],[-1,0,0,0,-1,0,0]], 5],
    [[[2,1,0,0,0,1,-1]], [[0,0,-1,1,0,0,0],[1,1,0,0,0,0,0],[0,0,-1,0,1,0,0],[-1,0,-1,0,0,0,0],[2,0,1,0,0,0,0]], 7],
    [[[0,0,0,1,0,-1,1]], [[-1,-1,0,0,0,0,0],[2,1,0,0,0,0,0],[0,0,1,-1,0,0,0],[1,0,0,1,0,0,0],[1,0,0,0,1,0,0]], 8],
    [[[0,0,1,0,0,-1,1]], [[2,1,0,0,0,0,0],[1,0,1,0,0,0,0],[-1,-1,0,0,0,0,0],[0,0,-1,1,0,0,0],[1,0,0,0,1,0,0]], 9],
    [[[-1,0,0,1,-1,-1,1]], [[-1,-1,0,0,0,0,0],[1,-1,0,1,0,0,0],[0,0,0,1,-1,0,0],[1,1,0,0,-1,0,0],[0,0,1,-1,0,0,0],[-1,0,0,0,-1,0,0]], 10],
    [[[1,0,0,0,0,1,-1]], [[-1,-1,0,0,0,0,0],[1,-1,0,1,0,0,0],[1,1,0,-1,0,0,0],[0,0,1,-1,0,0,0],[0,0,0,-1,1,0,0],[-1,0,0,-1,0,0,0]], 11],
    [[[2,1,0,-1,0,1,-1]], [[0,0,1,-1,0,0,0],[-1,-1,0,1,0,0,0],[-1,-1,0,0,1,0,0],[-2,-1,0,0,0,0,0]], 13],
    [[[-1,0,1,0,-1,-1,1]], [[1,1,0,0,-1,0,0],[-1,-1,0,0,0,0,0],[0,0,1,0,-1,0,0],[1,-1,1,0,0,0,0],[-1,0,0,0,-1,0,0],[0,0,-1,1,0,0,0]], 14],
    [[[1,0,0,0,0,1,-1]], [[1,1,-1,0,0,0,0],[0,0,-1,1,0,0,0],[-1,-1,0,0,0,0,0],[0,0,-1,0,1,0,0],[1,-1,1,0,0,0,0],[-1,0,-1,0,0,0,0]], 16],
    [[[2,1,-1,0,0,1,-1]], [[-1,-1,1,0,0,0,0],[0,0,-1,1,0,0,0],[-1,-1,0,0,1,0,0],[-2,-1,0,0,0,0,0]], 17],
    [[[0,1,0,-2,1,1,-1]], [[-2,0,0,-1,0,0,0],[0,0,0,1,-1,0,0],[0,0,1,-1,0,0,0],[-1,1,0,-1,0,0,0]], 18],
    [[[0,1,0,-1,0,1,-1]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[0,0,1,-1,0,0,0],[0,0,0,-1,1,0,0]], 19],
    [[[0,1,-2,0,1,1,-1]], [[0,0,1,0,-1,0,0],[-2,0,-1,0,0,0,0],[0,0,-1,1,0,0,0],[-1,1,-1,0,0,0,0]], 24],
    [[[0,1,-1,0,0,1,-1]], [[-1,1,-1,0,0,0,0],[-2,0,-1,0,0,0,0],[0,0,-1,1,0,0,0],[0,0,-1,0,1,0,0]], 26]
  ]],
  ["#1",
    [[0,0,0,0,0,0,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,1,1,0,1,1,1,0,1,0,1,1],
     [0,0,0,0,1,1,1,0,1,1,1,0,1,0,1],
     [0,0,0,0,1,1,1,1,0,1,1,1,1,1,0],
     [0,1,1,1,0,0,0,0,0,0,1,1,1,1,1],
     [0,1,1,1,0,0,1,1,1,1,1,1,1,1,1],
     [1,0,1,1,0,1,0,0,0,0,0,1,0,1,1],
     [1,1,0,1,0,1,0,0,0,0,1,0,1,0,1],
     [1,1,1,0,0,1,0,0,0,0,1,1,1,1,0],
     [1,1,1,1,0,1,0,0,0,0,1,1,1,1,1],
     [1,0,1,1,1,1,0,1,1,1,0,0,0,1,1],
     [1,1,0,1,1,1,1,0,1,1,0,0,1,0,1],
     [1,0,1,1,1,1,0,1,1,1,0,1,0,1,1],
     [1,1,0,1,1,1,1,0,1,1,1,0,1,0,1],
     [1,1,1,0,1,1,1,1,0,1,1,1,1,1,0]], [
    [[[1,1,-1,0,0,1,-1]], [[1,-1,1,0,0,0,0],[1,0,1,0,0,0,0],[1,1,0,0,0,0,0],[1,0,0,1,0,0,0],[1,0,0,0,1,0,0]], 0],
    [[[2,1,-1,0,1,1,-1]], [[0,0,1,0,-1,0,0],[1,-1,1,0,0,0,0],[2,0,0,1,0,0,0],[1,1,0,0,0,0,0],[0,0,0,1,-1,0,0],[-1,0,0,0,-1,0,0]], 1],
    [[[2,1,-1,1,0,1,-1]], [[0,0,1,-1,0,0,0],[1,1,0,0,0,0,0],[0,0,0,-1,1,0,0],[2,0,0,1,0,0,0],[1,-1,1,0,0,0,0],[-1,0,0,-1,0,0,0]], 2],
    [[[2,1,0,0,0,1,-1]], [[1,1,0,0,0,0,0],[0,0,-1,1,0,0,0],[0,0,-1,0,1,0,0],[-1,0,-1,0,0,0,0],[1,-1,1,0,0,0,0]], 3],
    [[[0,0,1,0,0,-1,1]], [[-1,-1,0,0,0,0,0],[2,1,0,0,0,0,0],[1,0,1,0,0,0,0],[1,0,0,1,0,0,0],[1,0,0,0,1,0,0]], 4],
    [[[-1,1,-1,0,0,0,0]], [[-2,0,0,0,0,-1,1],[1,0,0,0,1,0,0],[1,0,0,1,0,0,0],[0,1,0,0,0,0,0]], 5],
    [[[-1,0,1,0,-1,-1,1]], [[-1,-1,0,0,0,0,0],[1,1,0,0,-1,0,0],[1,-1,1,0,0,0,0],[1,-1,0,1,0,0,0],[0,0,1,0,-1,0,0],[-1,0,0,0,-1,0,0],[0,0,0,1,-1,0,0]], 9],
    [[[-1,0,1,-1,0,-1,1]], [[-1,-1,0,0,0,0,0],[1,1,0,-1,0,0,0],[0,0,1,-1,0,0,0],[1,-1,0,1,0,0,0],[0,0,0,-1,1,0,0],[-1,0,0,-1,0,0,0]], 10],
    [[[1,0,0,0,0,1,-1]], [[-1,-1,0,0,0,0,0],[1,1,-1,0,0,0,0],[0,0,-1,1,0,0,0],[0,0,-1,0,1,0,0],[1,-1,1,0,0,0,0],[-1,0,-1,0,0,0,0]], 11],
    [[[2,1,-1,0,0,1,-1]], [[-1,-1,1,0,0,0,0],[-1,-1,0,1,0,0,0],[-1,-1,0,0,1,0,0],[-2,-1,0,0,0,0,0]], 12],
    [[[0,1,-1,-1,1,1,-1]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[1,-1,1,0,0,0,0],[0,0,0,1,-1,0,0]], 16],
    [[[0,1,-1,0,0,1,-1]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[0,0,0,-1,1,0,0],[1,-1,1,0,0,0,0]], 17],
    [[[-1,1,-1,0,0,0,0]], [[-3,0,0,0,-1,-1,1],[-1,0,0,0,-1,0,0],[-1,0,0,1,-1,-1,1],[0,0,0,1,-1,0,0],[-2,1,0,0,-1,-1,1],[-1,1,0,0,-1,0,0]], 22],
    [[[-1,1,-1,0,0,0,0]], [[-3,0,0,-1,0,-1,1],[-1,0,0,-1,0,0,0],[0,0,0,-1,1,0,0],[-1,0,0,0,0,-1,1],[-1,1,0,-1,0,0,0]], 26],
    [[[-1,1,-1,0,0,0,0]], [[-2,-1,0,0,0,-1,1],[0,-1,0,0,0,0,0],[1,-1,0,0,1,0,0],[1,-1,0,1,0,0,0],[-1,0,0,0,0,-1,1]], 30]
  ]],
  ["#2",
    [[0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1],
     [0,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1],
     [0,1,1,1,0,0,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,1,0],
     [1,0,1,1,0,1,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1],
     [1,0,1,1,1,1,0,0,0,1,1,1,1,0,1,1,1,0,1,1,1,1,1],
     [1,1,0,1,1,1,1,0,0,0,0,0,1,0,1,1,1,0,1,1,1,1,1],
     [1,1,0,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1],
     [1,1,0,1,1,1,1,1,0,0,0,0,1,1,0,1,1,1,1,1,1,0,1],
     [1,1,0,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1],
     [1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1],
     [1,1,1,0,1,1,1,0,0,1,1,1,1,0,0,0,1,0,1,1,1,1,1],
     [1,1,1,0,1,1,1,1,1,1,0,1,1,0,0,0,1,1,1,1,1,0,1],
     [1,1,1,0,1,0,1,1,1,1,1,1,1,0,0,0,1,1,0,1,1,1,0],
     [1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1],
     [1,1,1,1,1,1,1,0,0,1,1,1,1,0,1,1,0,0,0,0,1,1,1],
     [1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1,0],
     [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1],
     [1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,0,1,1],
     [1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,0,0],
     [1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,0,0]], [
    [[[0,0,0,1,-1,0,0]], [[0,0,0,0,0,1,-1],[2,0,0,1,0,0,0],[1,0,0,1,0,1,-1],[2,0,1,0,0,0,0],[1,0,1,0,0,1,-1],[1,1,0,-1,0,1,-1],[1,1,0,0,0,0,0],[2,1,1,-1,0,1,-1]], 0],
    [[[0,0,0,1,-1,1,-1]], [[0,0,0,1,-1,0,0],[1,1,0,0,0,0,0],[1,0,0,0,1,0,0],[1,1,0,-1,0,0,0],[1,0,1,0,0,0,0]], 1],
    [[[1,0,0,1,0,1,-1]], [[0,0,0,1,-1,0,0],[-1,0,0,0,-1,0,0],[2,0,0,1,0,0,0],[2,0,1,0,0,0,0],[0,0,1,0,-1,0,0],[1,1,0,-1,0,0,0],[1,1,0,0,0,0,0]], 2],
    [[[1,0,1,1,-1,1,-1]], [[1,1,0,-1,0,0,0],[0,0,0,1,-1,0,0],[-1,0,-1,0,0,0,0],[1,1,0,0,0,0,0],[0,0,-1,0,1,0,0],[2,0,1,0,0,0,0]], 4],
    [[[1,1,0,-1,0,1,-1]], [[0,0,0,-1,1,0,0],[-1,-1,0,1,0,0,0],[1,0,1,0,0,0,0],[1,1,0,0,0,0,0]], 8],
    [[[2,1,1,-1,0,1,-1]], [[0,0,0,-1,1,0,0],[1,1,0,0,0,0,0],[-1,0,-1,0,0,0,0],[-1,-1,0,1,0,0,0],[2,0,1,0,0,0,0]], 12],
    [[[1,1,0,-1,0,0,0]], [[-1,-1,0,0,1,-1,1],[0,0,0,0,0,-1,1],[1,1,0,0,0,-1,1],[1,0,0,0,1,0,0],[0,0,0,0,1,-1,1],[2,1,0,0,0,0,0],[1,0,1,0,0,0,0]], 13],
    [[[1,1,0,-1,1,-1,1]], [[-1,-1,0,0,0,0,0],[0,0,0,1,-1,0,0],[1,1,0,-1,0,0,0],[1,0,1,0,0,0,0],[1,0,0,0,1,0,0]], 14],
    [[[0,1,0,-1,0,-1,1]], [[-1,-1,0,0,0,0,0],[1,1,0,-1,0,0,0],[-1,0,0,0,-1,0,0],[1,-1,0,1,0,0,0],[0,0,1,0,-1,0,0],[1,-1,1,0,0,0,0],[0,0,0,1,-1,0,0]], 20],
    [[[-1,0,0,0,0,1,-1]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[0,0,1,-1,0,0,0],[0,0,0,1,-1,0,0]], 21],
    [[[1,0,1,-1,0,-1,1]], [[-2,0,-1,0,0,0,0],[1,1,0,-1,0,0,0],[-1,1,-1,0,0,0,0],[0,0,-1,1,0,0,0],[0,0,1,0,-1,0,0]], 22],
    [[[1,1,0,-1,0,0,0]], [[-2,-1,0,0,0,-1,1],[-1,0,0,0,0,-1,1],[-1,0,0,0,-1,0,0],[0,0,1,0,-1,0,0],[0,-1,1,0,0,-1,1],[1,1,0,0,-1,0,0]], 23],
    [[[1,1,0,-1,0,0,0]], [[-1,0,0,0,0,-1,1],[-1,-1,1,0,0,0,0],[-2,-1,0,0,0,0,0],[-1,-1,0,0,1,0,0]], 32],
    [[[0,1,-1,-1,1,-1,1]], [[-1,-1,0,0,0,0,0],[1,-1,1,0,0,0,0],[-1,0,-1,0,0,0,0],[1,1,0,-1,0,0,0],[0,0,-1,0,1,0,0],[0,0,0,1,-1,0,0]], 36],
    [[[-1,0,0,1,-1,1,-1]], [[-2,0,-1,0,0,0,0],[0,0,0,1,-1,0,0],[1,1,0,-1,0,0,0],[-1,1,-1,0,0,0,0],[0,0,-1,0,1,0,0]], 38],
    [[[1,1,0,-1,0,0,0]], [[-2,-1,-1,0,1,-1,1],[-1,0,-1,0,0,-1,1],[-1,0,-1,0,0,0,0],[0,1,-1,0,0,-1,1],[0,0,-1,0,1,0,0],[-1,0,-1,0,1,-1,1],[1,1,-1,0,0,0,0],[0,-1,0,0,1,-1,1],[1,0,0,0,0,-1,1]], 41],
    [[[0,0,0,1,0,-1,1]], [[2,1,0,0,0,0,0],[-1,-1,0,0,0,0,0],[-1,-1,0,1,0,0,0],[1,0,1,0,0,0,0],[0,0,0,-1,1,0,0]], 43],
    [[[0,0,0,1,-1,0,0]], [[-1,-1,0,0,0,0,0],[-1,-1,0,0,0,1,-1],[1,-1,0,1,0,0,0],[0,-1,0,1,0,1,-1],[1,-1,1,0,0,0,0],[0,-1,1,0,0,1,-1],[0,0,0,-1,0,1,-1],[2,1,0,-1,0,1,-1],[1,0,1,-1,0,1,-1]], 45],
    [[[1,0,1,-1,0,1,-1]], [[1,1,-1,0,0,0,0],[-1,-1,0,0,0,0,0],[0,0,0,-1,1,0,0],[-1,0,-1,0,0,0,0],[-1,-1,0,1,0,0,0],[1,-1,1,0,0,0,0]], 53],
    [[[2,1,0,-1,0,1,-1]], [[-1,-1,1,0,0,0,0],[0,0,0,-1,1,0,0],[-2,-1,0,0,0,0,0],[-1,-1,0,1,0,0,0]], 54],
    [[[0,0,0,1,-1,0,0]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[-1,0,0,0,0,1,-1],[0,0,1,-1,0,0,0]], 55],
    [[[0,0,0,1,-1,0,0]], [[-2,0,-1,0,0,0,0],[0,1,0,-1,0,1,-1],[0,0,-1,1,0,0,0],[-1,1,-1,0,0,0,0],[-1,0,0,0,0,1,-1]], 58],
    [[[0,1,0,-1,0,1,-1]], [[-1,1,-1,0,0,0,0],[-2,0,-1,0,0,0,0],[-1,-1,0,1,0,0,0],[0,0,0,-1,1,0,0]], 59]
  ]],
  ["#3",
    [[0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,0,0,0,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,0,0,0,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1],
     [0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1],
     [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1],
     [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,1,1,0,1],
     [0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0],
     [0,1,1,1,1,1,1,0,0,1,1,0,1,0,1,0,0,0,0,0,1,1],
     [1,0,1,1,1,1,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,1],
     [1,0,1,1,1,1,1,1,0,0,0,1,0,1,1,1,1,1,1,1,1,1],
     [1,0,1,1,1,1,1,1,0,0,0,1,1,1,0,1,1,1,1,1,0,0],
     [1,1,0,1,1,1,1,0,0,1,1,0,0,0,1,0,0,0,0,0,1,1],
     [1,1,0,1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,1,1,1,1],
     [1,1,1,0,1,1,1,0,0,1,1,0,1,0,0,0,0,0,0,0,1,1],
     [1,1,1,0,1,1,1,1,1,1,0,1,1,0,0,1,1,1,1,1,0,0],
     [1,1,1,1,0,1,1,0,0,1,1,0,1,0,1,0,0,0,0,0,1,1],
     [1,1,1,1,1,1,1,0,0,1,1,0,1,0,1,0,0,0,0,0,1,1],
     [1,1,1,1,1,0,1,0,0,1,1,0,1,0,1,0,0,0,0,0,0,1],
     [1,1,1,1,1,1,0,0,0,1,1,0,1,0,1,0,0,0,0,0,1,0],
     [1,1,1,1,1,1,1,0,0,1,1,0,1,0,1,0,0,0,0,0,1,1],
     [1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,0,1,1,0,0],
     [1,1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,0,0]], [
    [[[0,0,0,1,-1,1,-1]], [[1,1,0,0,0,0,0],[1,0,0,0,1,0,0],[1,0,0,1,0,0,0],[2,0,1,-1,0,0,0],[1,0,1,0,0,0,0]], 0],
    [[[1,0,0,1,0,1,-1]], [[-1,0,0,0,-1,0,0],[2,0,0,1,0,0,0],[0,0,0,1,-1,0,0],[1,1,0,0,0,0,0],[2,0,1,0,0,0,0],[2,0,1,-1,0,0,0],[0,0,1,0,-1,0,0]], 1],
    [[[1,0,0,2,-1,1,-1]], [[1,1,0,0,0,0,0],[-1,0,0,-1,0,0,0],[2,0,0,1,0,0,0],[0,0,0,-1,1,0,0],[0,0,1,-1,0,0,0]], 2],
    [[[1,0,1,1,-1,1,-1]], [[1,1,0,0,0,0,0],[-1,0,-1,0,0,0,0],[2,0,1,-1,0,0,0],[0,0,-1,0,1,0,0],[0,0,-1,1,0,0,0],[2,0,1,0,0,0,0]], 3],
    [[[2,0,1,0,-1,1,-1]], [[-2,0,-1,1,0,0,0],[1,1,0,0,0,0,0],[1,0,1,0,0,0,0],[1,0,0,0,1,0,0]], 4],
    [[[3,0,1,0,0,1,-1]], [[1,1,0,0,0,0,0],[0,0,1,0,-1,0,0],[-2,0,-1,1,0,0,0],[2,0,1,0,0,0,0],[-1,0,0,0,-1,0,0]], 5],
    [[[3,0,2,0,-1,1,-1]], [[1,1,0,0,0,0,0],[-2,0,-1,1,0,0,0],[0,0,-1,0,1,0,0],[2,0,1,0,0,0,0],[-1,0,-1,0,0,0,0]], 6],
    [[[1,1,0,-1,1,-1,1]], [[-1,-1,0,0,0,0,0],[2,1,0,0,0,0,0],[1,0,1,0,0,0,0],[1,0,0,0,1,0,0],[1,0,0,1,0,0,0],[2,0,1,-1,0,0,0]], 7],
    [[[0,1,0,-1,0,-1,1]], [[-1,-1,0,0,0,0,0],[-1,0,0,0,-1,0,0],[1,-1,0,1,0,0,0],[0,0,0,1,-1,0,0],[1,1,0,0,-1,0,0],[1,-1,1,0,0,0,0],[2,0,1,-1,0,0,0],[0,0,1,0,-1,0,0]], 8],
    [[[-1,0,0,0,0,1,-1]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[0,0,0,1,-1,0,0],[0,0,1,-1,0,0,0]], 9],
    [[[1,0,1,-1,0,-1,1]], [[-2,0,-1,0,0,0,0],[-1,1,-1,0,0,0,0],[0,0,-1,1,0,0,0],[0,0,1,0,-1,0,0],[2,0,1,-1,0,0,0]], 10],
    [[[0,1,0,-2,1,-1,1]], [[-1,-1,0,0,0,0,0],[-1,0,0,-1,0,0,0],[1,-1,0,1,0,0,0],[0,0,0,-1,1,0,0],[1,1,0,-1,0,0,0],[0,0,1,-1,0,0,0]], 11],
    [[[-1,0,0,1,-1,1,-1]], [[-2,0,0,-1,0,0,0],[-1,1,0,-1,0,0,0],[0,0,0,-1,1,0,0],[0,0,1,-1,0,0,0]], 12],
    [[[0,1,-1,-1,1,-1,1]], [[-1,-1,0,0,0,0,0],[-1,0,-1,0,0,0,0],[1,1,-1,0,0,0,0],[0,0,-1,0,1,0,0],[0,0,-1,1,0,0,0],[2,0,1,-1,0,0,0],[1,-1,1,0,0,0,0]], 14],
    [[[-1,0,0,1,-1,1,-1]], [[-2,0,-1,0,0,0,0],[-1,1,-1,0,0,0,0],[0,0,-1,1,0,0,0],[0,0,-1,0,1,0,0],[2,0,1,-1,0,0,0]], 16],
    [[[-1,1,-1,0,1,-1,1]], [[-1,-1,0,0,0,0,0],[-2,0,-1,1,0,0,0],[2,1,0,0,0,0,0],[1,0,1,0,0,0,0],[1,0,0,0,1,0,0]], 17],
    [[[1,0,0,1,-1,1,-1]], [[2,0,1,-1,0,0,0],[-2,-1,0,0,0,0,0],[-1,-1,0,0,1,0,0],[-1,-1,0,1,0,0,0],[-1,-1,1,0,0,0,0]], 18],
    [[[-2,1,-1,0,0,-1,1]], [[-1,-1,0,0,0,0,0],[1,1,0,0,-1,0,0],[-2,0,-1,1,0,0,0],[0,0,1,0,-1,0,0],[-1,0,0,0,-1,0,0],[1,-1,1,0,0,0,0]], 19],
    [[[-2,1,-2,0,1,-1,1]], [[-1,-1,0,0,0,0,0],[1,1,-1,0,0,0,0],[-2,0,-1,1,0,0,0],[1,-1,1,0,0,0,0],[0,0,-1,0,1,0,0],[-1,0,-1,0,0,0,0]], 20],
    [[[3,0,1,0,-1,1,-1]], [[-2,0,-1,1,0,0,0],[-1,-1,1,0,0,0,0],[-1,-1,0,0,1,0,0],[-2,-1,0,0,0,0,0]], 21],
    [[[1,0,0,0,0,1,-1]], [[-2,0,-1,0,0,0,0],[-1,1,-1,0,0,0,0],[0,0,1,0,-1,0,0],[-2,0,-1,1,0,0,0]], 22],
    [[[1,0,1,0,-1,1,-1]], [[-2,0,-1,0,0,0,0],[-1,1,-1,0,0,0,0],[-2,0,-1,1,0,0,0],[0,0,-1,0,1,0,0]], 23]
  ]],
  ["#4",
    [[0]], [
    [[[2,0,0,0,0,0,1]], [], 0]
  ]],
  ["#5",
    [[0]], [
    [[[2,0,0,0,0,1,0]], [], 0]
  ]],
  ["#6",
    [[0,0,0,0],
     [0,0,0,0],
     [0,0,0,0],
     [0,0,0,0]], [
    [[[3,0,0,0,1,0,0]], [[3,0,0,1,0,0,0],[3,0,1,0,0,0,0],[3,1,0,0,0,0,0]], 0],
    [[[3,0,0,1,0,0,0]], [[3,0,0,0,1,0,0],[3,0,1,0,0,0,0],[3,1,0,0,0,0,0]], 1],
    [[[3,0,1,0,0,0,0]], [[3,0,0,0,1,0,0],[3,0,0,1,0,0,0],[3,1,0,0,0,0,0]], 2],
    [[[3,1,0,0,0,0,0]], [[3,0,0,0,1,0,0],[3,0,0,1,0,0,0],[3,0,1,0,0,0,0]], 3]
  ]]
]
