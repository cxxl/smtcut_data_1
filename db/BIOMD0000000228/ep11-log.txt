
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:18:59+02:00
Command line: ..\trop\ptcut.py BIOMD0000000228 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000228 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 35J7CPBU37HHX
Effective epsilon: 1/11.0

Reading db\BIOMD0000000228\vector_field_q.txt
Reading db\BIOMD0000000228\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k2*k3*k4 + k2*k3*x2 + k2*k4*x1 + k2*x1*x2 + k3*k4*x3 + k3*x2*x3 + k4*x1*x3 + x1*x2*x3

Multiplying equation 3 with: k11**2*k12*k13 + k11**2*k12*x2 + k11**2*k13*x1 + k11**2*x1*x2 + k12*k13*x3**2 + k12*x2*x3**2 + k13*x1*x3**2 + x1*x2*x3**2

Multiplying equation 4 with: k17*k18*k20 + k17*k18*x5 + k17*k20*x2 + k17*x2*x5 + k18*k20*x1 + k18*x1*x5 + k20*x1*x2 + x1*x2*x5

Multiplying equation 5 with: k20 + x5

Multiplying equation 6 with: k26*k27 + k26*x2 + k27*x1 + x1*x2

Multiplying equation 8 with: k34*k35*k37 + k34*k35*x9 + k34*k37*x2 + k34*x2*x9 + k35*k37*x1 + k35*x1*x9 + k37*x1*x2 + x1*x2*x9

Multiplying equation 9 with: k37 + x9
System is rational in variable x1,x2,x3,x5,x9.
System is rational in parameter k2,k3,k4,k11,k12,k13,k17,k18,k20,k26,k27,k34,k35,k37.
Tropicalization time: 8.613 sec
Variables: x1-x9
Saving tropical system...
Creating polyhedra... 
Creation time: 1.644 sec
Saving polyhedra...

Sorting ascending...
Dimension: 9
Formulas: 9
Order: 6 4 8 1 2 5 7 0 3
Possible combinations: 1 * 2 * 2 * 4 * 5 * 8 * 16 * 19 * 36 = 7,004,160 (10^7)
 Applying common planes (codim=1, hs=5)...
 [1/8 (#4)]: 2 => 2 (-), dim=7.0, hs=6.0
 [2/8 (#8)]: 2 => 2 (-), dim=7.0, hs=6.0
 [3/8 (#1)]: 4 => 2 (2 incl), dim=7.5, hs=7.0
  Removed: 1.0,1.3
 [4/8 (#2)]: 5 => 5 (-), dim=7.0, hs=6.0
 [5/8 (#5)]: 8 => 7 (1 empty), dim=7.0, hs=8.6
  Removed: 5.16
 [6/8 (#7)]: 16 => 12 (4 empty), dim=7.0, hs=9.1
  Removed: 7.75,7.67,7.55,7.79
 [7/8 (#0)]: 19 => 8 (2 empty, 9 incl), dim=7.0, hs=8.5
  Removed: 0.0,0.2,0.8,0.9,0.11,0.47,0.80,0.88,0.58,0.29,0.63
 [8/8 (#3)]: 36 => 28 (8 empty), dim=7.0, hs=10.3
  Removed: 3.97,3.68,3.104,3.9,3.107,3.87,3.56,3.29
 Savings: factor 9.306122448979592, 1 formulas
[1/7 (#4, #8)]: 2 * 2 = 4 => 4 (-), dim=6.0, hs=7.0
[2/7 (w1, #1)]: 4 * 2 = 8 => 6 (2 empty), dim=5.3, hs=8.3
[3/7 (w2, #2)]: 6 * 5 = 30 => 26 (4 incl), dim=4.4, hs=8.6
[4/7 (w3, #5)]: 26 * 7 = 182 => 62 (70 empty, 50 incl), dim=3.4, hs=10.4
[5/7 (w4, #0)]: 62 * 8 = 496 => 57 (333 empty, 106 incl), dim=2.9, hs=10.1
[6/7 (w5, #7)]: 57 * 12 = 684 => 31 (572 empty, 81 incl), dim=2.0, hs=10.3
[7/7 (w6, #3)]: 31 * 28 = 868 => 6 (846 empty, 16 incl), dim=1.3, hs=10.2
Total time: 0.764 sec, total intersections: 2,364, total inclusion tests: 5,890
Common plane time: 0.084 sec
Solutions: 6, dim=1.3, hs=10.2, max=62, maxin=868
f-vector: [0, 4, 2]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.074 GiB, pagefile 0.068 GiB
Memory: pmem(rss=76947456, vms=70139904, num_page_faults=22844, peak_wset=79441920, wset=76947456, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=42000, nonpaged_pool=42000, pagefile=70139904, peak_pagefile=72761344, private=70139904)
