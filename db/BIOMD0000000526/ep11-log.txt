
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:32:52+02:00
Command line: ..\trop\ptcut.py BIOMD0000000526 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000526 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 2CPP19CC12NSD
Effective epsilon: 1/11.0

Reading db\BIOMD0000000526\vector_field_q.txt
Reading db\BIOMD0000000526\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 2 with: k13*k14**3 + 3*k13*k14**2*x18 + 3*k13*k14*x18**2 + k13*x18**3 + k14**3*x1**2 + k14**2*x1**2*x18

Multiplying equation 7 with: k13*k14**3 + 3*k13*k14**2*x18 + 3*k13*k14*x18**2 + k13*x18**3 + k14**3*x1**2 + k14**2*x1**2*x18

System is rational in variable x1,x18.
System is rational in parameter k13,k14.
Tropicalization time: 7.883 sec
Variables: x1-x18
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 6.970 sec
Saving polyhedra...

Sorting ascending...
Dimension: 18
Formulas: 20
Order: 1 2 8 9 10 11 19 12 13 14 15 16 0 4 18 6 17 5 7 3
Possible combinations: 0 * 0 * 0 * 0 * 0 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 4 * 4 * 5 * 6 * 6 * 7 * 7 * 64 = 0
 Applying common planes (codim=2, hs=18)...
 [1/18 (#1)]: 0 => 0 (-), dim=0.0, hs=0.0
Total time: 0.004 sec, total intersections: 0, total inclusion tests: 0
Common plane time: 0.004 sec
Solutions: 0, max=0, maxin=0
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.072 GiB, pagefile 0.066 GiB
Memory: pmem(rss=75493376, vms=68493312, num_page_faults=22291, peak_wset=77479936, wset=75493376, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=41320, nonpaged_pool=41320, pagefile=68493312, peak_pagefile=70635520, private=68493312)
