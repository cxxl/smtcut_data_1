
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:36:02+02:00
Command line: ..\trop\ptcut.py BIOMD0000000625 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000625 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: J59DYLVM2MG3
Effective epsilon: 1/11.0

Reading db\BIOMD0000000625\vector_field_q.txt
Reading db\BIOMD0000000625\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k6 + x13

Multiplying equation 3 with: k6 + x13

Multiplying equation 5 with: k12*k14 + k12*x19 + k14*x6 + x19*x6

Multiplying equation 6 with: k17 + x5

Multiplying equation 12 with: k29 + x11

Multiplying equation 15 with: k23 + x6

Multiplying equation 19 with: k47 + x18
System is rational in variable x5,x6,x11,x13,x18,x19.
System is rational in parameter k6,k12,k13,k14,k17,k23,k29,k47,k52.
System is radical in parameter k13,k52.
Term in equation 5 is zero since k52=0
Term in equation 5 is zero since k52=0
Term in equation 5 is zero since k52=0
Term in equation 5 is zero since k52=0
Tropicalization time: 3.358 sec
Variables: x1-x8, x11-x19
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Warning: formula defines no polyhedron!  Ignored.

Creation time: 0.045 sec
Saving polyhedra...

Sorting ascending...
Dimension: 17
Formulas: 17
Order: 6 8 10 11 13 14 15 2 3 5 7 9 16 1 0 12 4
Possible combinations: 1 * 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 2 * 2 * 3 * 4 * 4 * 8 = 24,576 (10^4)
 Applying common planes (codim=7, hs=14)...
 [1/10 (#2)]: 2 => 2 (-), dim=9.0, hs=15.0
 [2/10 (#3)]: 2 => 2 (-), dim=9.0, hs=14.0
 [3/10 (#5)]: 2 => 2 (-), dim=9.0, hs=16.0
 [4/10 (#7)]: 2 => 2 (-), dim=9.0, hs=14.0
 [5/10 (#9)]: 2 => 2 (-), dim=9.0, hs=15.5
 [6/10 (#16)]: 2 => 2 (-), dim=9.0, hs=16.0
 [7/10 (#1)]: 3 => 3 (-), dim=9.0, hs=15.3
 [8/10 (#0)]: 4 => 4 (-), dim=9.0, hs=15.5
 [9/10 (#12)]: 4 => 4 (-), dim=9.0, hs=16.5
 [10/10 (#4)]: 8 => 4 (4 empty), dim=9.0, hs=17.0
  Removed: 4.0,4.1,4.2,4.3
 Savings: factor 2.0, 7 formulas
[1/9 (#2, #3)]: 2 * 2 = 4 => 4 (-), dim=8.0, hs=15.0
[2/9 (w1, #5)]: 4 * 2 = 8 => 8 (-), dim=7.0, hs=16.8
[3/9 (w2, #7)]: 8 * 2 = 16 => 16 (-), dim=6.0, hs=16.8
[4/9 (w3, #9)]: 16 * 2 = 32 => 24 (8 empty), dim=5.0, hs=17.8
[5/9 (w4, #16)]: 24 * 2 = 48 => 24 (8 empty, 16 incl), dim=4.0, hs=18.8
[6/9 (w5, #1)]: 24 * 3 = 72 => 21 (30 empty, 21 incl), dim=3.0, hs=18.3
[7/9 (w6, #0)]: 21 * 4 = 84 => 17 (59 empty, 8 incl), dim=2.0, hs=17.9
[8/9 (w7, #12)]: 17 * 4 = 68 => 7 (54 empty, 7 incl), dim=1.0, hs=17.7
[9/9 (w8, #4)]: 7 * 4 = 28 => 4 (22 empty, 2 incl), dim=0.0, hs=17.0
Total time: 0.303 sec, total intersections: 391, total inclusion tests: 1,812
Common plane time: 0.055 sec
Solutions: 4, dim=0.0, hs=17.0, max=24, maxin=84
f-vector: [4]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.072 GiB, pagefile 0.065 GiB
Memory: pmem(rss=75526144, vms=68546560, num_page_faults=22317, peak_wset=76984320, wset=75526144, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=41456, nonpaged_pool=41456, pagefile=68546560, peak_pagefile=70193152, private=68546560)
