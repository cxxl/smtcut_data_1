
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-03T22:38:42+02:00
Command line: ..\trop\ptcut.py BIOMD0000000599 -e11 --no-cache=n --maxruntime 3600 --maxmem 41393M --comment "concurrent=1, timeout=3600"

----------------------------------------------------------------------
Solving BIOMD0000000599 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=False, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=1, timeout=3600
Random seed: 5G1U0U8N0BO9
Effective epsilon: 1/11.0

Loading tropical system... done.
Loading polyhedra... done.

Sorting ascending...
Dimension: 30
Formulas: 33
Order: 4 6 8 9 11 15 16 19 20 21 24 25 27 28 3 5 10 26 12 14 17 22 23 2 7 31 32 30 13 18 0 1 29
Possible combinations: 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 1 * 2 * 2 * 2 * 2 * 4 * 4 * 4 * 4 * 4 * 9 * 9 * 9 * 9 * 14 * 15 * 21 * 28 * 28 * 36 = 13,379,723,235,164,160 (10^16)
 Applying common planes (codim=14, hs=30)...
 [1/19 (#3)]: 2 => 2 (-), dim=15.0, hs=31.0
 [2/19 (#5)]: 2 => 2 (-), dim=15.0, hs=31.0
 [3/19 (#10)]: 2 => 2 (-), dim=15.0, hs=30.5
 [4/19 (#26)]: 2 => 2 (-), dim=15.0, hs=30.5
 [5/19 (#12)]: 4 => 4 (-), dim=15.0, hs=32.0
 [6/19 (#14)]: 4 => 4 (-), dim=15.0, hs=32.5
 [7/19 (#17)]: 4 => 4 (-), dim=15.0, hs=32.2
 [8/19 (#22)]: 4 => 4 (-), dim=15.0, hs=32.5
 [9/19 (#23)]: 4 => 4 (-), dim=15.0, hs=32.0
 [10/19 (#2)]: 9 => 3 (6 empty), dim=15.0, hs=32.0
  Removed: 2.3,2.4,2.5,2.6,2.7,2.8
 [11/19 (#7)]: 9 => 4 (5 incl), dim=15.0, hs=29.8
  Removed: 7.1,7.2,7.5,7.7,7.8
 [12/19 (#31)]: 9 => 4 (3 empty, 2 incl), dim=15.0, hs=29.5
  Removed: 31.0,31.2,31.4,31.5,31.7
 [13/19 (#32)]: 9 => 5 (1 empty, 3 incl), dim=15.0, hs=29.6
  Removed: 32.2,32.5,32.6,32.7
 [14/19 (#30)]: 14 => 5 (7 empty, 2 incl), dim=15.0, hs=29.4
  Removed: 30.0,30.1,30.3,30.4,30.6,30.9,30.10,30.11,30.12
 [15/19 (#13)]: 15 => 9 (6 empty), dim=15.0, hs=34.3
  Removed: 13.9,13.10,13.11,13.12,13.13,13.14
 [16/19 (#18)]: 21 => 15 (6 incl), dim=15.0, hs=32.7
  Removed: 18.6,18.7,18.8,18.9,18.10,18.11
 [17/19 (#0)]: 28 => 10 (4 empty, 14 incl), dim=15.0, hs=32.0
  Removed: 0.2,0.3,0.7,0.10,0.11,0.12,0.13,0.14,0.15,0.18,0.20,0.21,0.22,0.23,0.24,0.25,0.26,0.27
 [18/19 (#1)]: 28 => 10 (4 empty, 14 incl), dim=15.0, hs=32.3
  Removed: 1.2,1.3,1.6,1.7,1.8,1.9,1.10,1.11,1.15,1.18,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27
 [19/19 (#29)]: 36 => 36 (-), dim=15.0, hs=35.5
 Savings: factor 1,400 (10^3), 14 formulas
[1/18 (#3, #5)]: 2 * 2 = 4 => 4 (-), dim=14.0, hs=32.0
 Applying common planes (codim=14, hs=21)...
 [1/18 (w1)]: 4 => 4 (-), dim=14.0, hs=34.0
 [2/18 (#10)]: 2 => 2 (-), dim=15.0, hs=33.5
 [3/18 (#26)]: 2 => 2 (-), dim=15.0, hs=33.5
 [4/18 (#12)]: 4 => 4 (-), dim=15.0, hs=34.5
 [5/18 (#14)]: 4 => 3 (1 incl), dim=15.0, hs=34.7
  Removed: 14.3
 [6/18 (#17)]: 4 => 4 (-), dim=15.0, hs=35.2
 [7/18 (#22)]: 4 => 3 (1 incl), dim=15.0, hs=34.7
  Removed: 22.3
 [8/18 (#23)]: 4 => 4 (-), dim=15.0, hs=35.0
 [9/18 (#2)]: 3 => 3 (-), dim=15.0, hs=32.0
 [10/18 (#7)]: 4 => 4 (-), dim=15.0, hs=32.8
 [11/18 (#31)]: 4 => 4 (-), dim=15.0, hs=32.2
 [12/18 (#32)]: 5 => 5 (-), dim=15.0, hs=32.6
 [13/18 (#30)]: 5 => 5 (-), dim=15.0, hs=32.4
 [14/18 (#13)]: 9 => 7 (2 incl), dim=15.0, hs=36.3
  Removed: 13.0,13.3
 [15/18 (#18)]: 15 => 15 (-), dim=15.0, hs=35.7
 [16/18 (#0)]: 10 => 10 (-), dim=15.0, hs=34.9
 [17/18 (#1)]: 10 => 10 (-), dim=15.0, hs=35.2
 [18/18 (#29)]: 36 => 21 (15 incl), dim=15.0, hs=35.7
  Removed: 29.35,29.6,29.7,29.8,29.9,29.10,29.11,29.12,29.13,29.14,29.15,29.16,29.17,29.23,29.29
 Savings: factor 3.9183673469387754, 0 formulas
[2/18 (w1, #10)]: 4 * 2 = 8 => 8 (-), dim=13.0, hs=34.5
[3/18 (w2, #26)]: 8 * 2 = 16 => 16 (-), dim=12.0, hs=35.0
[4/18 (w3, #14)]: 16 * 3 = 48 => 24 (24 incl), dim=11.3, hs=33.8
[5/18 (w4, #22)]: 24 * 3 = 72 => 12 (60 incl), dim=10.7, hs=33.3
[6/18 (w5, #2)]: 12 * 3 = 36 => 16 (16 empty, 4 incl), dim=10.5, hs=32.6
[7/18 (w6, #12)]: 16 * 4 = 64 => 56 (8 incl), dim=9.4, hs=34.2
[8/18 (w7, #17)]: 56 * 4 = 224 => 108 (116 empty), dim=8.1, hs=34.2
[9/18 (w8, #23)]: 108 * 4 = 432 => 54 (250 empty, 128 incl), dim=7.1, hs=33.4
[10/18 (w9, #7)]: 54 * 4 = 216 => 46 (101 empty, 69 incl), dim=6.4, hs=32.7
 Applying common planes (codim=3, hs=3)...
 [1/9 (w10)]: 46 => 46 (-), dim=6.4, hs=32.7
 [2/9 (#31)]: 4 => 3 (1 incl), dim=14.0, hs=31.7
  Removed: 31.3
 [3/9 (#32)]: 5 => 5 (-), dim=14.0, hs=31.6
 [4/9 (#30)]: 5 => 4 (1 incl), dim=14.0, hs=31.8
  Removed: 30.8
 [5/9 (#13)]: 7 => 7 (-), dim=14.0, hs=35.3
 [6/9 (#18)]: 15 => 12 (1 empty, 2 incl), dim=14.0, hs=34.9
  Removed: 18.12,18.13,18.14
 [7/9 (#0)]: 10 => 10 (-), dim=14.0, hs=33.9
 [8/9 (#1)]: 10 => 8 (2 incl), dim=14.1, hs=34.9
  Removed: 1.12,1.13
 [9/9 (#29)]: 21 => 16 (5 incl), dim=14.0, hs=34.8
  Removed: 29.24,29.25,29.26,29.27,29.28
 Savings: factor 3.41796875, 0 formulas
[11/18 (w10, #31)]: 46 * 3 = 138 => 32 (100 empty, 6 incl), dim=5.6, hs=31.9
[12/18 (w11, #30)]: 32 * 4 = 128 => 31 (75 empty, 22 incl), dim=4.4, hs=31.6
[13/18 (w12, #32)]: 31 * 5 = 155 => 36 (54 empty, 65 incl), dim=3.6, hs=31.5
[14/18 (w13, #13)]: 36 * 7 = 252 => 32 (180 empty, 40 incl), dim=3.2, hs=31.5
[15/18 (w14, #1)]: 32 * 8 = 256 => 38 (185 empty, 33 incl), dim=3.1, hs=31.4
[16/18 (w15, #0)]: 38 * 10 = 380 => 38 (252 empty, 90 incl), dim=3.1, hs=31.4
[17/18 (w16, #18)]: 38 * 12 = 456 => 20 (332 empty, 104 incl), dim=2.8, hs=31.4
[18/18 (w17, #29)]: 20 * 16 = 320 => 24 (258 empty, 38 incl), dim=2.8, hs=31.6
Total time: 5.796 sec, total intersections: 3,663, total inclusion tests: 23,128
Common plane time: 1.133 sec
Solutions: 24, dim=2.8, hs=31.6, max=108, maxin=456
f-vector: [0, 0, 12, 6, 6]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.109 GiB, pagefile 0.105 GiB
Memory: pmem(rss=73699328, vms=66842624, num_page_faults=34568, peak_wset=117272576, wset=73699328, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=47720, nonpaged_pool=41320, pagefile=66842624, peak_pagefile=112402432, private=66842624)
