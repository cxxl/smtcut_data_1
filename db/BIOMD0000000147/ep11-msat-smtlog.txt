
----------------------------------------------------------------------
This is SMTcut v4.6.0 by Christoph Lueders -- http://wrogn.com
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
pysmt 0.8.0, gmpy2 2.1.0a5
Datetime: 2020-04-09T19:44:58+02:00
Command line: BIOMD0000000147


Model: BIOMD0000000147
Logfile: db\BIOMD0000000147\ep11-msat-smtlog.txt
Loading polyhedra...  done.
Possible combinations: 10^15.9 in 25 bags
Bag sizes: 2 12 4 4 6 1 2 49 36 16 4 2 1 6 4 12 1 1 12 4 6 1 2 4 11

Minimizing (5,0)... (5,0)


Running SMT solver (1)... 0.094 sec
Checking... Minimizing (25,66)... (23,4), 0.047 sec
Inserting... 0.000 sec
[1 ph, mem 52/45, smt 0.094, isect 0.047, ins 0.000, tot 0.234]

Running SMT solver (2)... 0.016 sec
Checking... Minimizing (25,66)... (23,4), 0.047 sec
Inserting... 0.000 sec
[2 ph, mem 58/51, smt 0.109, isect 0.094, ins 0.000, tot 0.312]

Running SMT solver (3)... 0.031 sec
Checking... Minimizing (20,73)... (20,14), 0.078 sec
Inserting... 0.000 sec
[3 ph, mem 61/55, smt 0.141, isect 0.172, ins 0.000, tot 0.422]

Running SMT solver (4)... 0.031 sec
Checking... Minimizing (20,71)... (20,7), 0.062 sec
Inserting... 0.000 sec
[4 ph, mem 64/57, smt 0.172, isect 0.234, ins 0.000, tot 0.531]

Running SMT solver (5)... 0.016 sec
Checking... Minimizing (20,73)... (20,11), 0.078 sec
Inserting... 0.000 sec
[5 ph, mem 58/52, smt 0.188, isect 0.312, ins 0.000, tot 0.625]

Running SMT solver (6)... 0.016 sec
Checking... Minimizing (20,73)... (20,11), 0.078 sec
Inserting... 0.016 sec
[5 ph, mem 63/56, smt 0.203, isect 0.391, ins 0.016, tot 0.734]

Running SMT solver (7)... 0.016 sec
Checking... Minimizing (21,71)... (21,5), 0.062 sec
Inserting... 0.000 sec
[6 ph, mem 65/58, smt 0.219, isect 0.453, ins 0.016, tot 0.812]

Running SMT solver (8)... 0.016 sec
Checking... Minimizing (20,73)... (20,12), 0.062 sec
Inserting... 0.000 sec
[7 ph, mem 68/61, smt 0.234, isect 0.516, ins 0.016, tot 0.891]

Running SMT solver (9)... 0.016 sec
Checking... Minimizing (20,68)... (20,12), 0.094 sec
Inserting... 0.000 sec
[8 ph, mem 62/55, smt 0.250, isect 0.609, ins 0.016, tot 1.000]

Running SMT solver (10)... 0.000 sec
Checking... Minimizing (20,68)... (20,11), 0.062 sec
Inserting... 0.000 sec
[9 ph, mem 65/58, smt 0.250, isect 0.672, ins 0.016, tot 1.094]

Running SMT solver (11)... 0.000 sec
Checking... Minimizing (20,68)... (20,13), 0.078 sec
Inserting... 0.016 sec
[10 ph, mem 69/62, smt 0.250, isect 0.750, ins 0.031, tot 1.188]

Running SMT solver (12)... 0.016 sec
Checking... Minimizing (21,72)... (20,8), 0.078 sec
Inserting... 0.000 sec
[11 ph, mem 72/65, smt 0.266, isect 0.828, ins 0.031, tot 1.281]

Running SMT solver (13)... 0.016 sec
Checking... Minimizing (21,73)... (21,8), 0.078 sec
Inserting... 0.000 sec
[12 ph, mem 67/59, smt 0.281, isect 0.906, ins 0.031, tot 1.375]

Running SMT solver (14)... 0.016 sec
Checking... Minimizing (20,73)... (20,11), 0.062 sec
Inserting... 0.000 sec
[12 ph, mem 70/63, smt 0.297, isect 0.969, ins 0.031, tot 1.453]

Running SMT solver (15)... 0.016 sec
Checking... Minimizing (20,70)... (20,11), 0.078 sec
Inserting... 0.000 sec
[13 ph, mem 72/65, smt 0.312, isect 1.047, ins 0.031, tot 1.547]

Running SMT solver (16)... 0.000 sec
Checking... Minimizing (20,70)... (20,12), 0.062 sec
Inserting... 0.016 sec
[13 ph, mem 76/68, smt 0.312, isect 1.109, ins 0.047, tot 1.641]

Running SMT solver (17)... 0.000 sec
Checking... Minimizing (20,70)... (20,11), 0.094 sec
Inserting... 0.000 sec
[13 ph, mem 71/64, smt 0.312, isect 1.203, ins 0.047, tot 1.734]

Running SMT solver (18)... 0.016 sec
Checking... Minimizing (20,70)... (20,12), 0.062 sec
Inserting... 0.000 sec
[13 ph, mem 75/68, smt 0.328, isect 1.266, ins 0.047, tot 1.828]

Running SMT solver (19)... 0.000 sec
Checking... Minimizing (20,72)... (20,7), 0.078 sec
Inserting... 0.000 sec
[14 ph, mem 77/70, smt 0.328, isect 1.344, ins 0.047, tot 1.906]

Running SMT solver (20)... 0.016 sec
Checking... Minimizing (20,73)... (19,17), 0.078 sec
Inserting... 0.016 sec
[14 ph, mem 71/64, smt 0.344, isect 1.422, ins 0.062, tot 2.016]

Running SMT solver (21)... 0.016 sec
Checking... Minimizing (19,69)... (19,25), 0.078 sec
Inserting... 0.000 sec
[15 ph, mem 74/67, smt 0.359, isect 1.500, ins 0.062, tot 2.109]

Running SMT solver (22)... 0.016 sec
Checking... Minimizing (19,71)... (19,24), 0.078 sec
Inserting... 0.000 sec
[16 ph, mem 76/69, smt 0.375, isect 1.578, ins 0.062, tot 2.203]

Running SMT solver (23)... 0.016 sec
Checking... Minimizing (19,71)... (19,19), 0.094 sec
Inserting... 0.000 sec
[17 ph, mem 79/72, smt 0.391, isect 1.672, ins 0.062, tot 2.312]

Running SMT solver (24)... 0.016 sec
Checking... Minimizing (19,70)... (19,15), 0.062 sec
Inserting... 0.016 sec
[17 ph, mem 75/68, smt 0.406, isect 1.734, ins 0.078, tot 2.406]

Running SMT solver (25)... 0.016 sec
Checking... Minimizing (19,70)... (19,15), 0.078 sec
Inserting... 0.000 sec
[18 ph, mem 80/72, smt 0.422, isect 1.812, ins 0.078, tot 2.500]

Running SMT solver (26)... 0.016 sec
Checking... Minimizing (19,71)... (19,18), 0.078 sec
Inserting... 0.000 sec
[18 ph, mem 84/77, smt 0.438, isect 1.891, ins 0.078, tot 2.609]

Running SMT solver (27)... 0.000 sec
Checking... Minimizing (19,70)... (19,18), 0.062 sec
Inserting... 0.016 sec
[19 ph, mem 89/82, smt 0.438, isect 1.953, ins 0.094, tot 2.703]

Running SMT solver (28)... 0.016 sec
Checking... Minimizing (19,70)... (19,20), 0.078 sec
Inserting... 0.000 sec
[20 ph, mem 79/72, smt 0.453, isect 2.031, ins 0.094, tot 2.797]

Running SMT solver (29)... 0.016 sec
Checking... Minimizing (19,70)... (19,15), 0.062 sec
Inserting... 0.016 sec
[20 ph, mem 85/77, smt 0.469, isect 2.094, ins 0.109, tot 2.891]

Running SMT solver (30)... 0.016 sec
Checking... Minimizing (19,70)... (19,17), 0.078 sec
Inserting... 0.016 sec
[20 ph, mem 89/82, smt 0.484, isect 2.172, ins 0.125, tot 3.000]

Running SMT solver (31)... 0.000 sec
Checking... Minimizing (19,70)... (19,17), 0.094 sec
Inserting... 0.000 sec
[20 ph, mem 81/74, smt 0.484, isect 2.266, ins 0.125, tot 3.109]

Running SMT solver (32)... 0.000 sec
Checking... Minimizing (19,70)... (19,17), 0.078 sec
Inserting... 0.000 sec
[21 ph, mem 84/76, smt 0.484, isect 2.344, ins 0.125, tot 3.203]

Running SMT solver (33)... 0.000 sec
Checking... Minimizing (19,70)... (19,18), 0.078 sec
Inserting... 0.031 sec
[21 ph, mem 92/85, smt 0.484, isect 2.422, ins 0.156, tot 3.312]

Running SMT solver (34)... 0.000 sec
Checking... Minimizing (19,70)... (19,20), 0.078 sec
Inserting... 0.031 sec
[22 ph, mem 68/61, smt 0.484, isect 2.500, ins 0.188, tot 3.438]

Running SMT solver (35)... 0.016 sec
Checking... Minimizing (19,70)... (19,17), 0.062 sec
Inserting... 0.016 sec
[23 ph, mem 73/66, smt 0.500, isect 2.562, ins 0.203, tot 3.531]

Running SMT solver (36)... 0.016 sec
Checking... Minimizing (19,70)... (19,20), 0.078 sec
Inserting... 0.000 sec
[24 ph, mem 78/71, smt 0.516, isect 2.641, ins 0.203, tot 3.625]

Running SMT solver (37)... 0.016 sec
Checking... Minimizing (20,73)... (20,13), 0.062 sec
Inserting... 0.016 sec
[24 ph, mem 69/62, smt 0.531, isect 2.703, ins 0.219, tot 3.734]

Running SMT solver (38)... 0.016 sec
Checking... Minimizing (19,70)... (19,20), 0.062 sec
Inserting... 0.000 sec
[25 ph, mem 72/65, smt 0.547, isect 2.766, ins 0.219, tot 3.812]

Running SMT solver (39)... 0.016 sec
Checking... Minimizing (19,71)... (19,23), 0.078 sec
Inserting... 0.000 sec
[26 ph, mem 75/67, smt 0.562, isect 2.844, ins 0.219, tot 3.906]

Running SMT solver (40)... 0.016 sec
Checking... Minimizing (19,71)... (19,19), 0.078 sec
Inserting... 0.000 sec
[27 ph, mem 77/70, smt 0.578, isect 2.922, ins 0.219, tot 4.000]

Running SMT solver (41)... 0.016 sec
Checking... Minimizing (20,73)... (20,16), 0.078 sec
Inserting... 0.016 sec
[28 ph, mem 79/72, smt 0.594, isect 3.000, ins 0.234, tot 4.109]

Running SMT solver (42)... 0.000 sec
Checking... Minimizing (20,73)... (20,16), 0.062 sec
Inserting... 0.000 sec
[29 ph, mem 72/64, smt 0.594, isect 3.062, ins 0.234, tot 4.188]

Running SMT solver (43)... 0.016 sec
Checking... Minimizing (20,73)... (20,16), 0.078 sec
Inserting... 0.016 sec
[30 ph, mem 75/68, smt 0.609, isect 3.141, ins 0.250, tot 4.297]

Running SMT solver (44)... 0.000 sec
Checking... Minimizing (20,73)... (20,15), 0.078 sec
Inserting... 0.000 sec
[31 ph, mem 80/72, smt 0.609, isect 3.219, ins 0.250, tot 4.375]

Running SMT solver (45)... 0.016 sec
Checking... Minimizing (19,71)... (19,20), 0.062 sec
Inserting... 0.016 sec
[32 ph, mem 82/75, smt 0.625, isect 3.281, ins 0.266, tot 4.469]

Running SMT solver (46)... 0.000 sec
Checking... Minimizing (19,70)... (19,19), 0.078 sec
Inserting... 0.000 sec
[33 ph, mem 74/67, smt 0.625, isect 3.359, ins 0.266, tot 4.547]

Running SMT solver (47)... 0.016 sec
Checking... Minimizing (19,70)... (19,22), 0.078 sec
Inserting... 0.000 sec
[34 ph, mem 77/70, smt 0.641, isect 3.438, ins 0.266, tot 4.641]

Running SMT solver (48)... 0.016 sec
Checking... Minimizing (19,70)... (19,21), 0.078 sec
Inserting... 0.000 sec
[35 ph, mem 80/72, smt 0.656, isect 3.516, ins 0.266, tot 4.734]

Running SMT solver (49)... 0.016 sec
Checking... Minimizing (20,73)... (20,13), 0.078 sec
Inserting... 0.000 sec
[36 ph, mem 82/75, smt 0.672, isect 3.594, ins 0.266, tot 4.828]

Running SMT solver (50)... 0.016 sec
Checking... Minimizing (20,71)... (20,10), 0.062 sec
Inserting... 0.016 sec
[36 ph, mem 79/72, smt 0.688, isect 3.656, ins 0.281, tot 4.922]

Running SMT solver (51)... 0.000 sec
Checking... Minimizing (20,71)... (20,18), 0.078 sec
Inserting... 0.000 sec
[37 ph, mem 81/73, smt 0.688, isect 3.734, ins 0.281, tot 5.016]

Running SMT solver (52)... 0.016 sec
Checking... Minimizing (20,71)... (20,17), 0.078 sec
Inserting... 0.000 sec
[37 ph, mem 84/77, smt 0.703, isect 3.812, ins 0.281, tot 5.109]

Running SMT solver (53)... 0.016 sec
Checking... Minimizing (20,71)... (20,17), 0.078 sec
Inserting... 0.000 sec
[38 ph, mem 88/81, smt 0.719, isect 3.891, ins 0.281, tot 5.219]

Running SMT solver (54)... 0.000 sec
Checking... Minimizing (19,68)... (19,16), 0.094 sec
Inserting... 0.000 sec
[38 ph, mem 82/75, smt 0.719, isect 3.984, ins 0.281, tot 5.312]

Running SMT solver (55)... 0.016 sec
Checking... Minimizing (19,68)... (19,17), 0.078 sec
Inserting... 0.000 sec
[39 ph, mem 85/78, smt 0.734, isect 4.062, ins 0.281, tot 5.406]

Running SMT solver (56)... 0.016 sec
Checking... Minimizing (19,68)... (19,20), 0.078 sec
Inserting... 0.000 sec
[40 ph, mem 88/80, smt 0.750, isect 4.141, ins 0.281, tot 5.500]

Running SMT solver (57)... 0.016 sec
Checking... Minimizing (19,68)... (19,18), 0.078 sec
Inserting... 0.016 sec
[40 ph, mem 92/85, smt 0.766, isect 4.219, ins 0.297, tot 5.609]

Running SMT solver (58)... 0.016 sec
Checking... Minimizing (19,68)... (19,16), 0.078 sec
Inserting... 0.000 sec
[40 ph, mem 87/80, smt 0.781, isect 4.297, ins 0.297, tot 5.719]

Running SMT solver (59)... 0.000 sec
Checking... Minimizing (19,69)... (19,13), 0.078 sec
Inserting... 0.000 sec
[40 ph, mem 91/84, smt 0.781, isect 4.375, ins 0.297, tot 5.812]

Running SMT solver (60)... 0.000 sec
Checking... Minimizing (19,69)... (19,21), 0.078 sec
Inserting... 0.000 sec
[41 ph, mem 93/86, smt 0.781, isect 4.453, ins 0.297, tot 5.906]

Running SMT solver (61)... 0.000 sec
Checking... Minimizing (19,68)... (19,18), 0.078 sec
Inserting... 0.016 sec
[42 ph, mem 85/78, smt 0.781, isect 4.531, ins 0.312, tot 6.000]

Running SMT solver (62)... 0.016 sec
Checking... Minimizing (19,68)... (19,20), 0.062 sec
Inserting... 0.000 sec
[43 ph, mem 88/81, smt 0.797, isect 4.594, ins 0.312, tot 6.094]

Running SMT solver (63)... 0.000 sec
Checking... Minimizing (19,68)... (19,20), 0.078 sec
Inserting... 0.000 sec
[44 ph, mem 91/83, smt 0.797, isect 4.672, ins 0.312, tot 6.172]

Running SMT solver (64)... 0.000 sec
Checking... Minimizing (19,68)... (19,22), 0.078 sec
Inserting... 0.000 sec
[45 ph, mem 93/85, smt 0.797, isect 4.750, ins 0.312, tot 6.266]

Running SMT solver (65)... 0.016 sec
Checking... Minimizing (20,71)... (20,17), 0.078 sec
Inserting... 0.000 sec
[46 ph, mem 87/80, smt 0.812, isect 4.828, ins 0.312, tot 6.359]

Running SMT solver (66)... 0.000 sec
Checking... Minimizing (20,69)... (20,17), 0.062 sec
Inserting... 0.000 sec
[47 ph, mem 89/82, smt 0.812, isect 4.891, ins 0.312, tot 6.453]

Running SMT solver (67)... 0.000 sec
Checking... Minimizing (19,68)... (19,17), 0.078 sec
Inserting... 0.016 sec
[48 ph, mem 93/85, smt 0.812, isect 4.969, ins 0.328, tot 6.562]

Running SMT solver (68)... 0.016 sec
Checking... Minimizing (19,69)... (19,19), 0.078 sec
Inserting... 0.000 sec
[48 ph, mem 99/91, smt 0.828, isect 5.047, ins 0.328, tot 6.672]

Running SMT solver (69)... 0.000 sec
Checking... Minimizing (19,68)... (19,15), 0.062 sec
Inserting... 0.016 sec
[49 ph, mem 92/85, smt 0.828, isect 5.109, ins 0.344, tot 6.781]

Running SMT solver (70)... 0.000 sec
Checking... Minimizing (21,72)... (20,8), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 95/87, smt 0.828, isect 5.188, ins 0.344, tot 6.859]

Running SMT solver (71)... 0.016 sec
Checking... Minimizing (20,72)... (19,11), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 98/91, smt 0.844, isect 5.266, ins 0.344, tot 6.953]

Running SMT solver (72)... 0.016 sec
Checking... Minimizing (18,70)... (18,19), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 93/85, smt 0.859, isect 5.344, ins 0.359, tot 7.062]

Running SMT solver (73)... 0.016 sec
Checking... Minimizing (18,70)... (18,19), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 98/90, smt 0.875, isect 5.438, ins 0.375, tot 7.188]

Running SMT solver (74)... 0.000 sec
Checking... Minimizing (18,70)... (18,19), 0.094 sec
Inserting... 0.000 sec
[50 ph, mem 101/93, smt 0.875, isect 5.531, ins 0.375, tot 7.297]

Running SMT solver (75)... 0.000 sec
Checking... Minimizing (19,70)... (19,13), 0.078 sec
Inserting... 0.000 sec
[51 ph, mem 104/96, smt 0.875, isect 5.609, ins 0.375, tot 7.375]

Running SMT solver (76)... 0.016 sec
Checking... Minimizing (23,68)... (22,6), 0.078 sec
Inserting... 0.000 sec
[52 ph, mem 74/67, smt 0.891, isect 5.688, ins 0.375, tot 7.469]

Running SMT solver (77)... 0.000 sec
Checking... Minimizing (22,70)... (22,6), 0.047 sec
Inserting... 0.016 sec
[52 ph, mem 78/71, smt 0.891, isect 5.734, ins 0.391, tot 7.531]

Running SMT solver (78)... 0.000 sec
Checking... Minimizing (22,70)... (22,8), 0.047 sec
Inserting... 0.016 sec
[53 ph, mem 82/76, smt 0.891, isect 5.781, ins 0.406, tot 7.594]

Running SMT solver (79)... 0.016 sec
Checking... Minimizing (22,70)... (21,11), 0.047 sec
Inserting... 0.016 sec
[52 ph, mem 78/71, smt 0.906, isect 5.828, ins 0.422, tot 7.672]

Running SMT solver (80)... 0.016 sec
Checking... Minimizing (18,70)... (18,17), 0.078 sec
Inserting... 0.016 sec
[51 ph, mem 81/74, smt 0.922, isect 5.906, ins 0.438, tot 7.781]

Running SMT solver (81)... 0.016 sec
Checking... Minimizing (19,73)... (19,17), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 86/79, smt 0.938, isect 5.984, ins 0.453, tot 7.891]

Running SMT solver (82)... 0.016 sec
Checking... Minimizing (19,73)... (19,19), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 77/70, smt 0.953, isect 6.062, ins 0.469, tot 8.000]

Running SMT solver (83)... 0.016 sec
Checking... Minimizing (18,70)... (18,25), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 81/74, smt 0.969, isect 6.141, ins 0.469, tot 8.094]

Running SMT solver (84)... 0.016 sec
Checking... Minimizing (19,73)... (19,18), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 84/77, smt 0.984, isect 6.219, ins 0.469, tot 8.188]

Running SMT solver (85)... 0.000 sec
Checking... Minimizing (19,73)... (19,17), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 88/81, smt 0.984, isect 6.297, ins 0.469, tot 8.266]

Running SMT solver (86)... 0.031 sec
Checking... Minimizing (19,71)... (19,21), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 80/73, smt 1.016, isect 6.375, ins 0.469, tot 8.375]

Running SMT solver (87)... 0.016 sec
Checking... Minimizing (19,71)... (19,20), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 85/78, smt 1.031, isect 6.453, ins 0.484, tot 8.484]

Running SMT solver (88)... 0.016 sec
Checking... Minimizing (18,68)... (18,25), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 89/82, smt 1.047, isect 6.531, ins 0.484, tot 8.578]

Running SMT solver (89)... 0.016 sec
Checking... Minimizing (18,68)... (18,22), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 82/75, smt 1.062, isect 6.625, ins 0.500, tot 8.703]

Running SMT solver (90)... 0.000 sec
Checking... Minimizing (18,68)... (18,24), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 91/84, smt 1.062, isect 6.719, ins 0.516, tot 8.812]

Running SMT solver (91)... 0.016 sec
Checking... Minimizing (18,68)... (18,22), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 83/75, smt 1.078, isect 6.797, ins 0.531, tot 8.922]

Running SMT solver (92)... 0.016 sec
Checking... Minimizing (18,70)... (18,23), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 86/79, smt 1.094, isect 6.875, ins 0.531, tot 9.031]

Running SMT solver (93)... 0.000 sec
Checking... Minimizing (18,70)... (18,22), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 89/82, smt 1.094, isect 6.953, ins 0.531, tot 9.125]

Running SMT solver (94)... 0.000 sec
Checking... Minimizing (18,70)... (18,24), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 93/85, smt 1.094, isect 7.031, ins 0.547, tot 9.219]

Running SMT solver (95)... 0.016 sec
Checking... Minimizing (18,70)... (18,23), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 84/77, smt 1.109, isect 7.109, ins 0.547, tot 9.312]

Running SMT solver (96)... 0.000 sec
Checking... Minimizing (19,73)... (19,19), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 89/82, smt 1.109, isect 7.203, ins 0.562, tot 9.438]

Running SMT solver (97)... 0.016 sec
Checking... Minimizing (19,71)... (19,21), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 95/88, smt 1.125, isect 7.281, ins 0.578, tot 9.547]

Running SMT solver (98)... 0.016 sec
Checking... Minimizing (18,70)... (18,21), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 90/83, smt 1.141, isect 7.375, ins 0.594, tot 9.672]

Running SMT solver (99)... 0.016 sec
Checking... Minimizing (18,70)... (18,20), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 93/86, smt 1.156, isect 7.453, ins 0.594, tot 9.766]

Running SMT solver (100)... 0.016 sec
Checking... Minimizing (18,70)... (18,19), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 96/89, smt 1.172, isect 7.531, ins 0.609, tot 9.875]

Running SMT solver (101)... 0.016 sec
Checking... Minimizing (18,68)... (18,21), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 91/84, smt 1.188, isect 7.609, ins 0.609, tot 9.984]

Running SMT solver (102)... 0.000 sec
Checking... Minimizing (18,68)... (18,20), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 95/87, smt 1.188, isect 7.688, ins 0.625, tot 10.078]

Running SMT solver (103)... 0.016 sec
Checking... Minimizing (18,68)... (18,19), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 101/93, smt 1.203, isect 7.781, ins 0.641, tot 10.203]

Running SMT solver (104)... 0.016 sec
Checking... Minimizing (18,68)... (18,20), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 92/85, smt 1.219, isect 7.859, ins 0.656, tot 10.312]

Running SMT solver (105)... 0.016 sec
Checking... Minimizing (18,68)... (18,18), 0.078 sec
Inserting... 0.000 sec
[50 ph, mem 96/88, smt 1.234, isect 7.938, ins 0.656, tot 10.406]

Running SMT solver (106)... 0.016 sec
Checking... Minimizing (18,68)... (18,23), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 102/94, smt 1.250, isect 8.016, ins 0.672, tot 10.531]

Running SMT solver (107)... 0.000 sec
Checking... Minimizing (18,68)... (18,19), 0.062 sec
Inserting... 0.016 sec
[50 ph, mem 94/86, smt 1.250, isect 8.078, ins 0.688, tot 10.625]

Running SMT solver (108)... 0.016 sec
Checking... Minimizing (18,70)... (18,22), 0.094 sec
Inserting... 0.016 sec
[50 ph, mem 99/91, smt 1.266, isect 8.172, ins 0.703, tot 10.750]

Running SMT solver (109)... 0.016 sec
Checking... Minimizing (18,70)... (18,23), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 104/97, smt 1.281, isect 8.250, ins 0.719, tot 10.859]

Running SMT solver (110)... 0.016 sec
Checking... Minimizing (18,70)... (18,23), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 98/90, smt 1.297, isect 8.328, ins 0.734, tot 10.984]

Running SMT solver (111)... 0.031 sec
Checking... Minimizing (18,70)... (18,19), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 81/74, smt 1.328, isect 8.406, ins 0.750, tot 11.109]

Running SMT solver (112)... 0.016 sec
Checking... Minimizing (20,71)... (20,8), 0.062 sec
Inserting... 0.000 sec
[51 ph, mem 84/77, smt 1.344, isect 8.469, ins 0.750, tot 11.188]

Running SMT solver (113)... 0.016 sec
Checking... Minimizing (19,71)... (19,14), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 78/71, smt 1.359, isect 8.547, ins 0.766, tot 11.297]

Running SMT solver (114)... 0.016 sec
Checking... Minimizing (19,73)... (19,17), 0.078 sec
Inserting... 0.016 sec
[50 ph, mem 81/74, smt 1.375, isect 8.625, ins 0.781, tot 11.406]

Running SMT solver (115)... 0.016 sec
Checking... Minimizing (25,67)... (23,3), 0.031 sec
Inserting... 0.000 sec
[51 ph, mem 83/76, smt 1.391, isect 8.656, ins 0.781, tot 11.469]

Running SMT solver (116)... 0.016 sec
Checking... Minimizing (22,68)... (21,10), 0.062 sec
Inserting... 0.000 sec
[52 ph, mem 85/78, smt 1.406, isect 8.719, ins 0.781, tot 11.547]

Running SMT solver (117)... 0.016 sec
Checking... Minimizing (22,68)... (21,12), 0.062 sec
Inserting... 0.000 sec
[53 ph, mem 80/73, smt 1.422, isect 8.781, ins 0.781, tot 11.625]

Running SMT solver (118)... 0.016 sec
Checking... Minimizing (22,70)... (21,12), 0.062 sec
Inserting... 0.000 sec
[54 ph, mem 83/76, smt 1.438, isect 8.844, ins 0.781, tot 11.703]

Running SMT solver (119)... 0.000 sec
No more solutions found

54 polyhedra, 119 rounds, smt 1.438 sec, isect 8.844 sec, insert 0.781 sec, total 11.703 sec
Preprocessing 0.031 sec, super total 11.734 sec
Comparing... 0.516 sec.  Solution matches input.
Total contains() calls: 4132, full: 99, ratio: 2.396%
