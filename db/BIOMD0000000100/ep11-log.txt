
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:11:25+02:00
Command line: ..\trop\ptcut.py BIOMD0000000100 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000100 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 3DWGP7GI27BQR
Effective epsilon: 1/11.0

Reading db\BIOMD0000000100\vector_field_q.txt
Reading db\BIOMD0000000100\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 2 with: k10**k7*k5**2*k8**4*k9**2 + k10**k7*k5**2*k8**4*x4**2 + k10**k7*k5**2*k9**2*x3**4 + k10**k7*k5**2*x3**4*x4**2 + k10**k7*k8**4*k9**2*x2**2 + k10**k7*k8**4*x2**2*x4**2 + k10**k7*k9**2*x2**2*x3**4 + k10**k7*x2**2*x3**4*x4**2 + k5**2*k8**4*k9**2*x2**k7 + k5**2*k8**4*x2**k7*x4**2 + k5**2*k9**2*x2**k7*x3**4 + k5**2*x2**k7*x3**4*x4**2 + k8**4*k9**2*x2**2*x2**k7 + k8**4*x2**2*x2**k7*x4**2 + k9**2*x2**2*x2**k7*x3**4 + x2**2*x2**k7*x3**4*x4**2

Multiplying equation 3 with: k16**k30*k17**k15 + k16**k30*x2**k15 + k17**k15*x3**k30 + x2**k15*x3**k30

Multiplying equation 4 with: k10**k7*k5**2*k8**4*k9**2 + k10**k7*k5**2*k8**4*x4**2 + k10**k7*k5**2*k9**2*x3**4 + k10**k7*k5**2*x3**4*x4**2 + k10**k7*k8**4*k9**2*x2**2 + k10**k7*k8**4*x2**2*x4**2 + k10**k7*k9**2*x2**2*x3**4 + k10**k7*x2**2*x3**4*x4**2 + k5**2*k8**4*k9**2*x2**k7 + k5**2*k8**4*x2**k7*x4**2 + k5**2*k9**2*x2**k7*x3**4 + k5**2*x2**k7*x3**4*x4**2 + k8**4*k9**2*x2**2*x2**k7 + k8**4*x2**2*x2**k7*x4**2 + k9**2*x2**2*x2**k7*x3**4 + x2**2*x2**k7*x3**4*x4**2

Multiplying equation 5 with: k21**4*k22*k23**4*k26*x5 + k21**4*k22*k23**4*k28*k29 + k21**4*k22*k23**4*k29*x5 - k21**4*k23**4*k26*x5**2 + k21**4*k23**4*k26*x5 - k21**4*k23**4*k28*k29*x5 + k21**4*k23**4*k28*k29 - k21**4*k23**4*k29*x5**2 + k21**4*k23**4*k29*x5 - k21**4*k26*x2**4*x5**2 + k21**4*k26*x2**4*x5 - k21**4*k28*k29*x2**4*x5 + k21**4*k28*k29*x2**4 - k21**4*k29*x2**4*x5**2 + k21**4*k29*x2**4*x5 + k22*k23**4*k26*x2**4*x5 + k22*k23**4*k28*k29*x2**4 + k22*k23**4*k29*x2**4*x5 - k23**4*k26*x2**4*x5**2 + k23**4*k26*x2**4*x5 - k23**4*k28*k29*x2**4*x5 + k23**4*k28*k29*x2**4 - k23**4*k29*x2**4*x5**2 + k23**4*k29*x2**4*x5 - k26*x2**8*x5**2 + k26*x2**8*x5 - k28*k29*x2**8*x5 + k28*k29*x2**8 - k29*x2**8*x5**2 + k29*x2**8*x5
System is rational in variable x2,x3,x4,x5.
System is rational in parameter k5,k7,k8,k9,k10,k15,k16,k17,k21,k22,k23,k26,k27,k28,k29,k30,k32,k33.
System is exponential in parameter k7,k15,k30.
System is radical in parameter k26,k27.
Tropicalization time: 30.069 sec
Variables: x2-x5
Saving tropical system...
Creating polyhedra... 
Warning: formula defines no polyhedron!  Ignored.

Creation time: 3.687 sec
Saving polyhedra...

Sorting ascending...
Dimension: 4
Formulas: 4
Order: 1 3 0 2
Possible combinations: 4 * 7 * 11 * 11 = 3,388 (10^4)
[1/3 (#3, #1)]: 7 * 4 = 28 => 10 (9 empty, 9 incl), dim=2.0, hs=3.5
[2/3 (#0, w1)]: 11 * 10 = 110 => 16 (82 empty, 12 incl), dim=1.1, hs=4.8
[3/3 (w2, #2)]: 16 * 11 = 176 => 10 (144 empty, 22 incl), dim=1.2, hs=5.1
Total time: 0.038 sec, total intersections: 314, total inclusion tests: 428
Common plane time: 0.001 sec
Solutions: 10, dim=1.2, hs=5.1, max=16, maxin=176
f-vector: [0, 8, 2]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.072 GiB, pagefile 0.066 GiB
Memory: pmem(rss=77045760, vms=69894144, num_page_faults=22873, peak_wset=77049856, wset=77045760, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=41864, nonpaged_pool=41864, pagefile=69894144, peak_pagefile=70475776, private=69894144)
