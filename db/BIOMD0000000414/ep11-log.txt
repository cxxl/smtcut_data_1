
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:24:41+02:00
Command line: ..\trop\ptcut.py BIOMD0000000414 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000414 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 15GDXWM7GNU7O
Effective epsilon: 1/11.0

Reading db\BIOMD0000000414\vector_field_q.txt
Reading db\BIOMD0000000414\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k1*x1 + k4
System is rational in variable x1.
System is rational in parameter k1,k4,k5.
Tropicalization time: 0.660 sec
Variables: x1
Saving tropical system...
Creating polyhedra... 
Creation time: 0.005 sec
Saving polyhedra...

Sorting ascending...
Dimension: 1
Formulas: 1
Order: 0
Possible combinations: 1 = 1 (10^0)
Total time: 0.000 sec, total intersections: 0, total inclusion tests: 0
Common plane time: 0.000 sec
Solutions: 1, dim=0.0, hs=1.0, max=1, maxin=0
f-vector: [1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.067 GiB, pagefile 0.060 GiB
Memory: pmem(rss=71688192, vms=64634880, num_page_faults=20241, peak_wset=71692288, wset=71688192, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=39960, nonpaged_pool=39960, pagefile=64634880, peak_pagefile=64634880, private=64634880)
