
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:06:48+02:00
Command line: ..\trop\ptcut.py BIOMD0000000045 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7201M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000045 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 2HXSYA00WFMVP
Effective epsilon: 1/11.0

Reading db\BIOMD0000000045\vector_field_q.txt
Reading db\BIOMD0000000045\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 2 with: k11**2*k12**2*k17**2*k19**2*k9**2 + k11**2*k12**2*k17**2*k19**2*x2**2 + k11**2*k12**2*k17**2*k9**2*x4**2 + k11**2*k12**2*k17**2*x2**2*x4**2 + k11**2*k12**2*k19**2*k9**2*x2**2 + k11**2*k12**2*k19**2*x2**4 + k11**2*k12**2*k9**2*x2**2*x4**2 + k11**2*k12**2*x2**4*x4**2 + k11**2*k17**2*k19**2*k9**2*x2**2 + k11**2*k17**2*k19**2*x2**4 + k11**2*k17**2*k9**2*x2**2*x4**2 + k11**2*k17**2*x2**4*x4**2 + k11**2*k19**2*k9**2*x2**4 + k11**2*k19**2*x2**6 + k11**2*k9**2*x2**4*x4**2 + k11**2*x2**6*x4**2 + k12**2*k17**2*k19**2*k9**2*x3**2 + k12**2*k17**2*k19**2*x2**2*x3**2 + k12**2*k17**2*k9**2*x3**2*x4**2 + k12**2*k17**2*x2**2*x3**2*x4**2 + k12**2*k19**2*k9**2*x2**2*x3**2 + k12**2*k19**2*x2**4*x3**2 + k12**2*k9**2*x2**2*x3**2*x4**2 + k12**2*x2**4*x3**2*x4**2 + k17**2*k19**2*k9**2*x2**2*x3**2 + k17**2*k19**2*x2**4*x3**2 + k17**2*k9**2*x2**2*x3**2*x4**2 + k17**2*x2**4*x3**2*x4**2 + k19**2*k9**2*x2**4*x3**2 + k19**2*x2**6*x3**2 + k9**2*x2**4*x3**2*x4**2 + x2**6*x3**2*x4**2

Multiplying equation 3 with: k11**2*k12**2*k9**2 + k11**2*k12**2*x2**2 + k11**2*k9**2*x2**2 + k11**2*x2**4 + k12**2*k9**2*x3**2 + k12**2*x2**2*x3**2 + k9**2*x2**2*x3**2 + x2**4*x3**2

Multiplying equation 4 with: k17**2*k19**2 + k17**2*x4**2 + k19**2*x2**2 + x2**2*x4**2

System is rational in variable x2,x3,x4.
System is rational in parameter k2,k3,k4,k5,k9,k11,k12,k17,k19.
Tropicalization time: 35.835 sec
Variables: x1-x4
Saving tropical system...
Creating polyhedra... 
Creation time: 11.257 sec
Saving polyhedra...

Sorting ascending...
Dimension: 4
Formulas: 5
Order: 0 2 3 4 1
Possible combinations: 1 * 3 * 3 * 4 * 17 = 612 (10^3)
 Applying common planes (codim=1, hs=4)...
 [1/4 (#2)]: 3 => 1 (1 empty, 1 incl), dim=2.0, hs=4.0
  Removed: 2.3,2.4
 [2/4 (#3)]: 3 => 1 (1 empty, 1 incl), dim=2.0, hs=4.0
  Removed: 3.1,3.6
 [3/4 (#4)]: 4 => 3 (1 empty), dim=2.0, hs=4.0
  Removed: 4.2
 [4/4 (#1)]: 17 => 3 (11 empty, 3 incl), dim=2.0, hs=4.3
  Removed: 1.0,1.2,1.68,1.69,1.70,1.10,1.12,1.19,1.21,1.34,1.37,1.57,1.61,1.63
 Applying common planes (codim=3, hs=4)...
 [1/2 (#4)]: 3 => 1 (1 empty, 1 incl), dim=1.0, hs=4.0
  Removed: 4.1,4.3
 [2/2 (#1)]: 3 => 1 (1 empty, 1 incl), dim=1.0, hs=4.0
  Removed: 1.1,1.20
 Savings: factor 612 (10^3), 4 formulas
Total time: 0.006 sec, total intersections: 33, total inclusion tests: 22
Common plane time: 0.006 sec
Solutions: 1, dim=1.0, hs=4.0, max=1, maxin=0
f-vector: [0, 1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.074 GiB, pagefile 0.068 GiB
Memory: pmem(rss=76873728, vms=70733824, num_page_faults=23203, peak_wset=79527936, wset=76873728, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=42000, nonpaged_pool=42000, pagefile=70733824, peak_pagefile=73109504, private=70733824)
