[
  ["#0",
    [[0]], [
    [[[-2,0,1,0,0]], [], 0]
  ]],
  ["#1",
    [[0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1],
     [0,0,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1],
     [0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,1,1],
     [0,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1],
     [0,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1],
     [1,1,1,1,1,0,0,0,1,1,0,1,1,1,1,1,1],
     [1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1],
     [1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1],
     [1,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1],
     [1,1,0,1,1,1,1,1,0,0,1,1,1,1,1,1,1],
     [1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1],
     [1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1],
     [1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0],
     [1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1],
     [1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0],
     [1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0],
     [1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0,0]], [
    [[[1,0,1,0,0]], [[1,0,0,-1,0],[1,0,0,1,0],[1,0,0,0,1],[1,0,0,0,-1]], 0],
    [[[-3,0,0,2,0]], [[0,0,1,0,0],[0,0,0,0,1],[1,0,0,0,-1],[2,0,-1,0,0]], 1],
    [[[3,0,1,-2,0]], [[1,0,1,0,0],[1,0,0,0,-1],[0,0,-1,0,1],[0,0,-1,0,0]], 2],
    [[[0,0,1,0,-1]], [[0,0,-1,1,0],[-1,0,-1,0,0],[1,0,0,-1,0]], 10],
    [[[0,0,1,-1,0]], [[1,0,0,0,-1],[0,0,-1,0,1],[-1,0,-1,0,0]], 12],
    [[[1,0,1,0,0]], [[1,0,0,-1,0],[1,0,0,1,0],[-1,0,0,0,1]], 19],
    [[[-3,0,0,2,0]], [[0,0,1,0,0],[-1,0,0,0,1],[2,0,-1,0,0]], 20],
    [[[3,0,1,-2,0]], [[1,0,1,0,0],[-1,0,0,0,1],[0,0,-1,0,0]], 21],
    [[[0,0,0,0,1]], [[0,0,1,0,0],[-3,0,0,2,0],[2,0,-1,0,0]], 26],
    [[[0,0,1,0,-1]], [[-3,0,-1,2,0],[-1,0,0,1,0],[0,0,-1,0,0]], 34],
    [[[0,0,1,-1,0]], [[-1,0,0,0,1],[-1,0,-1,0,0]], 37],
    [[[-3,0,0,2,0]], [[-2,0,1,0,0],[1,0,0,0,-1],[4,0,-2,0,1]], 57],
    [[[-3,0,0,2,0]], [[-2,0,1,0,0],[4,0,-2,0,1],[-1,0,0,0,1],[11,0,-4,0,0]], 61],
    [[[-4,0,2,0,-1]], [[5,0,-2,0,0],[-3,0,0,2,0],[-2,0,1,0,0]], 63],
    [[[-4,0,2,0,-1]], [[-5,0,2,0,0],[4,0,-2,1,0],[3,0,-1,0,0],[-3,0,0,2,0]], 68],
    [[[-4,0,2,-1,0]], [[4,0,-2,0,1],[-11,0,4,0,0],[3,0,-1,0,0]], 69],
    [[[-3,0,1,0,0]], [[-2,0,0,0,1],[-2,0,0,1,0]], 70]
  ]],
  ["#2",
    [[0,1,1],
     [1,0,0],
     [1,0,0]], [
    [[[-3,0,0,2,0]], [[2,0,-1,0,0]], 0],
    [[[-3,0,0,2,0]], [[11,0,-4,0,0],[-2,0,1,0,0]], 3],
    [[[-4,0,2,-1,0]], [[-11,0,4,0,0]], 4]
  ]],
  ["#3",
    [[0,0,1],
     [0,0,1],
     [1,1,0]], [
    [[[0,0,0,0,1]], [[2,0,-1,0,0]], 0],
    [[[-4,0,2,0,-1]], [[5,0,-2,0,0],[-2,0,1,0,0]], 1],
    [[[-4,0,2,0,-1]], [[-5,0,2,0,0]], 6]
  ]],
  ["#4",
    [[0,0,0,0],
     [0,0,0,0],
     [0,0,0,0],
     [0,0,0,0]], [
    [[[0,0,0,0,1]], [[0,0,0,1,0],[0,0,1,0,0],[0,1,0,0,0]], 0],
    [[[0,0,0,1,0]], [[0,0,0,0,1],[0,0,1,0,0],[0,1,0,0,0]], 1],
    [[[0,0,1,0,0]], [[0,0,0,0,1],[0,0,0,1,0],[0,1,0,0,0]], 2],
    [[[0,1,0,0,0]], [[0,0,0,0,1],[0,0,0,1,0],[0,0,1,0,0]], 3]
  ]]
]
