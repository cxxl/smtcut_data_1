
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:01:43+02:00
Command line: ..\trop\ptcut.py BIOMD0000000003 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 7198M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000003 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 7EEK5HO6XOWF
Effective epsilon: 1/11.0

Reading db\BIOMD0000000003\vector_field_q.txt
Reading db\BIOMD0000000003\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k10 + x1

Multiplying equation 2 with: -k11*k13*k5 - k11*k13*x1 - k11*k5*x2 - k11*x1*x2 + k13*k5*x2 - k13*k5 + k13*x1*x2 - k13*x1 + k5*x2**2 - k5*x2 + x1*x2**2 - x1*x2

Multiplying equation 3 with: -k14*k15 - k14*x3 + k15*x3 - k15 + x3**2 - x3
System is rational in variable x1,x2,x3.
System is rational in parameter k5,k10,k11,k13,k14,k15.
Tropicalization time: 1.385 sec
Variables: x1-x3
Saving tropical system...
Creating polyhedra... 
Creation time: 0.018 sec
Saving polyhedra...

Sorting ascending...
Dimension: 3
Formulas: 3
Order: 0 2 1
Possible combinations: 3 * 5 * 6 = 90 (10^2)
 Applying common planes (codim=0, hs=1)...
 [1/3 (#0)]: 3 => 3 (-), dim=2.0, hs=2.3
 [2/3 (#2)]: 5 => 5 (-), dim=2.0, hs=3.2
 [3/3 (#1)]: 6 => 4 (2 incl), dim=2.0, hs=2.2
  Removed: 1.6,1.7
 Savings: factor 1.5, 0 formulas
[1/2 (#1, #0)]: 4 * 3 = 12 => 6 (2 empty, 4 incl), dim=1.3, hs=3.5
[2/2 (w1, #2)]: 6 * 5 = 30 => 6 (20 empty, 4 incl), dim=0.8, hs=3.7
Total time: 0.008 sec, total intersections: 56, total inclusion tests: 115
Common plane time: 0.002 sec
Solutions: 6, dim=0.8, hs=3.7, max=6, maxin=30
f-vector: [1, 5]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.068 GiB, pagefile 0.061 GiB
Memory: pmem(rss=72564736, vms=65572864, num_page_faults=20507, peak_wset=72568832, wset=72564736, peak_paged_pool=545648, paged_pool=545472, peak_nonpaged_pool=40232, nonpaged_pool=40232, pagefile=65572864, peak_pagefile=65572864, private=65572864)
