
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 0-1-1-0
\ dimension 0; is compact
MAXIMIZE
Subject To
  eq0: +1 x3 = -1   \ x3 = 11.0
  eq1: +1 x2 = 0    \ x2 = 1.0
  eq2: +1 x1 = 1    \ x1 = 0.0909091
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

\ combo: 1-3-3-0
\ dimension 0; is compact
MAXIMIZE
Subject To
  eq0: +1 x3 = -1   \ x3 = 11.0
  eq1: +1 x2 = -1   \ x2 = 11.0
  eq2: +1 x1 = 1    \ x1 = 0.0909091
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

