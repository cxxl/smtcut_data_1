
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 0

\ combo: 14-8-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 >= -1       \ x1**-1 <= 11.0
  ie1: +2 x1 >= 1        \ x1 <= 0.301511
  eq0: +1 x4 = 0         \ x4 = 1.0
  eq1: +1 x3 = 0         \ x3 = 1.0
  eq2: +2 x1 -1 x2 = 2   \ x1**2 * x2**-1 = 0.00826446
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 1

\ combo: 4-4-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x1 >= 0    \ x1**-1 <= 1.0
  ie1: +1 x1 >= -1   \ x1 <= 11.0
  eq0: +1 x4 = 0     \ x4 = 1.0
  eq1: +1 x3 = 0     \ x3 = 1.0
  eq2: +2 x2 = 1     \ x2 = 0.301511
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 2

\ combo: 7-5-2-0
\ dimension 1; is not compact
MAXIMIZE
Subject To
  ie0: -1 x4 >= 0   \ x4**-1 <= 1.0
  eq0: +1 x3 = 0    \ x3 = 1.0
  eq1: +1 x2 = 0    \ x2 = 1.0
  eq2: +1 x1 = 1    \ x1 = 0.0909091
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 3

\ combo: 7-5-1-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x4 >= -2   \ x4**-1 <= 121.0
  ie1: +1 x4 >= 0    \ x4 <= 1.0
  eq0: +1 x3 = 0     \ x3 = 1.0
  eq1: +1 x2 = 0     \ x2 = 1.0
  eq2: +1 x1 = 1     \ x1 = 0.0909091
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 4

\ combo: 1-1-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -1 x2 >= 0    \ x2**-1 <= 1.0
  ie1: +1 x2 >= -1   \ x2 <= 11.0
  eq0: +1 x4 = 0     \ x4 = 1.0
  eq1: +1 x3 = 0     \ x3 = 1.0
  eq2: +2 x1 = 1     \ x1 = 0.301511
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ file 5

\ combo: 0-0-0-0
\ dimension 1; is compact
MAXIMIZE
Subject To
  ie0: -2 x1 >= -1       \ x1**-1 <= 3.31662
  ie1: +1 x1 >= 0        \ x1 <= 1.0
  eq0: +1 x4 = 0         \ x4 = 1.0
  eq1: +1 x3 = 0         \ x3 = 1.0
  eq2: +2 x1 +2 x2 = 1   \ x1 * x2 = 0.301511
END

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ end

