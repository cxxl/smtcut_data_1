
----------------------------------------------------------------------
This is ptcut v3.5.1
Python 3.7.7 (default, Mar 23 2020, 23:19:08) [MSC v.1916 64 bit (AMD64)]
numpy 1.18.1, sympy 1.5.1, gmpy2 2.1.0a5
It is now 2020-04-04T14:35:03+02:00
Command line: ..\trop\ptcut.py BIOMD0000000590 -e11 --rat --no-cache=n --maxruntime 100 --maxmem 10849M --comment "concurrent=6, timeout=100"

----------------------------------------------------------------------
Solving BIOMD0000000590 with ep=1/11, round=0, complex=False, sumup=False, keep_coeff=False, paramwise=True, complete=False, filter=0, fusion=False, algorithm 3, resort=False, bbox=False, common=9, qdisjoint=False, nonewton=False, mul_denom=True, convexhull=(False, 2, 4, 0.5, 2), likeness 14, backend PPL_C_Polyhedron
Comment: concurrent=6, timeout=100
Random seed: 245JIA3GUMUF6
Effective epsilon: 1/11.0

Reading db\BIOMD0000000590\vector_field_q.txt
Reading db\BIOMD0000000590\conservation_laws_q.txt
Computing tropical system... 
Multiplying equation 1 with: k11*k4 + k11*x8 + k4*x1 + x1*x8

Multiplying equation 2 with: k11*k13*k4 + k11*k13*x8 + k11*k4*x2 + k11*x2*x8 + k13*k4*x1 + k13*x1*x8 + k4*x1*x2 + x1*x2*x8

Multiplying equation 3 with: k13*k15 + k13*x3 + k15*x2 + x2*x3

Multiplying equation 4 with: k15*k17 + k15*k18*x4 + k17*x3 + k18*x3*x4

Multiplying equation 5 with: k17*k20 + k17*x5 + k18*k20*x4 + k18*x4*x5

Multiplying equation 6 with: k20*k22 + k20*x6 + k22*x5 + x5*x6

Multiplying equation 7 with: k22*k24 + k22*x7 + k24*x6 + x6*x7

Multiplying equation 8 with: k24*k26*k28 + k24*k26*x8 + k24*k28*x8 + k24*x8**2 + k26*k28*x7 + k26*x7*x8 + k28*x7*x8 + x7*x8**2

Multiplying equation 9 with: k26*k28 + k26*x8 + k28*x9 + x8*x9
System is rational in variable x1,x2,x3,x4,x5,x6,x7,x8,x9.
System is rational in parameter k2,k3,k4,k5,k6,k8,k9,k10,k11,k13,k15,k17,k18,k20,k22,k24,k26,k28,k29,k31.
System is radical in parameter k2,k3,k5,k6,k8,k9,k10,k29,k31.
Tropicalization time: 17.385 sec
Variables: x1-x9
Saving tropical system...
Creating polyhedra... 
Creation time: 0.211 sec
Saving polyhedra...

Sorting ascending...
Dimension: 9
Formulas: 9
Order: 0 2 3 8 1 4 5 6 7
Possible combinations: 2 * 2 * 2 * 2 * 4 * 4 * 4 * 4 * 5 = 20,480 (10^4)
 Applying common planes (codim=0, hs=1)...
 [1/9 (#0)]: 2 => 2 (-), dim=8.0, hs=2.0
 [2/9 (#2)]: 2 => 2 (-), dim=8.0, hs=3.0
 [3/9 (#3)]: 2 => 2 (-), dim=8.0, hs=3.0
 [4/9 (#8)]: 2 => 2 (-), dim=8.0, hs=3.0
 [5/9 (#1)]: 4 => 2 (2 empty), dim=8.0, hs=3.0
  Removed: 1.2,1.3
 [6/9 (#4)]: 4 => 4 (-), dim=8.0, hs=3.5
 [7/9 (#5)]: 4 => 4 (-), dim=8.0, hs=3.2
 [8/9 (#6)]: 4 => 4 (-), dim=8.0, hs=3.2
 [9/9 (#7)]: 5 => 5 (-), dim=8.0, hs=3.6
 Savings: factor 2.0, 0 formulas
[1/8 (#0, #2)]: 2 * 2 = 4 => 4 (-), dim=7.0, hs=4.0
 Applying common planes (codim=0, hs=1)...
 [1/8 (w1)]: 4 => 2 (2 empty), dim=7.0, hs=4.0
 [2/8 (#3)]: 2 => 2 (-), dim=8.0, hs=4.0
 [3/8 (#8)]: 2 => 2 (-), dim=8.0, hs=4.0
 [4/8 (#1)]: 2 => 2 (-), dim=8.0, hs=3.0
 [5/8 (#4)]: 4 => 4 (-), dim=8.0, hs=4.5
 [6/8 (#5)]: 4 => 4 (-), dim=8.0, hs=4.2
 [7/8 (#6)]: 4 => 4 (-), dim=8.0, hs=4.2
 [8/8 (#7)]: 5 => 5 (-), dim=8.0, hs=4.6
 Savings: factor 2.0, 0 formulas
[2/8 (w1, #3)]: 2 * 2 = 4 => 2 (2 empty), dim=6.0, hs=5.0
 Applying common planes (codim=2, hs=2)...
 [1/7 (w2)]: 2 => 2 (-), dim=6.0, hs=5.0
 [2/7 (#8)]: 2 => 2 (-), dim=6.0, hs=6.0
 [3/7 (#1)]: 2 => 2 (-), dim=6.0, hs=5.0
 [4/7 (#4)]: 4 => 1 (3 empty), dim=6.0, hs=5.0
  Removed: 4.1,4.2,4.3
 [5/7 (#5)]: 4 => 4 (-), dim=6.0, hs=6.2
 [6/7 (#6)]: 4 => 4 (-), dim=6.0, hs=6.2
 [7/7 (#7)]: 5 => 5 (-), dim=6.0, hs=6.6
 Applying common planes (codim=3, hs=5)...
 [1/6 (w2)]: 2 => 2 (-), dim=5.0, hs=6.0
 [2/6 (#8)]: 2 => 2 (-), dim=5.0, hs=7.0
 [3/6 (#1)]: 2 => 2 (-), dim=5.0, hs=6.0
 [4/6 (#5)]: 4 => 1 (3 empty), dim=5.0, hs=6.0
  Removed: 5.0,5.3,5.4
 [5/6 (#6)]: 4 => 4 (-), dim=5.0, hs=7.2
 [6/6 (#7)]: 5 => 5 (-), dim=5.0, hs=7.6
 Applying common planes (codim=4, hs=6)...
 [1/5 (w2)]: 2 => 2 (-), dim=4.0, hs=7.0
 [2/5 (#8)]: 2 => 2 (-), dim=4.0, hs=8.0
 [3/5 (#1)]: 2 => 2 (-), dim=4.0, hs=7.0
 [4/5 (#6)]: 4 => 1 (3 empty), dim=4.0, hs=7.0
  Removed: 6.0,6.1,6.2
 [5/5 (#7)]: 5 => 5 (-), dim=4.0, hs=8.6
 Applying common planes (codim=5, hs=7)...
 [1/4 (w2)]: 2 => 2 (-), dim=3.0, hs=8.0
 [2/4 (#8)]: 2 => 2 (-), dim=3.0, hs=9.0
 [3/4 (#1)]: 2 => 2 (-), dim=3.0, hs=8.0
 [4/4 (#7)]: 5 => 2 (2 empty, 1 incl), dim=2.5, hs=8.5
  Removed: 7.1,7.4,7.5
 Savings: factor 160 (10^2), 3 formulas
[3/5 (w2, #8)]: 2 * 2 = 4 => 3 (1 empty), dim=2.0, hs=9.3
[4/5 (w3, #1)]: 3 * 2 = 6 => 4 (2 empty), dim=1.0, hs=9.5
[5/5 (w4, #7)]: 4 * 2 = 8 => 1 (3 empty, 4 incl), dim=1.0, hs=10.0
Total time: 0.040 sec, total intersections: 150, total inclusion tests: 295
Common plane time: 0.031 sec
Solutions: 1, dim=1.0, hs=10.0, max=4, maxin=8
f-vector: [0, 1]
Canonicalizing...
Saving solutions... 
Calculation done.
Peak memory: working set 0.072 GiB, pagefile 0.065 GiB
Memory: pmem(rss=77033472, vms=70148096, num_page_faults=21817, peak_wset=77041664, wset=77033472, peak_paged_pool=545632, paged_pool=545456, peak_nonpaged_pool=41728, nonpaged_pool=41728, pagefile=70148096, peak_pagefile=70287360, private=70148096)
